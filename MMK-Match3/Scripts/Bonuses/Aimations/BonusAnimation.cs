﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animation = MMK.Match3.TokenComponents.Animation;

namespace MMK.Match3
{
    public abstract class BonusAnimation : Animation, IDamageDealer
    {
        public ShapesManager Manager { get; set; }

        protected float removingTime;
        protected readonly List<GameObject> animatingTokens = new List<GameObject>();

        protected override void Start ()
        {
            base.Start();

            removingTime = length + 1f;
            DamageAmount = 1;
            //ScoreMultiplier = 2;

            Invoke(nameof(Remove), removingTime);
        }

        public int DamageAmount { get; set; }

        public int ScoreMultiplier { get; set; }

        public void DealDamage(IEnumerable<GameObject> objects)
        {
            foreach (var o in objects) {
                o.DealDamage(this);
            }
        }

        protected abstract IEnumerator CheckRemove();

        protected void Remove ()
        {
            StartCoroutine(CheckRemove());
        }

        protected abstract void OverlapAndDestroy();
    }
}
