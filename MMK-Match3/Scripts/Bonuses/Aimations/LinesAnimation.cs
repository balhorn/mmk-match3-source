﻿using System;
using System.Collections;
using MMK.Core;
using MMK.Match3;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Animations/Spikes animation")]
    public class LinesAnimation : BonusAnimation
    {
        public GameObject animationPrefab;
        public float      explosionDuration = 0.672f;
        public float      explosionDelay = 0.2f;

        private bool isVertical;
        private bool moving;

        private GameObject     instance;
        private SpriteRenderer animationRenderer;
        private Animator       prefabAnimator;

        protected override void Awake ()
        {
            if (!Application.isPlaying) {
                return;
            }

            enabled = false;
            isVertical = GetComponent<LinesAction>().isVertical;
            //InitPrefab();
        }

        private void InitPrefab()
        {
            try {
                animationPrefab.transform.localPosition = Vector3.zero;
                animationRenderer = animationPrefab.GetComponent<SpriteRenderer>();
                prefabAnimator = animationPrefab.GetComponent<Animator>();

                prefabAnimator.SetBool("isVertical", isVertical);
            } catch (NullReferenceException) {
                Debug.LogError("Line bonus animation requires animation prefab to work correctly");
            }
        }


        protected override void Start ()
        {
            base.Start();

            InitPrefab();

            CancelInvoke();

            StartCoroutine(Play());
        }

        private void Update ()
        {
            if (moving) {
                OverlapAndDestroy();
            }
        }

        protected override void Animate()
        {
            animator.SetTrigger(trigger);
            animator.SetInteger("Color", (int) gameObject.GetColor());
            particleSystem?.Play(true);
        }

        protected override void AfterWaiting()
        {
            var origin = Level.Current.Builder.FieldOrigin;
            var prefabTranform = animationPrefab.transform;

            var x = isVertical
                ? prefabTranform.position.x
                : origin.x + (float) LevelConfig.columns / 2 * Match3Player.Settings.tokenSize.x;
            var y = isVertical
                ? origin.y + (float) LevelConfig.rows / 2 * Match3Player.Settings.tokenSize.y
                : prefabTranform.position.y;

            prefabTranform.parent = null;
            prefabTranform.position = new Vector2(x, y);
            prefabAnimator.enabled = true;
        }

        public override IEnumerator Play()
        {
            if (GameController.state == GameState.Idle) {
                GameController.state = GameState.Animating;
            }

            int hash = StartAnimation();

            Animate();

            yield return new WaitForSeconds(explosionDelay);

            AfterWaiting();
            moving = true;

            yield return new WaitForSeconds(explosionDuration);

            Destroy(animationPrefab);
            EndAnimationImmidiate(hash);

            AfterAnimation();

            if (GameController.state == GameState.Animating) {
                GameController.state = GameState.Idle;
            }
        }

        protected override void OverlapAndDestroy ()
        {
            var cast = RaycastAgainstToken(animationRenderer);
            var hit = cast.collider?.gameObject;

            if (hit == null) {
                return;
            }

            hit.DealDamage(this);
            hit.layer = LayerMask.NameToLayer("Default");
        }

        private RaycastHit2D RaycastAgainstToken (SpriteRenderer spriteRenderer)
        {
            return Physics2D.BoxCast(
                spriteRenderer.bounds.center,
                spriteRenderer.bounds.extents,
                0f,
                Vector2.one,
                100f,
                LayerMask.GetMask("Animation"));
        }

        protected override IEnumerator CheckRemove()
        {
            yield return null;
        }
    }
}