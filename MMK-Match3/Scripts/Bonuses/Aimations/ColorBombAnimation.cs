﻿using System.Collections;
using System.IO;
using System.Linq;
using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Animations/Color bomb animation")]
    public class ColorBombAnimation : BonusAnimation
    {
        public float growingSpeed = 10f;
        public TokenColor color;

        private GameObject animationPrefab;
        private SpriteRenderer rend;

        public TokenColor Color
        {
            get; set;
        }

        private new Transform transform;

        private void Awake ()
        {
            enabled = false;
        }

        protected override void Start ()
        {
            base.Start();

            SetupObject();

            radius = rend.bounds.extents.x;
            transform = animationPrefab.transform;
            offset = growingSpeed * 0.2f;

            StartCoroutine(Play());
        }

        private void SetupObject()
        {
            animationPrefab = ElementFactory.LoadEmptyPrefab();
            animationPrefab.transform.position = GetComponent<Transform>().position;
            animationPrefab.transform.parent = GetComponent<Transform>();
            animationPrefab.SetActive(false);

            rend = animationPrefab.GetComponent<SpriteRenderer>();

            var sprites = Resources.LoadAll<Sprite>(Path.Combine(Constants.SpritesFolder, "bomb"));
            rend.sprite = sprites[14];
            rend.sortingLayerName = "Tokens";
            rend.sortingOrder = 5;
        }

        public override IEnumerator Play()
        {
            if (GameController.state == GameState.Idle) {
                GameController.state = GameState.Animating;
            }

            animator.enabled = true;
            animator.SetTrigger(trigger);

            yield return new WaitForSeconds(length);

            GetComponent<SpriteRenderer>().enabled = false;
            animationPrefab.SetActive(true);
            running = true;

            int hash = animationManager.StartAnimation(Hash);

            yield return new WaitUntil(() => animationPrefab == null);

            EndAnimationImmidiate(hash);
            Destroy(gameObject);

            if (GameController.state == GameState.Animating) {
                GameController.state = GameState.Idle;
            }
        }

        protected override IEnumerator CheckRemove()
        {
            yield return new WaitForSeconds(0.3f);//WaitUntil(() => animatingTokens.All(token => token == null));

            Destroy(animationPrefab);
        }

        private float radius;
        private float offset;
        private bool running;

        private void Update ()
        {
            if (!running) {
                return;
            }

            OverlapAndDestroy();
            ScaleExplosion();
        }

        protected override void OverlapAndDestroy ()
        {
            var casts = Physics2D.CircleCastAll(rend.bounds.center,
                radius,
                Vector2.one,
                100f,
                LayerMask.GetMask("Animation"));

            var tokens = casts
                .Where(cast => cast.collider != null)
                .Select(cast => cast.collider.gameObject);

            foreach (var token in tokens) {
                token.layer = LayerMask.NameToLayer("Default");
                animatingTokens.Add(token);
                token.DealDamage(this);
            }
        }

        private void ScaleExplosion ()
        {
            var scale = transform.localScale;
            scale.x += growingSpeed * Time.deltaTime;
            scale.y += growingSpeed * Time.deltaTime;
            radius += (growingSpeed - offset) * Time.deltaTime;

            transform.localScale = scale;
        }

    
    }
}
