﻿using System.Linq;
using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Actions/Color bomb action")]
    public class ColorBombAction : MMK.Match3.TokenComponents.Action
    {
        private ColorBombAnimation anim;

        private TokenColor targetColor;

        protected override void Awake ()
        {
            base.Awake();
            GetComponent<BonusMatch>().matchableWithTokens = true;
        }

        protected new void Start()
        {
            base.Start ();
            anim = GetComponent<ColorBombAnimation>();
            shapesInAction = MatchShape.None | MatchShape.Five;
        }

        public override void Run (GameObject target)
        {
            gameObject.SendMessage("OnActivate", SendMessageOptions.DontRequireReceiver);
            CollectTokens(target);
            SetupScore();
            DealDamage(destroyingTokens);
            Destroy(gameObject);
            //anim.color = targetColor;
            //owner.Animation.enabled = true;
        }

        protected override void CollectTokens (GameObject target)
        {
            targetColor = target != null
                ? target.GetColor()
                : ChooseColor();

            destroyingTokens = Tokens.Items
                .Where(token => token.GetColor() == targetColor)
                .ToList();

            RemoveRunningBonuses();

            foreach (var token in destroyingTokens) {
                token.layer = LayerMask.NameToLayer("Animation");
            }
        }

        private TokenColor ChooseColor ()
        {
            TokenColor color;
            do {
                color = Tokens[PositionInfo.GenerateRandom()].GetColor();
            } while (color == TokenColor.None);

            return color;
        }
    }
}
