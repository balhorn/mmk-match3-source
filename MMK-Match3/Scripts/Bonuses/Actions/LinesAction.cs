﻿using System.Linq;
using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Actions/Lines action")]
    public class LinesAction : MMK.Match3.TokenComponents.Action
    {
        public bool isVertical;

        public static int animationsStarted;
        public static int animationsFinished;

        private new LinesAnimation animation;

        protected override void Awake()
        {
            base.Awake();
            isVertical = owner.matchShape.HasFlag(MatchShape.HorizontalFour);
            shapesInAction = MatchShape.None | (isVertical ? MatchShape.HorizontalFour : MatchShape.VerticalFour);
        }

        protected new void Start()
        {
            base.Start();
            animation = GetComponent<LinesAnimation>();
            animation.Manager = shapesManager;
        }

        public override void Run(GameObject target)
        {
            gameObject.SendMessage("OnActivate", SendMessageOptions.DontRequireReceiver);
            CollectTokens(target);
            SetupScore();
            animation.enabled = true;
        }

        protected override void CollectTokens(GameObject target)
        {
            destroyingTokens = isVertical
                ? shapesManager.Tokens.GetEntireColumn(gameObject).Distinct().ToList()
                : shapesManager.Tokens.GetEntireRow(gameObject).Distinct().ToList();

            destroyingTokens.Remove(gameObject);

            RemoveRunningBonuses();

            foreach (var token in destroyingTokens) {
                token.layer = LayerMask.NameToLayer("Animation");
            }
        }
    }
}