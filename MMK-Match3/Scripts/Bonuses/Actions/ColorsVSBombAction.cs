﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MMK.Match3.TokenComponents;
using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Actions/Color bomb vs bomb action")]
    public class ColorsVSBombAction : MMK.Match3.TokenComponents.Action
    {
        private List<GameObject> collectedTokens = new List<GameObject>();
        private List<GameObject> bombs = new List<GameObject>();

        private GameObject bombBonus;

        protected new void Start ()
        {
            base.Start();

            shapesInAction = (MatchShape.Five | MatchShape.T | MatchShape.Corner);
        }

        public IEnumerator RunAnimation ()
        {
            yield return new WaitForSeconds(0.5f);
            TriggerLines();

            Destroy(gameObject);
            Destroy(target);
        }

        private void TriggerLines ()
        {
            DealDamage(bombs);
            DealDamage(new[] { target, gameObject });

            //TODO Temporary stub until we don't have super bonuses animation
            if (bombs.Count == 0) {
                owner.Animation.EndAnimationImmidiate(234252);
            }
        }

        public override void Run (GameObject target)
        {
            gameObject.SendMessage("SuperBonus", GetType().Name, SendMessageOptions.DontRequireReceiver);
            RuntimeInit(target);

            collectedTokens.Clear();
            bombs.Clear();
            bombBonus = gameObject.HasComponent<BombAction>() ? gameObject : target;

            CollectTokens(target);
            SpawnBonuses();

            StartCoroutine(RunAnimation());
        }

        private void SpawnBonuses ()
        {
            foreach (var item in collectedTokens) {
                if (item.IsBlocked()) {
                    InjectUnder(item);
                    continue;
                }

                PositionInfo info = new PositionInfo(item.GetComponent<Movement>());
                GameObject bonus = shapesManager.BonusManager.GetBombBonus(item.GetColor());
                bonus = shapesManager.CreateBonus(bonus, info);

                bombs.Add(bonus);
                Destroy(item);
            }
        }

        private void InjectUnder (GameObject blockedGo)
        {
        }

        protected override void CollectTokens (GameObject target)
        {
            TokenColor color = bombBonus.GetColor();
            collectedTokens = Tokens.Items
                .Where(token => token.GetColor() == color
                                && !token.HasComponent<Bonus>()
                                && !token.Undestroyable()
                )
                .ToList();

            collectedTokens.Remove(gameObject);
            collectedTokens.Remove(target);
        }
    }
}
