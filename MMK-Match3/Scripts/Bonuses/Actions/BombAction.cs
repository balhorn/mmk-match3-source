﻿using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Actions/Bomb Action")]
    public class BombAction : Action
    {
        protected new void Start()
        {
            base.Start ();
            shapesInAction = MatchShape.T | MatchShape.Corner | MatchShape.None;
        }

        public override void Run (GameObject target)
        {
            gameObject.SendMessage("OnActivate", SendMessageOptions.DontRequireReceiver);
            CollectTokens(target);
            SetupScore();
            DealDamage(destroyingTokens);

            //owner.Graphics.RenderBaseObject(false);
            StartCoroutine(owner.Animation.Play());
        }

        protected override void CollectTokens (GameObject target)
        {
            var movement = GetComponent<Movement>();
            for (int row = movement.Row - 1; row <= movement.Row + 1; ++row) {
                for (int column = movement.Column - 1; column <= movement.Column + 1; ++column) {
                    var position = new PositionInfo(row, column);
                    if (row < 0 || row > LevelConfig.rows - 1
                        || column < 0 || column > LevelConfig.columns - 1
                        || shapesManager.Tokens[position] == null) {
                        continue;
                    }

                    destroyingTokens.Add(shapesManager.Tokens[position]);
                }
            }

            destroyingTokens.Remove (gameObject);
        }

        protected override void SetupScore ()
        {
            Score = Match3Player.Settings.bombExplosionScore;
            DependsOnCascade = false;

            ScoreManager.RecordScore(this);
        }
    }
}
