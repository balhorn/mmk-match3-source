﻿using System.Linq;
using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Actions/Bomb vs bomb action")]
    public class BombVSBombAction : BombAction
    {
        protected new void Start ()
        {
            base.Start();
            shapesInAction = MatchShape.T | MatchShape.Corner;
        }

        public override void Run (GameObject target)
        {
            gameObject.SendMessage("SuperBonus", GetType().Name, SendMessageOptions.DontRequireReceiver);
            RuntimeInit(target);
            base.Run(target);
            DealDamage(new[] {gameObject, target});
            Destroy(target);
        }

        protected override void CollectTokens (GameObject target)
        {
            var movement = GetComponent<Movement>();
            for (var row = movement.Row - 2; row <= movement.Row + 2; ++row) {
                for (var column = movement.Column - 2; column <= movement.Column + 2; ++column) {
                    var position = new PositionInfo(row, column);
                    if (row < 0 || row > LevelConfig.rows - 1
                        || column < 0 || column > LevelConfig.columns - 1
                        || shapesManager.Tokens[position] == null) {
                        continue;
                    }

                    destroyingTokens.Add(shapesManager.Tokens[position]);
                }
            }

            destroyingTokens.Remove(target);
            destroyingTokens.Remove(gameObject);
            RemoveRunningBonuses();
        }

        protected override void SetupScore ()
        {
            foreach (var durability in destroyingTokens.Select(token => token.Durability())) {
                Score += durability?.Score ?? 0;
            }
            Score += Match3Player.Settings.bombExplosionScore;
            DependsOnCascade = false;

            ScoreManager.RecordScore(this);
        }
    }
}

