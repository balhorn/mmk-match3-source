﻿using System.Linq;
using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Actions/Lines vs bomb action")]
    public class LinesVSBombAction : Action
    {
        protected new void Start()
        {
            base.Start ();
            shapesInAction = MatchShape.T | MatchShape.HorizontalFour | MatchShape.VerticalFour;
        }

        public override bool SuitableFor(MatchShape first, MatchShape second)
        {
            return (first | second) == (MatchShape.T | MatchShape.Corner | MatchShape.HorizontalFour)
                   || (first | second) == (MatchShape.T | MatchShape.Corner | MatchShape.VerticalFour);
        }

        public override void Run (GameObject target)
        {
            gameObject.SendMessage("SuperBonus", GetType().Name, SendMessageOptions.DontRequireReceiver);

            CollectTokens(target);
            RuntimeInit(target);
            SetupScore();
            DealDamage(destroyingTokens);
            DealDamage(new[] {gameObject, target});

            Destroy(target);
            Destroy(gameObject);
        }

        protected override void CollectTokens (GameObject target)
        {
            var movement = GetComponent<Movement> ();

            if (movement == null) return;

            int column = movement.Column;
            int row = movement.Row;
            int[] columns = { column - 1, column, column + 1 };
            int[] rows = { row - 1, row, row + 1 };

            destroyingTokens = shapesManager.Tokens.GetColumns(columns).ToList();
            destroyingTokens.AddRange(shapesManager.Tokens.GetRows(rows));

            destroyingTokens = destroyingTokens.Distinct().ToList();

            //destroyingTokens.Remove(target);
            //destroyingTokens.Remove(gameObject);
            RemoveRunningBonuses();
        }
    }
}
