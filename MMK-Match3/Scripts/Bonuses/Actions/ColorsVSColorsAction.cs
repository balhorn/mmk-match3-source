﻿using System.Linq;
using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Actions/Color bomb vs color bomb action")]
    public class ColorsVSColorsAction : MMK.Match3.TokenComponents.Action
    {
        protected new void Start ()
        {
            base.Start ();
            shapesInAction = MatchShape.Five;
        }
	
        public override void Run (GameObject target)
        {
            gameObject.SendMessage("SuperBonus", GetType().Name, SendMessageOptions.DontRequireReceiver);
            RuntimeInit(target);
        
            CollectTokens(target);
            SetupScore();
            DealDamage(destroyingTokens);
            DealDamage(new[] {gameObject, target});

            Destroy(gameObject);
            Destroy(target);
        }

        protected override void CollectTokens (GameObject target)
        {
            destroyingTokens = Tokens.Items.ToList();
            RemoveRunningBonuses();

            foreach (var destroyingToken in destroyingTokens) {
                destroyingToken.layer = LayerMask.NameToLayer("Animation");
            }
        }
    }
}
