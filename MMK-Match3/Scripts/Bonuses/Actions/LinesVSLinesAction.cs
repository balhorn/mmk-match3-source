﻿using System.Linq;
using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Actions/Lines vs lines action")]
    public class LinesVSLinesAction : TokenComponents.Action
    {
        protected new void Start()
        {
            base.Start ();
            shapesInAction = MatchShape.VerticalFour | MatchShape.HorizontalFour;
        }

        public override bool SuitableFor (MatchShape first, MatchShape second)
        {
            bool result = base.SuitableFor (first, second);
            if (!result) {
                return (shapesInAction & first & second) == MatchShape.HorizontalFour
                       || (shapesInAction & first & second) == MatchShape.VerticalFour;
            }

            return true;
        }

        public override void Run (GameObject target)
        {
            gameObject.SendMessage("SuperBonus", GetType().Name, SendMessageOptions.DontRequireReceiver);

            //owner.IsRunning = true;
            RuntimeInit(target);
        
            CollectTokens(target);
            SetupScore();
            DealDamage(destroyingTokens);

            Destroy(target);
            Destroy(gameObject);
        }

        protected override void CollectTokens (GameObject target)
        {
            destroyingTokens = shapesManager.Tokens.GetEntireRow(gameObject)
                .Union(shapesManager.Tokens.GetEntireColumn(gameObject))
                .Distinct()
                .ToList();

            //Prevent target's bonus from triggering
            //destroyingTokens.Remove(target);
            //destroyingTokens.Remove(gameObject);
        }
    }
}
