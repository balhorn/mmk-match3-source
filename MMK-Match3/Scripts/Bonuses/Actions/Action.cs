﻿using UnityEngine;
using System.Collections.Generic;
using MMK.Core;

namespace MMK.Match3.TokenComponents
{
    [RequireComponent(typeof(Bonus))]
    public abstract class Action : ElementComponent, IDamageDealer, IMatch3ScoreEntity
    {
        protected List<GameObject> destroyingTokens;
        protected MatchShape shapesInAction;

        protected Animator animator;
        protected SpriteRenderer spriteRenderer;

        protected Bonus owner;
        protected ShapesManager shapesManager;
        protected AnimationsManager animationsManager;

        public int Score
        {
            get; set;
        }
        public bool DependsOnCascade
        {
            get; set;
        }
        public Vector2 Coordinates
        {
            get; set;
        }
        public UnityEngine.Color TextColor
        {
            get; set;
        }

        protected GameObject target;

        protected virtual void Awake ()
        {
            owner = GetComponent<Bonus>();
            destroyingTokens = new List<GameObject>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            animator = GetComponent<Animator>();
        }

        protected void Start ()
        {
            shapesManager = ShapesManager.Instance;
            animationsManager = AnimationsManager.Instance;

            DamageAmount = 1;
            TextColor = Utilites.GetTokenColor(gameObject.GetColor());
        }

        protected virtual void SetupScore ()
        {
            DependsOnCascade = false;
            Coordinates = transform.position;
            foreach (var token in destroyingTokens.FindAll(elem => elem.HasComponent<Durability>())) {
                var targetDurability = token.Durability();
                if (targetDurability != null) {
                    Score += targetDurability.Score;
                }
            }

            ScoreManager.RecordScore(this);
        }

        protected void RuntimeInit (GameObject target)
        {
            this.target = target;
            target.GetComponent<Bonus>().IsRunning = true;
            owner.IsRunning = true;
        }

        public virtual bool SuitableFor (MatchShape first, MatchShape second)
        {
            return shapesInAction == (first | second);
        }

        protected void RemoveRunningBonuses ()
        {
            destroyingTokens.RemoveAll(token => token.HasComponent<Bonus>() && token.GetComponent<Bonus>().IsRunning);
        }

        public abstract void Run (GameObject target);
        protected abstract void CollectTokens (GameObject target);

        public int DamageAmount
        {
            get; set;
        }

        public int ScoreMultiplier
        {
            get; set;
        }

        public void DealDamage (IEnumerable<GameObject> objects)
        {
            foreach (var o in objects) {
                o.DealDamage(this);
            }
        }
    }


}
