﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MMK.Match3.TokenComponents;
using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Actions/Color bomb vs lines action")]
    public class ColorsVSLinesAction : Action
    {
        private List<GameObject> lines;
        private List<GameObject> collectedTokens;
        private GameObject linesBonus;

        protected new void Start ()
        {
            base.Start();

            lines = new List<GameObject>();
            collectedTokens = new List<GameObject>();

            shapesInAction = MatchShape.Five | MatchShape.HorizontalFour | MatchShape.VerticalFour;
        }

        public override bool SuitableFor (MatchShape first, MatchShape second)
        {
            return (shapesInAction & (first | second)) == (MatchShape.Five | MatchShape.HorizontalFour) 
                   || (shapesInAction & (first | second)) == (MatchShape.Five | MatchShape.VerticalFour);
        }

        public IEnumerator RunAnimation ()
        {
            yield return new WaitForSeconds(1f);
            TriggerLines();

            //TODO Temporary stub until we don't have super bonuses animation
            if (lines.Count == 0) {
                owner.Animation.EndAnimationImmidiate(234252);
            }

            Destroy(gameObject);
            Destroy(target);
        }

        public override void Run (GameObject target)
        {
            gameObject.SendMessage("SuperBonus", GetType().Name, SendMessageOptions.DontRequireReceiver);

            RuntimeInit(target);
            lines.Clear();
            collectedTokens.Clear();

            linesBonus = gameObject.HasComponent<LinesAction>() ? gameObject : target;

            CollectTokens(target);
            SpawnLines();

            StartCoroutine(RunAnimation());
        }

        private void TriggerLines ()
        {
            DealDamage(lines);
            DealDamage(new[] {target, gameObject});
        }

        private void SpawnLines ()
        {
            foreach (var item in collectedTokens) {
                if (item.IsBlocked()) {
                    InjectUnder(item);
                    continue;
                }

                PositionInfo info = new PositionInfo(item.GetComponent<Movement>());
                GameObject bonusPrefab = shapesManager.BonusManager.GetLineBonus(linesBonus.GetColor());
                GameObject bonus = shapesManager.CreateBonus(bonusPrefab, info);

                lines.Add(bonus);
                Destroy(item);
            }
        }

        protected override void CollectTokens (GameObject target)
        {
            TokenColor color = linesBonus.GetColor();
            collectedTokens = Tokens.Items
                .Where(token => token.GetColor() == color
                                && !token.HasComponent<Bonus>()
                                && !token.Undestroyable()
                )
                .ToList();

            collectedTokens.Remove(gameObject);
            collectedTokens.Remove(target);
        }

        private void InjectUnder (GameObject blockedGo)
        {

        }
    }
}
