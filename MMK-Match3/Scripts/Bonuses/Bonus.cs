﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

using MMK.Core;
using MMK.Match3;
using Utilites = MMK.Match3.Utilites;

namespace MMK.Match3.TokenComponents
{
    [AddComponentMenu("Match3/Active/Bonus")]
    [ElementEditorMenu("Active/Bonus")]
    public class Bonus : Active
    {
        [EnumFlags]
        public MatchShape matchShape;

        //Next two prorties are used for win bonus mechanics
        public float explosionDelay;
        public int   explosionOrder;

        [HideInInspector]
        public ShapesManager     shapesManager;

        public static readonly int BonusesAmount = 3;

        protected Action[]          availableActions;

        protected List<GameObject>  destroyingTokens;

        public TokenColor Color => gameObject.GetColor();

        // Use this for initialization
        protected override void Start ()
        {
            InitializeProperties();
            Utilites.FindGraphicsAndAnimation(gameObject, this);
            LoadSprite();
            LoadParticles();
            gameObject.SendMessage("OnSpawn", SendMessageOptions.DontRequireReceiver);
        }

        private void LoadSprite()
        {
            var sprite = Match3Player.Settings.Sprites.GetSprite(Color, matchShape);
            if (sprite != null) {
                Graphics.Sprite = sprite;
            }
        }

        private void LoadParticles ()
        {
            var particlesHolder = Match3Player.Settings.Sprites.GetParticles(Color, matchShape);
            if (particlesHolder != null) {
                Animation.Particles = particlesHolder;
            }
        }

        private void InitializeProperties ()
        {
            destroyingTokens = new List<GameObject> ();
            shapesManager = GameObject.FindGameObjectWithTag("ShapesManager").GetComponent<ShapesManager>();
            availableActions = GetComponents<MMK.Match3.TokenComponents.Action> ();
        }

        public void SelfDestroy()
        {
            IsRunning = false;
            Destroy(gameObject);
        }


        public void RunAppropriateAction (MatchShape shape, GameObject target)
        {
            IsRunning = true;
            availableActions
                .First(action => action.SuitableFor(shape, matchShape))
                .Run(target);
        }

        public override void Activate()
        {
            if (IsRunning) {
                return;
            }

            base.Activate();

            try {
                RunAppropriateAction(MatchShape.None, null);
            } catch (InvalidOperationException) {
                SelfDestroy();
            }
        }

        private void OnTokenDestroyed ()
        {
            Activate();
        }

        public override void SetupGraphics(Graphics graphics)
        {
            this.Graphics = graphics;
        }

        public override void SetupAnimation(Animation animation)
        {
            this.Animation = animation;
            this.Animation.graphics = Graphics;
        }
    }
}