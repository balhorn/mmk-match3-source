﻿using System;

namespace MMK.Match3
{
    /// <summary>
    /// Defines shape of the matched tokens
    /// </summary>
    [Flags]
    public enum MatchShape
    {
        /// <summary>
        /// No match
        /// </summary>
        None = 1,

        /// <summary>
        /// Three tokens in a row or in column
        /// </summary>
        Three = 1 << 1,

        /// <summary>
        /// Four tokens in a row
        /// </summary>
        HorizontalFour = 1 << 2,

        /// <summary>
        /// Four tokens in column
        /// </summary>
        VerticalFour = 1 << 3,

        /// <summary>
        /// Five tokens in a row or in column
        /// </summary>
        Five = 1 << 4,

        /// <summary>
        /// T shape of match
        ///
        /// o o o
        /// * o *
        /// * o *
        ///
        /// or
        ///
        /// o * *
        /// o o o
        /// o * *
        ///
        /// and so on
        ///
        /// </summary>
        T = 1 << 5,

        /// <summary>
        /// o o o
        /// o * *
        /// o * *
        ///
        /// or
        ///
        /// o o o
        /// * * o
        /// * * o
        ///
        /// and so on
        /// </summary>
        Corner = 1 << 6,

        /// <summary>
        /// Match in sqare
        ///
        /// o o
        /// o o
        ///
        /// or
        ///
        /// o
        /// o o
        /// o o
        ///
        /// and so on
        /// </summary>
        Square = 1 << 7
    }


    public enum SpriteLoadingMode
    {
        OnThisObject,
        OnChildObject
    }

    public enum GridLayer
    {
        Background,
        Base,
        Tokens,
        Attachments
    }

    /// <summary>
    /// Messages to be sent when some action with token occurs
    /// </summary>
    [Flags]
    public enum TokenEvent
    {
        /// <summary>
        /// Token finished it's movement animation
        /// </summary>
        OnMovementFinished = 1 << 1,

        /// <summary>
        /// All durability components on the token were disabled
        /// </summary>
        OnTokenDestroyed = 1 << 2,

        /// <summary>
        /// Some durability component on the token lost on durability point
        /// </summary>
        OnDurabilityLost = 1 << 3,

        /// <summary>
        /// Some durability component on the token was disabled
        /// </summary>
        OnDurabilityBroken = 1 << 4,

        /// <summary>
        /// Active component activated
        /// </summary>
        OnActivate = 1 << 5,

        /// <summary>
        /// Bonus has spawned
        /// </summary>
        OnSpawn = 1 << 6
    }

    public enum TokenColor
    {
        None = 0,
        Yellow = 1,
        Green = 2,
        Orange = 3,
        Blue = 4,
        Purple = 5,
        Red = 6,
        Random = 7,
    }

    public enum LevelType
    {
        CollectIngridients,
        Score,
        CollectElements
    }
}

