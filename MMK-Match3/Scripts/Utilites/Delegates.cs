﻿using UnityEngine;
using System.Collections;
using MMK.Match3;

namespace MMK.Match3
{
    public class Delegates
    {
        public delegate void ShapedMatch (ShapedBundle bundle);
        public delegate void TokenDestroyed (TokenColor color, PositionInfo info, IDamageDealer dealer);
    }
}