﻿using System.Collections.Generic;
using System.Linq;
using MMK.Match3;
using MMK.Match3.TokenComponents;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MMK.Match3
{
    internal class ShuffleCorrector : IMatchesCorrector
    {
        private readonly List<GameObject> tokens;
        private readonly ITokensCollection tokensCollection;

        private GameObject initialToken;
        private TokenColor withoutColor;
        private bool       failed;

        public ShuffleCorrector(List<GameObject> tokens)
        {
            this.tokens = tokens;
            tokensCollection = ElementComponent.Tokens;
        }

        public void EnsureNoMatches(PositionInfo info)
        {
            withoutColor = tokensCollection[info].GetColor();
            initialToken = tokensCollection[info];

            while (!failed && SmallCross(withoutColor, info)) {
                ChangeToken(info);
            }

            while (!failed && BigCross(withoutColor, info)) {
                ChangeToken(info);
            }

            tokens.Remove(tokensCollection[info]);
        }

        private void ChangeToken (PositionInfo info)
        {
            var newToken = ChooseAnotherToken(withoutColor);
            if (newToken != null) {
                tokensCollection[info] = newToken;
                withoutColor = tokensCollection[info].GetColor();
            } else {
                tokensCollection[info] = initialToken;
                failed = true;
            }
        }

        private bool SmallCross (TokenColor color, PositionInfo position)
        {
            var top    = position + GravityVector.DirectionUp;
            var bottom = position + GravityVector.DirectionDown;
            var left   = position + GravityVector.DirectionLeft;
            var right  = position + GravityVector.DirectionRight;

            bool horizontal = !left.Overextend() && !right.Overextend()
                              && tokensCollection[left].GetColor().Equals(color)
                              && tokensCollection[right].GetColor().Equals(color);

            bool vertical = !top.Overextend() && !bottom.Overextend()
                              && tokensCollection[top].GetColor().Equals(color)
                              && tokensCollection[bottom].GetColor().Equals(color);

            return horizontal || vertical;
        }

        private bool BigCross (TokenColor color, PositionInfo position)
        {
            bool result = false;
            foreach (var direction in new []
            {
                GravityVector.DirectionUp,
                GravityVector.DirectionDown,
                GravityVector.DirectionLeft,
                GravityVector.DirectionRight
            }) {
                var furtherPosition = position + direction;
                var mostFarPosition = furtherPosition + direction;
                result |= !furtherPosition.Overextend() && !mostFarPosition.Overextend()
                     && tokensCollection[furtherPosition].GetColor() == color
                     && tokensCollection[mostFarPosition].GetColor() == color;
            }

            return result;
        }

        private GameObject ChooseAnotherToken (TokenColor exeptColor)
        {
            var tokensOfSuchColor = new List<GameObject>();
            for (int count = 0; count < 5; ++count) {
                var color = Match3.Utilites.GetRandomColor(exeptColor);
                tokensOfSuchColor = tokens
                    .Where(token => token != null && token.GetColor() == color)
                    .ToList();

                if (tokensOfSuchColor.Count > 0) {
                    break;
                }
            }

            if (tokensOfSuchColor.Count == 0) {
                return null;
            }

            var randomIndex = Random.Range(0, tokensOfSuchColor.Count);
            var choosenToken = tokensOfSuchColor[randomIndex];

            return choosenToken;
        }
    }
}