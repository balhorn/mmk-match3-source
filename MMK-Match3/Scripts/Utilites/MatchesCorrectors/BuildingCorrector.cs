﻿using MMK.Match3;

namespace MMK.Match3
{
    internal class BuildingCorrector : IMatchesCorrector
    {
        private readonly TokenColor[,] colors;
        private readonly GameObjectsSerializedMatrix elements;

        public BuildingCorrector(TokenColor[,] colors, GameObjectsSerializedMatrix elements)
        {
            this.colors = colors;
            this.elements = elements;
        }

        public void EnsureNoMatches(PositionInfo info)
        {
            EnsureNoMatches(info.row, info.column);
        }

        private void EnsureNoMatches (int row, int column)
        {
            var withoutColor = colors[row, column];

            while (SmallCross(withoutColor, row, column)) {
                colors[row, column] = Match3.Utilites.GetRandomColor(withoutColor);
                withoutColor = colors[row, column];
            }

            while (BigCross(withoutColor, row, column)) {
                colors[row, column] = Match3.Utilites.GetRandomColor(withoutColor);
                withoutColor = colors[row, column];
            }
        }

        private bool BigCross (TokenColor withoutColor, int row, int column)
        {
            bool top = row < LevelConfig.rows - 2
                            && elements[row + 1, column] != null && elements[row + 2, column] != null
                            && colors[row + 1, column] == withoutColor
                            && colors[row + 2, column] == withoutColor;

            bool bottom = row > 1
                            && elements[row - 1, column] != null && elements[row - 2, column] != null
                            && colors[row - 1, column] == withoutColor
                            && colors[row - 2, column] == withoutColor;

            bool left = column > 1
                            && elements[row, column - 1] != null && elements[row, column - 2] != null
                            && colors[row, column - 1] == withoutColor
                            && colors[row, column - 2] == withoutColor;

            bool right = column < LevelConfig.columns - 2
                            && elements[row, column + 1] != null && elements[row, column + 2] != null
                            && colors[row, column + 1] == withoutColor
                            && colors[row, column + 2] == withoutColor;

            return top || bottom || left || right;
        }

        private bool SmallCross (TokenColor withoutColor, int row, int column)
        {
            bool vertical = row > 0 && row < LevelConfig.rows - 1
                            && elements[row + 1, column] != null && elements[row - 1, column] != null
                            && colors[row + 1, column] == withoutColor
                            && colors[row - 1, column] == withoutColor;

            bool horizontal = column > 0 && column < LevelConfig.columns - 1
                              && elements[row, column + 1] != null && elements[row, column - 1] != null
                              && colors[row, column + 1] == withoutColor
                              && colors[row, column - 1] == withoutColor;

            return vertical || horizontal;
        }
    }
}
