﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Net;
using MMK.Core;
using MMK.Match3;
using UnityEngine;

using MMK.Match3.TokenComponents;

using Animation = MMK.Match3.TokenComponents.Animation;
using Graphics = MMK.Match3.TokenComponents.Graphics;

namespace MMK.Match3
{
    public static class Utilites
    {
        internal static int CalculateAdditionalTurnsCost (int lossesAlready)
        {
            if (lossesAlready < 0) {
                return 0;
            }

            if (lossesAlready == 0) {
                return Match3Player.Settings.additionalTurnCost;
            }

            var d = Match3Player.Settings.baseCostStep + Match3Player.Settings.incrementStepBy * lossesAlready - 1;
            return CalculateAdditionalTurnsCost(lossesAlready - 1) + d;
        }

        /// <summary>
        /// Copy all public fields of the given component to  the gameobject;
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static ElementComponent CopyComponent (ElementComponent source, GameObject target)
        {
            var type = source.GetType();
            var fields = type.GetFields(BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance);

            var added = target.AddComponent(type);
            foreach (var field in fields) {
                field.SetValue(added, field.GetValue(source));
            }

            return added as ElementComponent;
        }

        internal static Texture GetTokenTexture (GameObject token)
        {
            var graphics = token.GetComponent<MMK.Match3.TokenComponents.Graphics>();
            if (graphics != null) {
                return graphics.Sprite?.texture;
            }

            var renderer = token.GetComponent<SpriteRenderer>();
            return renderer?.sprite?.texture;
        }


        /// <summary>
        /// Sets up links to the Graphics and Animation component.
        /// It's very important to use it in the Awake() function of the IGraphicsHandler component!
        ///
        /// Utilites.FindGraphicsAndAnimation(gameObject, this);
        ///
        /// </summary>
        /// <param name="token">Game object to search for components in</param>
        /// <param name="handler">Component to add links to</param>
        public static void FindGraphicsAndAnimation (GameObject token, IGraphicsHandler handler)
        {
            try {
                handler.SetupGraphics(token.GetComponents<Graphics>().First(graphics => graphics.Equals(handler.Graphics)));
            } catch (InvalidOperationException) {
            }

            try {
                handler.SetupAnimation(token.GetComponents<Animation>().First(animation => animation.Equals(handler.Animation)));
            } catch (InvalidOperationException) {
            }

        }

        /// <summary>
        /// Generates random TokenColor value exept None, Random and the given one
        /// </summary>
        /// <param name="butThisOne">Color to be excluded from random choice. Random by default</param>
        /// <returns></returns>
        public static TokenColor GetRandomColor (TokenColor butThisOne = TokenColor.Random)
        {
            TokenColor color = butThisOne;
            var enums = Enum.GetValues (typeof(TokenColor));

            do {
                color = (TokenColor) enums.GetValue (UnityEngine.Random.Range(0, enums.Length));
            } while (color == butThisOne || color == TokenColor.None || color == TokenColor.Random );

            return color;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="color"></param>
        /// <returns>RGB color coresponding to the given TokenColor</returns>
        public static UnityEngine.Color GetTokenColor(TokenColor color)
        {
            switch (color) {
                case TokenColor.Yellow:
                    return Constants.YellowColor;
                case TokenColor.Green:
                    return Constants.GreenColor;
                case TokenColor.Blue:
                    return Constants.BlueColor;
                case TokenColor.Orange:
                    return Constants.OrangeColor;
                case TokenColor.Purple:
                    return Constants.PurpleColor;
                case TokenColor.Red:
                    return Constants.RedColor;
                default:
                    return UnityEngine.Color.white;
            }
        }

        public static bool AreVerticalOrHorizontalNeighbors (Movement s1, Movement s2)
        {
            return (s1.Column == s2.Column ||
                    s1.Row == s2.Row)
                   && Mathf.Abs(s1.Column - s2.Column) <= 1
                   && Mathf.Abs(s1.Row - s2.Row) <= 1;
        }
    }
}