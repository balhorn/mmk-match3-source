﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Match3;
using MMK.Match3.TokenComponents;
using UnityEngine;
using Color = UnityEngine.Color;

namespace MMK.Match3
{
    /// <summary>
    /// Class designed for defining shape of the tokens matches. Being used with bonuses
    /// </summary>
    public class ShapedBundle : IMatch3ScoreEntity
    {
        private readonly List<Movement> movements;

        public TokenColor Color { get; set; }
        public MatchShape Shape { get; set; }
        public GameObject CentralToken { get; set; }

        public int Score { get; set; }
        public bool DependsOnCascade { get; set; }
        public Vector2 Coordinates { get; set; }
        public Color TextColor { get; set; }

        public static event Delegates.ShapedMatch OnShapedMatch;

        public ShapedBundle (TokenColor color, MatchShape shape, GameObject centralToken)
        {
            Color = color;
            Shape = shape;
            CentralToken = centralToken;
            Coordinates = Camera.main.WorldToScreenPoint(centralToken.transform.position);
            TextColor = Match3.Utilites.GetTokenColor(color);

            Match3SoundManager.Match();
            SetupScore();
            FireEvent();
        }

        public ShapedBundle (IEnumerable<GameObject> coloredTokens, TokenColor color, bool fireEvent = true)
        {
            movements = coloredTokens
                .Where(token => token != null)
                .Select(token => token.GetComponent<Movement>())
                .ToList();

            Color = color;
            Match3SoundManager.Match();
            CalculateMatchShape();

            //Coordinates = Camera.main.WorldToScreenPoint(CentralToken.transform.position);
            Coordinates = CentralToken.transform.position;
            TextColor = Match3.Utilites.GetTokenColor(color);

            SetupScore();
            if (fireEvent) {
                FireEvent();
            }
        }

        private void SetupScore ()
        {
            DependsOnCascade = true;
            switch (Shape) {
                case MatchShape.None:
                    Score = 0;
                    break;
                case MatchShape.Three:
                    Score = Match3Player.Settings.matchThreeScore;
                    break;
                case MatchShape.HorizontalFour:
                    goto case MatchShape.VerticalFour;
                case MatchShape.VerticalFour:
                    Score = Match3Player.Settings.matchFourScore;
                    break;
                case MatchShape.Five:
                    Score = Match3Player.Settings.matchFiveScore;
                    break;
                case MatchShape.T:
                    Score = Match3Player.Settings.matchTScore;
                    break;
                case MatchShape.Corner:
                    Score = Match3Player.Settings.matchCornerScore;
                    break;
                case MatchShape.Square:
                    Score = Match3Player.Settings.matchSquareScore;
                    break;
            }

            ScoreManager.RecordScore(this);
        }

        /// <summary>
        /// Calculates the match shape and position of bonus to be spawned
        /// </summary>
        private void CalculateMatchShape()
        {
            int horizontalCount = GetHorizontalCount();
            int verticalCount = GetVerticalCount();
            //Need to determine the position of bonus to be spawned
            float latestFallenTime = movements.Select(movement => movement.LastMoved).Max();

            if (horizontalCount == 1 && verticalCount == 1) {
                Shape = MatchShape.None;
            }
            //tokens matched in one line
            else if (horizontalCount == 1 || verticalCount == 1) {
                if (horizontalCount == 3 || verticalCount == 3) {
                    Shape = MatchShape.Three;
                    CentralToken = verticalCount == 1 ? GetMiddleTokenHorizontally() : GetMiddleTokenVertically();
                } else if (horizontalCount == 4 || verticalCount == 4) {
                    Shape = horizontalCount == 4 ? MatchShape.HorizontalFour : MatchShape.VerticalFour;
                    //finding the position of the bonus
                    CentralToken = movements.First(movement => movement.LastMoved >= latestFallenTime).gameObject;
                } else {
                    Shape = MatchShape.Five;
                    //if we have match of 5 then bonus always spawns in the middle
                    CentralToken = verticalCount == 1 ? GetMiddleTokenHorizontally() : GetMiddleTokenVertically();
                }
            } else {
                if (verticalCount == 5 || horizontalCount == 5) {
                    //five is more strong combination, so it replases T or Corner
                    Shape = MatchShape.Five;
                    CentralToken = verticalCount == 1 ? GetMiddleTokenHorizontally() : GetMiddleTokenVertically();
                } else {
                    Shape = CornerOrT();
                }
            }
        }

        /// <summary>
        /// Determines whether match has shape of T or Corner
        /// </summary>
        /// <returns>T or corner shape</returns>
        private MatchShape CornerOrT()
        {
            var columns = movements.Select(movement => movement.Column).Distinct();
            var rows = movements.Select(movement => movement.Row).Distinct();

            try {
                var intersection = GetVerticalLine().Intersect(GetHorizontalLine(), new TokensEqualityComparer()).ElementAt(0).GetComponent<Movement>();
                bool isT = intersection.Row != rows.Max() && intersection.Row != rows.Min() || intersection.Column != columns.Min() && intersection.Column != columns.Max();

                CentralToken = intersection.gameObject;

                return isT ? MatchShape.T : MatchShape.Corner;
            } catch (System.Exception) {
                return MatchShape.None;
            }
        }

        private GameObject GetMiddleTokenVertically()
        {
            try {
                int middleRow = (int) movements.Select(movement => movement.Row).Average();
                return movements.First(movement => movement.Row == middleRow).gameObject;
            } catch (InvalidOperationException) {
                return null;
            }
        }

        private GameObject GetMiddleTokenHorizontally()
        {
            try {
                int middleColumn = (int) movements.Select(movement => movement.Column).Average();
                return movements.First(movement => movement.Column == middleColumn).gameObject;
            } catch (InvalidOperationException) {
                return null;
            }
        }

        private int GetHorizontalCount()
        {
            int[] counts = new int[LevelConfig.rows];
            foreach (var movement in movements) {
                counts[movement.Row]++;
            }

            return counts.Max();
        }

        private List<GameObject> GetHorizontalLine()
        {
            int[] counts = new int[LevelConfig.rows];
            foreach (var movement in movements) {
                counts[movement.Row]++;
            }

            int maxIndex = GetMaxIndex(counts);
            return movements.Where(movement => movement.Row == maxIndex).Select(movement => movement.gameObject).ToList();
        }

        private List<GameObject> GetVerticalLine()
        {
            int[] counts = new int[LevelConfig.columns];
            foreach (var movement in movements) {
                counts[movement.Column]++;
            }

            int maxIndex = GetMaxIndex(counts);
            return movements.Where(movement => movement.Column == maxIndex).Select(movement => movement.gameObject).ToList();
        }

        /// <summary>
        /// Helper method to get the index of the maximum element of the array
        /// </summary>
        /// <returns>The max index.</returns>
        /// <param name="collection">Collection.</param>
        private static int GetMaxIndex(int[] collection)
        {
            int maxIndex = 0;
            for (int i = 0; i < collection.Length; i++) {
                if (collection[i] > collection[maxIndex]) {
                    maxIndex = i;
                }
            }

            return maxIndex;
        }

        private int GetVerticalCount()
        {
            int[] counts = new int[LevelConfig.columns];
            foreach (var movement in movements) {
                counts[movement.Column]++;
            }

            return counts.Max();
        }

        private void FireEvent()
        {
            if (OnShapedMatch != null) {
                OnShapedMatch(this);
            }
        }

        public override string ToString()
        {
            return $"Bundle of {Color} color. Totally {movements.Count} tokens. Shape is {Shape}";
        }
    }


    internal class TokensEqualityComparer : IEqualityComparer<GameObject>
    {
        public bool Equals(GameObject x, GameObject y)
        {
            if (!x.HasComponent<Movement>() || !y.HasComponent<Movement>()) {
                return false;
            }

            var xMovement = x.GetComponent<Movement>();
            var yMovement = y.GetComponent<Movement>();

            if (xMovement == null || yMovement == null) {
                return false;
            }

            return xMovement.Row == yMovement.Row && xMovement.Column == yMovement.Column;
        }

        public int GetHashCode(GameObject obj)
        {
            var movement = obj.GetComponent<Movement>();

            return movement != null ? 10 * movement.Row + movement.Column : obj.GetHashCode();
        }
    }
}