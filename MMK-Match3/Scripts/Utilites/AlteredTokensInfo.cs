﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MMK.Match3
{
    public class AlteredTokensInfo : IEnumerable<GameObject>
    {
        private HashSet<GameObject> newTokens { get; set; }
        public float MaxDistance => distances.Max();

        private readonly HashSet<float> distances = new HashSet<float>();

        public void AddDistance (float distance)
        {
            distances.Add(distance);
        }

        public void AddToken(GameObject go)
        {
            newTokens.Add(go);
        }

        public void AddRange (IEnumerable<GameObject> objects)
        {
            foreach (var gameObject in objects) {
                newTokens.Add(gameObject);
            }
        }

        public AlteredTokensInfo()
        {
            newTokens = new HashSet<GameObject>();
            distances.Add(0);
        }

        public void Clear ()
        {
            newTokens.Clear();
        }

        public static AlteredTokensInfo operator + (AlteredTokensInfo one, AlteredTokensInfo another)
        {
            var newInfo = new AlteredTokensInfo();
            newInfo.AddRange(one);
            newInfo.AddRange(another);
            newInfo.AddDistance(one.MaxDistance);
            newInfo.AddDistance(another.MaxDistance);

            return newInfo;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<GameObject> GetEnumerator()
        {
            return newTokens.GetEnumerator();
        }
    }
}
