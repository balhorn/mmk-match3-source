﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMK.Match3.TokenComponents;
using MMK.Core;
using MMK.Match3;
using UnityEngine;

namespace MMK.Match3
{
    public static class Match3Extensions
    {
        /// <summary>
        /// Check whether tooken can be destroyed
        /// </summary>
        /// <param name="token"></param>
        /// <returns>True if token has at least one enabled durability components</returns>
        public static bool Undestroyable (this GameObject token)
        {
            return !token.HasComponent<Durability>()
                || token.GetComponents<Durability>().All(component => !component.enabled);
        }

        /// <summary>
        /// Checks whether token can be moved 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>True if obj has at least one enabled movement component</returns>
        public static bool IsBlocked (this GameObject obj)
        {
            return obj == null || !obj.HasComponent<Movement>() || obj.GetComponent<Movement>().enabled == false;
        }

        /// <summary>
        /// Checks whether token can be matched
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>True if obj has at least one enabled Match component</returns>
        public static bool IsMatch (this GameObject obj)
        {
            return obj != null && obj.HasComponent<Match>() && obj.GetComponent<Match>().enabled;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>True if sprite renderer is enabled</returns>
        public static bool IsVisible (this GameObject obj)
        {
            return obj.HasComponent<SpriteRenderer>() && obj.GetComponent<SpriteRenderer>().enabled;
        }

        /// <summary>
        /// Gets color value from Color component
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Color value if Color component exists and TokenColor.None otherwise</returns>
        public static TokenColor GetColor (this GameObject obj)
        {
            return obj != null && obj.HasComponent<MMK.Match3.TokenComponents.Color>() 
                ? obj.GetComponent<MMK.Match3.TokenComponents.Color>().Value
                : TokenColor.None;
        }

        public static bool TheSameColor (this GameObject one, GameObject another)
        {
            var b = one.HasComponent<MMK.Match3.TokenComponents.Color>()
                && another.HasComponent<MMK.Match3.TokenComponents.Color>()
                && one.GetColor().Equals(another.GetColor());

            return b;
        }

        /// <summary>
        /// Matches two tokens. If match is successfull, both tokens are most likely to be destroyed
        /// </summary>
        /// <param name="token"></param>
        /// <param name="target"></param>
        /// <returns>True if match is successfull and false otherwise</returns>
        public static bool Match (this GameObject token, GameObject target)
        {
            var component = token.GetComponent<Match>();
            return component != null && component.DoMatch(target);
        }

        /// <summary>
        /// Damages Durability component with the biggest order.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="damageDealer">Component to provide damage</param>
        /// <returns>Whether damage dealing completed without errors</returns>
        public static bool DealDamage (this GameObject obj, IDamageDealer damageDealer)
        {
            if (obj == null || obj.Undestroyable()) {
                return false;
            }

            try {
                var durability = obj.GetComponents<Durability>()
                    .First(component => component.enabled && component.IsFirst);
                durability.TakeDamage(damageDealer);

                return true;
            } catch (InvalidOperationException e) {
                Debug.Log("Error while dealing damage. Returning");
                Debug.Log(e.Message);

                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Durability with the highest order</returns>
        public static Durability Durability (this GameObject obj)
        {
            return obj.GetComponents<Durability>().FirstOrDefault(component => component.IsFirst && component.enabled);
        }
    }
}


