﻿using UnityEngine;

namespace MMK.Match3
{
    internal class SquareMatrix <TDataType> where TDataType : struct
    {
        private TDataType[,] matrix ;
        private float       determinant = float.MinValue;

        public TDataType this [int row, int column] => matrix[row, column];

        public float Determinant
        {
            get
            {
                if (Mathf.Approximately(determinant, float.MinValue)) {
                    GaussianDeterminant ();
                }

                return determinant;
            }
        }

        public static SquareMatrix<int> TVerticalTop => TUpper;
        public static SquareMatrix<int> TVerticalLower => TLower;
        public static SquareMatrix<int> THorizontalLeft => TUpper.Transpose ();
        public static SquareMatrix<int> THorizontalRight => TLower.Transpose ();

        public static SquareMatrix<int> CornerTopLeft => CornerTop;
        public static SquareMatrix<int> CornerLowerLeft => CornerLower;
        public static SquareMatrix<int> CornerTopRight => CornerLower.Transpose ();
        public static SquareMatrix<int> CornerLowerRight => CornerTop.TransposeInverse ();

        private static readonly SquareMatrix<int> TUpper = new SquareMatrix<int> (new[,] {{1,1,1}, {0,1,0}, {0,1,0}});
        private static readonly SquareMatrix<int> TLower = new SquareMatrix<int> (new[,] {{0,1,0}, {0,1,0}, {1,1,1}});

        private static readonly SquareMatrix<int> CornerTop   = new SquareMatrix<int> (new[,] {{1,1,1}, {1,0,0}, {1,0,0}});
        private static readonly SquareMatrix<int> CornerLower = new SquareMatrix<int> (new[,] {{1,0,0}, {1,0,0}, {1,1,1}});

        public SquareMatrix<TDataType> Transpose()
        {
            for (int i = 0; i < matrix.Rank; i++) {
                for (int j = 0; j < matrix.GetLength(i); j++) {
                    if (i == j) {
                        continue;
                    }

                    TDataType c = matrix [i, j];
                    matrix [i, j ] = matrix [j , i];
                    matrix [j, i] = c;
                }
            }

            return this;
        }

        private SquareMatrix<TDataType> TransposeInverse()
        {
            for (int i = 0; i < matrix.Rank; i++) {
                for (int j = matrix.GetLength(i) - 1; j >= 0; j--) {
                    if (i == matrix.GetLength(i) - 1 - j) { 
                        continue;
                    }

                    TDataType c = matrix [i, j];
                    matrix [i, j ] = matrix [j , i];
                    matrix [j, i] = c;
                }
            }

            return this;
        }

        private void GaussianDeterminant()
        {

        }

        public SquareMatrix (int dimension)
        {
            matrix = new TDataType[dimension, dimension];
        }

        public SquareMatrix (TDataType[,] matrix)
        {
            this.matrix = matrix;
        }
    }
}
