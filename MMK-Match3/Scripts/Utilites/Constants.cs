﻿using System.IO;
using UnityEngine;

namespace MMK.Match3
{
    public static class Constants
    {
        public static readonly float AnimationDuration = 0.2f;
        public static readonly float MoveAnimationMinDuration = 0.1f;

        public static readonly float WaitBeforePotentialMatchesCheck = 2f;
        public static readonly float OpacityAnimationFrameDelay = 0.05f;

        public static readonly int MinimumScore = 20;

        public static readonly int MaximumPreboostersPerLevel = 3;
        public static readonly int MaximumLifes = 5;
        public static readonly int MinimumMatches = 3;
        public static readonly int DefaultPreboostersNumber = 2;

        public static readonly Color YellowColor = new Color(0.941f, 0.9178f, 0.055f);
        public static readonly Color OrangeColor = new Color(0.929f, 0.663f, 0.337f);
        public static readonly Color GreenColor = new Color(0.69f, 0.933f, 0.310f);
        public static readonly Color BlueColor = new Color(0.376f, 0.851f, 0.925f);
        public static readonly Color PurpleColor = new Color(0.819f, 0.369f, 0.882f);
        public static readonly Color RedColor = new Color(1f, 0f, 0f);

        public static readonly int MaximumRows = 9;
        public static readonly int MaximumColumns = 9;
        public static readonly int MaximumScoreMultiplier = 3;

        public const string ElementsEditorScene = "Elements editor";

        public const string MapSceneName = "MapM3";

        public const string MMKMatch3Root = Core.Constants.MMKRoot + "Match3/";
        public const string SpritesFolder = "Sprites";
        public const string PrefabsFolder = "Prefabs";
        public const string LevelsFolder = "Levels";
        public const string LevelsFolderPath = MMKMatch3Root + "Resources/" + LevelsFolder;

        public static readonly string BackgroundFolder = Path.Combine(PrefabsFolder, "Background");
        public static readonly string HolesForlder = Path.Combine(PrefabsFolder, "Holes");
        public static readonly string ElementsFolder = Path.Combine(PrefabsFolder, "Tokens");
        public static readonly string CellsFolder = Path.Combine(PrefabsFolder, "Base");
        public static readonly string AttachmentsFolder = Path.Combine(PrefabsFolder, "Attachments");
        public static readonly string PreboostersFolder = Path.Combine(PrefabsFolder, "Preboosters");
    }
}



