﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using MMK.Match3.TokenComponents;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MMK.Match3
{
    public static class ElementFactory
    {
        internal static List<GameObject> regularElements = new List<GameObject>();

        internal static GameObject GetElement (string name)
        {
            var path = Path.Combine(Constants.ElementsFolder, name);
            return LoadAndInstantiate(path);
        }

        internal static GameObject GetRandomToken ()
        {
            var path = Path.Combine(Constants.ElementsFolder, "Random token");
            return LoadAndInstantiate(path);
        }

        internal static GameObject GetCell (string name)
        {
            var path = Path.Combine(Constants.CellsFolder, name);
            return LoadAndInstantiate(path);
        }

        /// <summary>
        /// Loads Empty prefab in the Resources/Prefabs folder and returns it instantiated
        /// </summary>
        /// <returns></returns>
        public static GameObject LoadEmptyPrefab ()
        {
            var path = Path.Combine(Constants.PrefabsFolder, "Empty prefab");
            return LoadAndInstantiate(path);
        }

        private static GameObject LoadAndInstantiate (string path)
        {
            var prefab = Resources.Load<GameObject>(path);
            return prefab != null ? Object.Instantiate(prefab) : null;
        }

        internal static GameObject LoadAttachment (string attachmentName)
        {
            var path = Path.Combine(Constants.AttachmentsFolder, attachmentName);
            return Resources.Load<GameObject>(path);
        }

        /// <summary>
        /// Attaches the specified attachment to the gameobject.
        /// </summary>
        /// <param name="target">Game Object to attach to</param>
        /// <param name="attachmentName">Name of the attachment prefab. It should be placed the Resources/Prefabs/Attachments folder</param>
        /// <returns></returns>
        public static GameObject Attach (GameObject target, string attachmentName)
        {
            if (target == null) {
                return null;
            }

            var attachment = LoadAttachment(attachmentName);
            if (attachment == null) {
                return null;
            }

            foreach (var component in attachment.GetComponents<ElementComponent>()) {
                var comp = Utilites.CopyComponent(component, target);
                var type = comp.GetType();

                foreach (var property in type.GetProperties()
                                         .Where(property => property.CanRead && property.CanWrite)
                                         .Where(property => property.DeclaringType.IsSubclassOf(typeof(ElementComponent))
                                                         || property.DeclaringType == typeof(ElementComponent))
                                         .Where(property => property.PropertyType != typeof(GameObject))) {
                    property.SetValue(comp, property.GetValue(component, null), null);
                }

                foreach (var field in type
                                      .GetFields(BindingFlags.Instance | BindingFlags.Public)
                                      .Where(field => field.FieldType == typeof(GameObject))) {

                    var obj = (GameObject) field.GetValue(comp);

                    if (obj == null) {
                        continue;
                    }

                    if (obj != attachment) {
                        var originalObject = field.GetValue(comp) as GameObject;
                        var newObject =
                            Object.Instantiate(originalObject);

                        newObject.transform.parent = target.transform;
                        newObject.transform.localPosition = originalObject.transform.localPosition;

                        field.SetValue(comp, newObject);
                    } else {
                        field.SetValue(comp, target);
                    }
                }
            }

            return target;
        }
    }
}
