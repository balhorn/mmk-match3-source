﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMK.Match3.TokenComponents;
using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    internal class MatchPattern : IEquatable<MatchPattern>
    {
        #region Static members

        private static readonly Dictionary<MatchShape, int> Priorities = new Dictionary<MatchShape, int> {
            {MatchShape.Three, 0},
            {MatchShape.HorizontalFour, 1},
            {MatchShape.VerticalFour, 1},
            {MatchShape.T, 2},
            {MatchShape.Corner, 2},
            {MatchShape.Five, 3}
        };

        private static ITokensCollection TokensCollection => ElementComponent.Tokens;

        public static MatchPattern Find(PositionInfo start, GravityVector direction)
        {
            var pattern = new MatchPattern(start, direction);
            return pattern.tokens.Count != 0 ? pattern : null;
        }

        #endregion

        public int Priority => doubleBonus ? 10 : Priorities[Shape];

        public TokenColor Color { get; private set; }
        public MatchShape Shape { get; private set; }
        public List<GameObject> tokens = new List<GameObject>();

        public bool IsValid
        {
            get { return tokens.All(token => token != null); }
        }

        private bool doubleBonus;

        private bool oneLeft;
        private bool oneRight;
        private bool twoLeft;
        private bool twoRight;
        private bool twoForward;

        private MatchPattern (PositionInfo start, GravityVector direction)
        {
            var origin = TokensCollection[start];
            tokens.Add(origin);
            Color = origin.GetColor();

            if (CheckBlocking(start, direction) 
             || CheckBonus(start, direction)) {
                return;
            }

            CheckLeft(start, direction);
            CheckRight(start, direction);
            CheckForvard(start, direction);

            if (twoLeft && twoRight) {
                Shape = MatchShape.Five;
                return;
            }

            if (oneLeft && oneRight && twoForward
                || twoLeft && oneRight && twoForward
                || twoRight && oneLeft && twoForward) {
                Shape = MatchShape.T;
                return;
            }

            if (twoLeft && twoForward || twoRight && twoForward) {
                Shape = MatchShape.Corner;
                return;
            }

            if (oneLeft && twoRight || oneRight && twoLeft) {
                Shape = MatchShape.HorizontalFour;
                return;
            }

            if (twoRight || twoLeft || oneRight && oneLeft) {
                Shape = MatchShape.Three;
                return;
            }

            if (CheckThreeInARow(start, direction)) {
                tokens.Add(origin);
                Shape = MatchShape.Three;
                return;
            }

            tokens.Clear();
        }

        public bool StrongerThen (MatchPattern other)
        {
            if (other == null) {
                return true;
            }

            return Priority > other.Priority;
        }

        private bool CheckBlocking(PositionInfo start, GravityVector direction)
        {
            var blocked = TokensCollection[start + direction].IsBlocked() || !TokensCollection[start + direction].IsMatch();

            if (blocked) {
                tokens.Clear();
            }

            return blocked;
        }

        private bool CheckBonus(PositionInfo start, GravityVector direction)
        {
            var startToken = TokensCollection[start];
            var oppositeToken = TokensCollection[start + direction];

            var bonusMatch = startToken.GetComponent<BonusMatch>();
            doubleBonus = oppositeToken.HasComponent<Bonus>() && startToken.HasComponent<Bonus>()
                || bonusMatch != null && bonusMatch.matchableWithTokens;
            if (!doubleBonus) {
                return false;
            }

            tokens.Add(oppositeToken);
            return true;
        }

        private void CheckLeft (PositionInfo start, GravityVector direction)
        {
            var leftDirection = direction.Left;
            var leftPosition = start + leftDirection;
            var leftToken = TokensCollection[leftPosition];

            if (!(oneLeft = leftToken.GetColor().Equals(Color) && leftToken.IsMatch())) {
                return;
            }
            tokens.Add(leftToken);

            var furtherLeftToken = TokensCollection[leftPosition + leftDirection.Left];
            if (!(twoLeft = furtherLeftToken.GetColor().Equals(Color) && furtherLeftToken.IsMatch())) {
                return;
            }

            oneLeft = false;
            tokens.Add(furtherLeftToken);
        }

        private void CheckRight (PositionInfo start, GravityVector direction)
        {
            var rightDirection = direction.Right;
            var rightPosition = start + rightDirection;
            var rightToken = TokensCollection[rightPosition];

            if (!(oneRight = rightToken.GetColor().Equals(Color) && rightToken.IsMatch())) {
                return;
            }
            tokens.Add(rightToken);

            var furtherRightPosition = rightPosition + rightDirection.Right;
            var furtherRightToken = TokensCollection[furtherRightPosition];
            if (!(twoRight = furtherRightToken.GetColor().Equals(Color) && furtherRightToken.IsMatch())) {
                return;
            }

            oneRight = false;
            tokens.Add(furtherRightToken);
        }

        private void CheckForvard (PositionInfo start, GravityVector direction)
        {
            var forwardPosition = (start + direction) + direction;
            var forwardToken = TokensCollection[forwardPosition];
            var furtherToken = TokensCollection[forwardPosition + direction];

            var forwardValid = forwardToken.GetColor().Equals(Color) && forwardToken.IsMatch();
            var furtherValid = furtherToken.GetColor().Equals(Color) && furtherToken.IsMatch();

            if (!(twoForward = forwardValid && furtherValid)) {
                return;
            }

            tokens.AddRange(new [] {forwardToken, furtherToken});
        }

        private bool CheckThreeInARow (PositionInfo start, GravityVector direction)
        {
            var pos = start + direction.Revert;

            CheckLeft(pos, direction);

            if (twoLeft) {
                return true;
            }

            tokens.Clear();

            CheckRight(pos, direction);

            if (twoRight) {
                return true;
            }

            tokens.Clear();

            CheckForvard(start, direction);

            return twoForward;
        }

        public override string ToString()
        {
            if (tokens.Count == 0) {
                return "No match";
            }

            return doubleBonus 
                ? "Double bonus match" 
                : $"Match is going to be {Shape} match of {Color} color";
        }

        public bool Equals(MatchPattern other)
        {
            return tokens.Equals(other.tokens);
        }
    }
}
