﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    internal class MatchHelper
    {
        public bool runInBackground = true;

        private MatchPattern topPattern;

        private readonly GravityVector[] gravities = {
            GravityVector.DirectionDown,
            GravityVector.DirectionUp,
            GravityVector.DirectionLeft,
            GravityVector.DirectionRight
        };

        public List<GameObject> Fetch ()
        {
            RefreshQueue();

            return topPattern != null ? topPattern.tokens : null;
        }

        public IEnumerator Animate (IEnumerable<GameObject> tokens)
        {
            var gameObjects = tokens as GameObject[] ?? tokens.ToArray();

            for (float i = 1f; i >= 0.3f; i -= 0.1f) {
                AnimateTokens(gameObjects, i);
                yield return new WaitForSeconds(Constants.OpacityAnimationFrameDelay);
            }
            for (float i = 0.3f; i <= 1f; i += 0.1f) {
                AnimateTokens(gameObjects, i);
                yield return new WaitForSeconds(Constants.OpacityAnimationFrameDelay);
            }
        }

        private static void AnimateTokens (IEnumerable<GameObject> tokens, float alpha)
        {
            foreach (var item in tokens) {
                if (item == null || !item.HasComponent<SpriteRenderer>()) {
                    continue;
                }

                var renderer = item.GetComponent<SpriteRenderer>();
                var c = renderer.color;
                c.a = alpha;
                renderer.color = c;
            }
        }

        private void RefreshQueue ()
        {
            topPattern = null;
            foreach (var token in ElementComponent.Tokens.Items) {
                if (token.IsBlocked()) {
                    continue;
                }

                PositionInfo position = token.GetComponent<Movement>().Position;
                foreach (MatchPattern pattern in gravities
                    .Select(gravity => MatchPattern.Find(position, gravity))
                    .Where(pattern => pattern != null)
                    .Where(pattern => pattern.StrongerThen(topPattern))) {
                    topPattern = pattern;
                }
            }
        }
    }
}