﻿using UnityEngine;
using System.Collections;
using MMK.Match3;

public class LevelGridElement : MonoBehaviour
{
    public GridLayer layer;
    private TokenColor color = TokenColor.Random;
    private SpriteRenderer renderer;

    public GameObject Prefab
    {
        get { return prefab; }
        set
        {
            prefab = value;
            Sprite = prefab?.GetComponent<MMK.Match3.TokenComponents.Graphics>()?.EditorSprite;
            Color = prefab.GetColor();
        }
    }

    public Sprite Sprite
    {
        set
        {
            if (Renderer != null) {
                Renderer.sprite = value;
            }
        }
    }

    public void Reset ()
    {
        Color = TokenColor.Random;
        Prefab = null;
        Sprite = null;
    }

    private Match3Settings Settings => settings ?? (settings = Resources.Load<Match3Settings>("GameSettings"));

    public TokenColor Color
    {
        get { return color; }
        set
        {
            color = value;
            Renderer.color = Utilites.GetTokenColor(value);
        }
    }

    public SpriteRenderer Renderer => renderer ?? (renderer = GetComponent<SpriteRenderer>());

    [SerializeField]
    private GameObject prefab;
    private Match3Settings settings;
}
