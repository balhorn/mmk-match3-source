﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using PositionsList = System.Collections.Generic.Dictionary<int, MMK.Match3.PositionInfo>;

namespace MMK.Match3
{
    public class LevelGrid : MonoBehaviour
    {
        public Match3PremadeLevel CurrentLevel
        {
            set
            {
                currentLevel = value;
                InitLevel();
                CalculateOrigin();
                DrawTokens();
            }
        }

        private bool chessBackground;

        public bool ChessBackground
        {
            get { return chessBackground; }
            set
            {
                chessBackground = value;
                ApplyChessBackground();
            }
        }

        private Match3Settings Settings => settings ?? (settings = Resources.Load<Match3Settings>("GameSettings"));

        public Color SpawnPositionColor
        {
            set
            {
                var newColor = value;
                newColor.a = 0.4f;
                spawnPositionColor = newColor;
            }
            get { return spawnPositionColor; }
        }

        public Color DespawnPositionColor
        {
            set
            {
                var newColor = value;
                newColor.a = 0.4f;
                despawnPositionColor = newColor;
            }
            get { return despawnPositionColor; }
        }

        public Color IngridientSpawnerColor
        {
            set
            {
                var newColor = value;
                newColor.a = 0.4f;
                ingridientSpawnerColor = newColor;
            }
            get
            {
                return ingridientSpawnerColor;
            }
        }

        public string IngridientSpawnerIconName { get; set; }

        public Color gridColor = Color.green;
        private Color spawnPositionColor = new Color(1f, 0.92f, 0.016f, 0.4f);
        private Color despawnPositionColor = new Color(1f, 0, 0, 0.4f);
        private Color ingridientSpawnerColor = new Color(1f, 0.682f, 0f);
        public bool highlightSpawnPositions = true;

        private Match3Settings settings;
        private Match3PremadeLevel currentLevel;

        private List<List<GameObject>> background  = new List<List<GameObject>>();
        private List<List<GameObject>> cells       = new List<List<GameObject>>();
        private List<List<GameObject>> tokens      = new List<List<GameObject>>();
        private List<List<GameObject>> attachments = new List<List<GameObject>>();

        private readonly Dictionary<GridLayer, bool> layersEnabled = new Dictionary<GridLayer, bool> {
            {GridLayer.Background, true },
            {GridLayer.Base, true},
            {GridLayer.Tokens, true},
            {GridLayer.Attachments, true}
        };

        private int rows, columns;
        private Vector2 fieldOrigin;

        private PositionsList spawnDictionary;
        private PositionsList despawnDictionary;
        private PositionsList ingridientSpawnersDictionary;

        private GameObject[] holes;
        private IEnumerable<GameObject> Holes => holes ?? (holes = Resources.LoadAll<GameObject>(Constants.HolesForlder));

        public void Update ()
        {
            if ((int) currentLevel.size.y == columns && (int) currentLevel.size.x == rows) {
                return;
            }

            rows = (int) currentLevel.size.x;
            columns = (int) currentLevel.size.y;

            CalculateOrigin();
            CalculatePositions();
            DrawTokens();
        }

        public void SaveLevel ()
        {
            var rowsNumber = background.Count;
            if (rowsNumber == 0) {
                return;
            }

            var columnsNumber = background[0].Count;
            if (columnsNumber == 0) {
                return;
            }

            SortHoles(false);

            if (ChessBackground) {
                ApplyChessBackground();
            }

            currentLevel.background = new GameObjectsSerializedMatrix(rowsNumber, columnsNumber);
            currentLevel.cells = new GameObjectsSerializedMatrix(rowsNumber, columnsNumber);
            currentLevel.elements = new GameObjectsSerializedMatrix(rowsNumber, columnsNumber);
            currentLevel.attachments = new GameObjectsSerializedMatrix(rowsNumber, columnsNumber);

            for (int row = 0; row < rowsNumber; row++) {
                for (int column = 0; column < columnsNumber; ++column) {
                    var gridElement = background[row][column].GetComponent<LevelGridElement>();
                    currentLevel.background[row, column] = gridElement.Prefab;

                    currentLevel.cells[row, column] =
                        cells[row][column].GetComponent<LevelGridElement>().Prefab;
                    currentLevel.elements[row, column] =
                        tokens[row][column].GetComponent<LevelGridElement>().Prefab;
                    currentLevel.attachments[row, column] =
                        attachments[row][column].GetComponent<LevelGridElement>().Prefab;
                }
            }

            SortHoles(true);

            currentLevel.spawnPositions   = spawnDictionary.Values.Where(pos => pos != null).ToList();
            currentLevel.despawnPositions = despawnDictionary.Values.Where(pos => pos != null).ToList();
            currentLevel.ingridientSpawners = ingridientSpawnersDictionary.Values.Where(pos => pos != null).ToList();
        }

        public void SetSpawnPosition(Vector2 worldPosition)
        {
            SetPosition(worldPosition, spawnDictionary);
        }

        public void SetDespawnPosition (Vector2 worldPosition)
        {
            SetPosition(worldPosition, despawnDictionary);
        }

        public void SetIngridientSpawner (Vector2 worldPosition)
        {
            SetPosition(worldPosition, ingridientSpawnersDictionary);
        }

        private void SetPosition (Vector2 worldPosition, List<PositionInfo> list)
        {
            try {
                var positionInfo = WorldPositionToIndex(worldPosition);

                if (list.RemoveAll(pos => pos.Equals(positionInfo)) > 0) {
                    return;
                }

                list.RemoveAll(pos => pos.Column == positionInfo.column);
                list.Add(positionInfo);
            } catch (DivideByZeroException) {
                Debug.LogError("Seems like tokenSize vector has 0 value. Please check it in the settings window");
            }
        }

        private void SetPosition (Vector2 worldPosition, PositionsList list)
        {
            try {
                var positionInfo = WorldPositionToIndex(worldPosition);
                if (list.ContainsKey(positionInfo.Column)
                    && list[positionInfo.Column] != null
                    && list[positionInfo.Column].Equals(positionInfo)) {
                    list[positionInfo.Column] = null;
                    return;
                }

                list[positionInfo.Column] = positionInfo;

            } catch (DivideByZeroException) {
                Debug.LogError("Seems like tokenSize vector has 0 value. Please check it in the settings window");
            }
        }

        public void SwitchLayer (GridLayer layer, bool enabled)
        {
            if (layersEnabled[layer] == enabled) {
                return;
            }

            layersEnabled[layer] = enabled;
            List<List<GameObject>> collection = null;
            switch (layer) {
                case GridLayer.Background:
                    collection = background;
                    break;
                case GridLayer.Tokens:
                    collection = tokens;
                    break;
                case GridLayer.Base:
                    collection = cells;
                    break;
                case GridLayer.Attachments:
                    collection = attachments;
                    break;
            }
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    collection[i][j].SetActive(enabled);
                }
            }
        }

        public bool GetLayerState (GridLayer layer)
        {
            return layersEnabled[layer];
        }


        public bool CanDrawLayer (Vector2 worldPosition, GridLayer layer)
        {
            var position = WorldPositionToIndex(worldPosition);
            switch (layer) {
                case GridLayer.Background:
                    return true;
                case GridLayer.Base:
                    goto case GridLayer.Tokens;
                case GridLayer.Tokens:
                    var back = background[position.row][position.column].GetComponent<LevelGridElement>().Prefab;
                    return back != null && !Holes.Contains(back);
                case GridLayer.Attachments:
                    var token = tokens[position.row][position.column].GetComponent<LevelGridElement>().Prefab;
                    var backgr = background[position.row][position.column].GetComponent<LevelGridElement>().Prefab;
                    return backgr != null && !Holes.Contains(backgr) && token != null;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool CanEraseLayer (Vector2 worldPosition, GridLayer layer)
        {
            var position = WorldPositionToIndex(worldPosition);
            switch (layer) {
                case GridLayer.Background:
                    return cells[position.row][position.column].GetComponent<LevelGridElement>().Prefab == null
                           && tokens[position.row][position.column].GetComponent<LevelGridElement>().Prefab == null
                           && attachments[position.row][position.column].GetComponent<LevelGridElement>().Prefab == null;
                default:
                    return true;
            }
        }

        public void Erase (List<LevelGridElement> elements)
        {
            var back = elements.Find(elem => elem.layer == GridLayer.Background);
            elements.Remove(back);

            foreach (var element in elements) {
                element.Reset();
            }

            if (back == null) {
                return;
            }

            if (CanEraseLayer(back.transform.position, GridLayer.Background)) {
                back.Reset();
                return;
            }

            Debug.LogWarning("You cannot erase backround while some another layers exist");
        }

        public void EraseAll (Vector2 worldPosition)
        {
            var position = WorldPositionToIndex(worldPosition);
            background[position.row][position.column].GetComponent<LevelGridElement>().Reset();
            cells[position.row][position.column].GetComponent<LevelGridElement>().Reset();
            tokens[position.row][position.column].GetComponent<LevelGridElement>().Reset();
            attachments[position.row][position.column].GetComponent<LevelGridElement>().Reset();
        }

        private void InitLevel()
        {
            rows = (int) currentLevel.size.x;
            columns = (int) currentLevel.size.y;

            ClearAll();

            spawnDictionary = ToSortedDictionary(currentLevel.spawnPositions) ?? new PositionsList();
            despawnDictionary = ToSortedDictionary(currentLevel.despawnPositions) ?? new PositionsList();
            ingridientSpawnersDictionary = ToSortedDictionary(currentLevel.ingridientSpawners) ?? new PositionsList();

            ChessBackground = currentLevel.chessBackground;

            if (currentLevel.background == null) {
                return;
            }

            for (int row = 0; row < rows; row++) {
                background.Add(new List<GameObject>());
                cells.Add(new List<GameObject>());
                tokens.Add(new List<GameObject>());
                attachments.Add(new List<GameObject>());
                for (int column = 0; column < columns; column++) {
                    AddNewToken(row, currentLevel.background[row, column], currentLevel.cells[row, column], currentLevel.elements[row, column], currentLevel.attachments[row, column]);
                }
            }

            SortHoles(true);
        }

        private void DrawTokens()
        {
            while (cells.Count != 0 && cells[0].Count > columns) {
                var count = cells[0].Count;
                for (int i = 0; i < rows; i++) {
                    DestroyToken(i, count - 1);
                    background[i].RemoveAt(count - 1);
                    cells[i].RemoveAt(count - 1);
                    tokens[i].RemoveAt(count - 1);
                    attachments[i].RemoveAt(count - 1);
                }
            }

            while (cells.Count > rows) {
                for (int i = 0; i < cells[0].Count; i++) {
                    DestroyToken(0, i);
                }
                background.RemoveAt(0);
                cells.RemoveAt(0);
                tokens.RemoveAt(0);
                attachments.RemoveAt(0);
            }

            while (cells.Count < rows) {
                background.Add(new List<GameObject>());
                cells.Add(new List<GameObject>());
                tokens.Add(new List<GameObject>());
                attachments.Add(new List<GameObject>());

                var index = cells.Count - 1;
                for (int i = 0; i < columns; i++) {
                    AddNewToken(index, GetBackground(index, i), null, Settings.defaultTokenPrefab, null);
                }
            }

            while (cells.Count != 0 && cells[0].Count < columns) {
                var count = cells.Count;
                for (int i = 0; i < count; i++) {
                    AddNewToken(i, GetBackground(i, cells[0].Count - 1), null, Settings.defaultTokenPrefab, null);
                }
            }

            for (int i = 0; i < rows; ++i) {
                for (int j = 0; j < columns; ++j) {
                    var position = fieldOrigin + new Vector2(Settings.tokenSize.x * (j + 0.5f), Settings.tokenSize.y * (i + 0.5f));
                    cells[i][j].transform.parent.position = position;
                }
            }
        }

        private void CalculatePositions()
        {
            if (spawnDictionary == null) {
                spawnDictionary = new PositionsList();
            }

            if (despawnDictionary == null) {
                despawnDictionary = new PositionsList();
            }

            if (ingridientSpawnersDictionary == null) {
                ingridientSpawnersDictionary = new PositionsList();
            }

            while (spawnDictionary.Count > columns) {
                spawnDictionary.Remove(spawnDictionary.Count - 1);
            }

            while (despawnDictionary.Count > columns) {
                despawnDictionary.Remove(despawnDictionary.Count - 1);
            }

            while (ingridientSpawnersDictionary.Count > columns) {
                ingridientSpawnersDictionary.Remove(ingridientSpawnersDictionary.Count - 1);
            }

            while (spawnDictionary.Count < columns) {
                spawnDictionary[spawnDictionary.Count] = new PositionInfo(rows - 1, spawnDictionary.Count);
            }

            foreach (var value in spawnDictionary.Values.Where(pos => pos != null)) {
                var distance = background.Count - value.Row - 1;
                value.Row = Mathf.Clamp(rows - distance - 1, 0, rows - 1);
            }

            foreach (var value in despawnDictionary.Values.Where(pos => pos != null)) {
                var distance = background.Count - value.Row - 1;
                value.Row = Mathf.Clamp(rows - distance - 1, 0, rows - 1);
            }

            foreach (var value in ingridientSpawnersDictionary.Values.Where(pos => pos != null)) {
                var distance = background.Count - value.Row - 1;
                value.Row = Mathf.Clamp(rows - distance - 1, 0, rows - 1);
            }
        }


        public void ClearAll ()
        {
            foreach (var list in tokens) {
                foreach (var token in list.Where(token => token != null)) {
                    DestroyImmediate(token.transform.parent.gameObject);
                }
            }

            background.Clear();
            cells.Clear();
            tokens.Clear();
            attachments.Clear();

        }

        private void CalculateOrigin()
        {
            var halfRows = currentLevel.size.x / 2;
            var halfColumns = currentLevel.size.y / 2;

            var screenCenter = Vector2.zero;

            var xPosition = screenCenter.x - (int) halfColumns * Settings.tokenSize.x;
            var yPosition = screenCenter.y - (int) halfRows * Settings.tokenSize.y;

            //Correcting for even and odd rows and columns
            xPosition -= Mathf.Approximately(halfColumns - (int) halfColumns, 0) ? 0 : Settings.tokenSize.x / 2;
            yPosition -= Mathf.Approximately(halfRows - (int) halfRows, 0) ? 0 : Settings.tokenSize.y / 2;

            fieldOrigin = new Vector2(xPosition, yPosition);
        }

        private void OnDrawGizmos()
        {
            try {
                Gizmos.color = gridColor;

                var horizontalLength = columns * Settings.tokenSize.x;
                for (int i = 0; i <= rows; i++) {
                    var start = fieldOrigin + new Vector2(0f, i * Settings.tokenSize.y);
                    var end = fieldOrigin + new Vector2(horizontalLength, i * Settings.tokenSize.y);
                    Gizmos.DrawLine(start, end);
                }

                var verticalLength = rows * Settings.tokenSize.y;
                for (int i = 0; i <= columns; i++) {
                    var start = fieldOrigin + new Vector2(i * Settings.tokenSize.x, verticalLength);
                    var end = fieldOrigin + new Vector2(i * Settings.tokenSize.x, 0f);
                    Gizmos.DrawLine(start, end);
                }

                if (!highlightSpawnPositions) {
                    return;
                }

                Gizmos.color = spawnPositionColor;
                foreach (var position in spawnDictionary.Values.Where(pos => pos != null)) {
                    var pos = fieldOrigin + new Vector2(Settings.tokenSize.x * (position.column + 0.5f), Settings.tokenSize.x * (position.row + 0.5f));
                    Gizmos.DrawCube(pos, Settings.tokenSize);
                }

                Gizmos.color = despawnPositionColor;
                foreach (var position in despawnDictionary.Values.Where(pos => pos != null)) {
                    var pos = fieldOrigin + new Vector2(Settings.tokenSize.x * (position.column + 0.5f), Settings.tokenSize.x * (position.row + 0.5f));
                    Gizmos.DrawCube(pos, Settings.tokenSize);
                }

                if (string.IsNullOrEmpty(IngridientSpawnerIconName)) {
                    return;
                }

                Gizmos.color = new Color(1f, 1f, 1f, 0.4f);
                foreach (var spawner in ingridientSpawnersDictionary.Values.Where(pos => pos != null)) {
                    var pos = fieldOrigin + new Vector2(Settings.tokenSize.x * (spawner.column + 0.5f), Settings.tokenSize.x * (spawner.row + 0.5f));
                    Gizmos.DrawIcon(pos, IngridientSpawnerIconName);
                }
            } catch (Exception e) {
                Debug.LogWarning(e);
            }
        }

        private GameObject SetupLayer(GridLayer layer, GameObject parent, GameObject prefab)
        {
            var layerName = layer.ToString();
            var objectName = GetSortingLayerName(layer);
            var result = new GameObject(objectName);

            var spriteRenderer = result.AddComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = layerName;

            var collider = result.AddComponent<BoxCollider2D>();
            collider.size = Settings.tokenSize;

            var gridElement = result.AddComponent<LevelGridElement>();
            gridElement.layer = layer;
            gridElement.Prefab = prefab;

            result.transform.SetParent(parent.transform, false);
            return result;
        }

        private string GetSortingLayerName (GridLayer gridLayer)
        {
            var layerName = gridLayer.ToString();
            switch (gridLayer) {
                case GridLayer.Background:
                    goto case GridLayer.Base;
                case GridLayer.Base:
                    return layerName;
                default:
                    return layerName.Remove(layerName.Length - 1);
            }
        }

        private void SortHoles (bool onInit)
        {
            var rowsNumber = background.Count;
            if (rowsNumber == 0) {
                return;
            }

            var columnsNumber = background[0].Count;
            if (columnsNumber == 0) {
                return;
            }

            for (int row = 0; row < rowsNumber; row++) {
                for (int column = 0; column < columnsNumber; column++) {
                    var moveFrom = onInit ? tokens[row][column] : background[row][column];
                    var moveTo = onInit ? background[row][column] : tokens[row][column];

                    var gridElement = moveFrom.GetComponent<LevelGridElement>();
                    if (!Holes.Contains(gridElement.Prefab)) {
                        continue;
                    }
                    moveTo.GetComponent<LevelGridElement>().Prefab = gridElement.Prefab;
                    gridElement.Prefab = null;
                }
            }
        }

        private void ApplyChessBackground ()
        {
            if (background.Count <= 0) {
                return;
            }

            for (int row = 0; row < background.Count; ++row) {
                for (int column = 0; column < background[0].Count; ++column) {
                    try {
                        var element = background[row][column].GetComponent<LevelGridElement>();
                        if (element.Prefab == null) {
                            continue;
                        }

                        element.Prefab = GetBackground(row, column);
                    } catch (IndexOutOfRangeException) {
                        continue;
                    }
                }
            }
        }

        private GameObject GetBackground (int row, int column)
        {
            if (!ChessBackground) {
                return Settings.defaultBackroundPrefab;
            }

            if (row % 2 == 0) {
                return column % 2 == 0 ? Settings.Sprites.whiteBackground : Settings.Sprites.blackBackground;
            }

            return column % 2 == 0 ? Settings.Sprites.blackBackground : Settings.Sprites.whiteBackground;
        }

        private void DestroyToken(int row, int column)
        {
            DestroyImmediate(cells[row][column].transform.parent.gameObject);
        }

        private void AddNewToken(int index, GameObject backroundPrefab, GameObject cellPrefab, GameObject tokenPrefab, GameObject attachmentPrefab)
        {
            var rootGo = new GameObject("Element");

            background[index].Add(SetupLayer(GridLayer.Background, rootGo, backroundPrefab));
            cells[index].Add(SetupLayer(GridLayer.Base, rootGo, cellPrefab));
            tokens[index].Add(SetupLayer(GridLayer.Tokens, rootGo, tokenPrefab));
            attachments[index].Add(SetupLayer(GridLayer.Attachments, rootGo, attachmentPrefab));
        }

        private PositionInfo WorldPositionToIndex(Vector2 worldPosition)
        {
            var yPos = (worldPosition.x - fieldOrigin.x) / Settings.tokenSize.x - 0.5f;
            var xPos = (worldPosition.y - fieldOrigin.y) / Settings.tokenSize.y - 0.5f;
            return new PositionInfo(Mathf.RoundToInt(xPos), Mathf.RoundToInt(yPos));
        }

        private PositionsList ToSortedDictionary (List<PositionInfo> positions)
        {
            var result = new PositionsList();

            if (positions == null) {
                return result;
            }

            for (int column = 0; column < currentLevel.size.y; column++) {
                result[column] = positions.Find(pos => pos.Column == column);
            }

            return result;
        }

        private void OnDestroy ()
        {
        }
    }
}