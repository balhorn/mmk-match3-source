﻿using System;
using System.Collections;
using System.Collections.Generic;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    /// <summary>
    /// Data structure to represent a position in the field array.
    /// Provides information about row and column as well as some helper methods.
    /// </summary>
    [Serializable]
    public class PositionInfo : IEquatable<PositionInfo>
    {
        public PositionInfo (Movement movement)
        {
            if (movement == null) {
                Column = -10;
                Row = -10;
                return;
            }

            Column = movement.Column;
            Row = movement.Row;
        }

        public PositionInfo (int row, int column)
        {
            Row = row;
            Column = column;
        }

        public PositionInfo (PositionInfo info)
        {
            Row = info.Row;
            Column = info.Column;
        }

        public PositionInfo ()
        {
        }

        /// <summary>
        /// Applies gravity vector to the position info i.e. moves the position in the specified direction
        /// For example:
        /// PositionInfo pos = new PositionInfo (4, 5);
        /// GravityVector vec = new GravityVector (1, 0);
        /// PositionInfo result = pos + vec; // (4, 4);
        /// </summary>
        /// <param name="info"></param>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static PositionInfo operator + (PositionInfo info, GravityVector vector)
        {
            return new PositionInfo {Row = info.Row + (int) vector.Value.y, Column = info.Column + (int) vector.Value.x};
        }

        /// <summary>
        /// Tranforms given world coordinates into respective position in the array. If coordinates don't belong to the array, returns (-1, -1)
        /// </summary>
        /// <param name="coordinates">World position</param>
        /// <param name="origin">Coordinates of the origin of the array</param>
        /// <returns></returns>
        public static PositionInfo FromCoordinates (Vector2 coordinates, Vector2 origin)
        {
            if (coordinates.x < origin.x || coordinates.y < origin.y
                || coordinates.x > origin.x + Match3Player.Settings.tokenSize.x * LevelConfig.columns
                || coordinates.y > origin.y + Match3Player.Settings.tokenSize.y * LevelConfig.rows) {
                return new PositionInfo(-1, -1);
            }

            var row    = (int) ((coordinates.y - origin.y) / Match3Player.Settings.tokenSize.y);
            var column = (int) ((coordinates.x - origin.x) / Match3Player.Settings.tokenSize.x);

            return new PositionInfo(row, column);
        }

        /// <summary>
        /// Distance between two positions considered usual vectors metrix
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static float Distance (PositionInfo first, PositionInfo second)
        {
            var rowsSquare = (first.Row - second.Row) * (first.Row - second.Row);
            var columnsSquare = (first.Column - second.Column) * (first.Column - second.Column);

            return Mathf.Sqrt(rowsSquare + columnsSquare);
        }

        /// <summary>
        /// Generate random position within the extents of the current level
        /// </summary>
        /// <returns>Random position</returns>
        public static PositionInfo GenerateRandom ()
        {
            var row = UnityEngine.Random.Range(0, LevelConfig.rows);
            var column = UnityEngine.Random.Range(0, LevelConfig.columns);

            return new PositionInfo(row, column);
        }

        public int column;
        public int row;

        public int Column { get { return column; } set {column = value;} }
        public int Row { get { return row; } set { row = value; } }

        public bool Equals(PositionInfo other)
        {
            return Column == other.Column && Row == other.Row;
        }

        public bool IsNeighbour (PositionInfo other)
        {
            return Mathf.Abs(column - other.column) > 1 && Mathf.Abs(row - other.row) > 1;
        }

        /// <summary>
        /// Moves position in the direction specified by given vector
        /// </summary>
        /// <param name="gravity">Gravity vector to be applied</param>
        public void UpdateWithGravity (GravityVector gravity)
        {
            var result = this + gravity;
            Row = result.Row;
            Column = result.Column;
        }

        /// <summary>
        /// Checks whether current position doesn't overextend the field extents
        /// </summary>
        /// <returns></returns>
        public bool Overextend ()
        {
            return Row < 0 || Row > LevelConfig.rows - 1 || Column < 0 || Column > LevelConfig.columns - 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="origin"></param>
        /// <returns>World coordinates based on specified field origin</returns>
        public Vector2 Coordinates (Vector2 origin)
        {
            return origin +
                   new Vector2(Column * Match3Player.Settings.tokenSize.x, Row * Match3Player.Settings.tokenSize.y);
        }


        public override string ToString()
        {
            return $"Row: {Row}, Column: {Column}";
        }
    }


    public class PositionComparer : IEqualityComparer<PositionInfo>
    {
        public bool Equals (PositionInfo x, PositionInfo y)
        {
            return x.Equals(y);
        }

        public int GetHashCode (PositionInfo obj)
        {
            return 10 * obj.Row + obj.Column;
        }
    }
}