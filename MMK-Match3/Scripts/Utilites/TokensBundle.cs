﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MMK.Match3
{
    public class TokensBundle : IEnumerable<GameObject>
    {
        private HashSet<GameObject> tokens;

        public int Count => tokens.Count;

        public IEnumerable<GameObject> MatchedTokens => tokens;

        public void AddObject(GameObject go)
        {
            tokens.Add(go);
        }

        /// <summary>
        /// Separates bundle by color.
        /// </summary>
        /// <param name="fireEvent"></param>
        /// <returns>List of shaped bundles of each color</returns>
        public IEnumerable<ShapedBundle> DetermineShapes(bool fireEvent = true)
        {
            IEnumerable<TokenColor> colors = tokens.Select (token => token.GetColor()).Distinct ();
            List<ShapedBundle> bundles = new List<ShapedBundle>();
            foreach (var color in colors) {
                IEnumerable<GameObject> coloredTokens = tokens.Where (token => token.GetColor() == color);
                bundles.Add(new ShapedBundle(coloredTokens, color, fireEvent));
            }

            return bundles;
        }

        public void AddObjectRange(IEnumerable<GameObject> gos)
        {
            foreach (var item in gos) {
                AddObject(item);
            }
        }

        public TokensBundle()
        {
            tokens = new HashSet<GameObject>();
        }

        public TokensBundle(IEnumerable<GameObject> objects)
        {
            tokens = new HashSet<GameObject>(objects);
        }

        public void Clear()
        {
            tokens.Clear();
        }

        public IEnumerator<GameObject> GetEnumerator ()
        {
            return tokens.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator ()
        {
            return tokens.GetEnumerator();
        }
    }
}

