﻿using MMK.Core;
using MMK.Match3.Preboosters;

namespace MMK.Match3
{
    public class Match3Player : Player
    {
        public new static Match3Player Instance => Player.Instance as Match3Player;

        public new static Match3Settings Settings => Player.Settings as Match3Settings;

        public void SpendBooster(Prebooster prebooster)
        {
            SpendBooster(prebooster.GetType().Name);
        }

        public int GetBoosterAmount(Prebooster prebooster)
        {
            return GetBoosterAmount(prebooster.GetType().Name);
        }
    }
}