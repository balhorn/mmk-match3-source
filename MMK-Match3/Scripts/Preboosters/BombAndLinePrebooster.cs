﻿using UnityEngine;
using System.Collections;

namespace MMK.Match3.Preboosters
{
    public class BombAndLinePrebooster : Prebooster
    {
        protected override void Awake ()
        {
            base.Awake();
            preboosters.Add(new PreboosterInfo(MatchShape.T, 1));

            //randomly choose vertical or horizontal line bonus
            AddLineBonus();
        }

        protected void AddLineBonus ()
        {
            var offset = Random.Range(2, 4);
            preboosters.Add(new PreboosterInfo((MatchShape)(1 << offset), 1));
        }
    }
}

