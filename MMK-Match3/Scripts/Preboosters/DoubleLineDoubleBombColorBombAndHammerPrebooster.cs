﻿namespace MMK.Match3.Preboosters
{
    public class DoubleLineDoubleBombColorBombAndHammerPrebooster : DoubleLineDoubleBombAndHammerPrebooster
    {
        protected override void Awake()
        {
            base.Awake();
            preboosters.Add(new PreboosterInfo(MatchShape.Five, 1));
        }
    }
}