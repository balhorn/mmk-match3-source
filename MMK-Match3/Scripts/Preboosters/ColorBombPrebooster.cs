﻿using UnityEngine;
using System.Collections;

namespace MMK.Match3.Preboosters
{
    public class ColorBombPrebooster : Prebooster
    {
        protected override void Awake ()
        {
            base.Awake();
            preboosters.Add(new PreboosterInfo(MatchShape.Five, 1));
        }
    }
}

