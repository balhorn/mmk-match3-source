﻿using MMK.Core;
using MMK.Match3;

namespace MMK.Match3.Preboosters
{
    public class BombAndHammerPrebooster : Prebooster
    {
        protected override void Awake ()
        {
            base.Awake();
            preboosters.Add(new PreboosterInfo(MatchShape.T, 1));
        }

        public override bool Save()
        {
            if (!base.Save()) {
                return false;
            }

            Player.Instance.GainBooster(typeof (HammerBooster), 1);
            return true;
        }
    }
}