﻿namespace MMK.Match3.Preboosters
{
    public class DoubleLineDoubleBombAndHammerPrebooster : DoubleLineBombAndHammerPrebooster
    {
        protected override void Awake()
        {
            base.Awake();
            preboosters.Add(new PreboosterInfo(MatchShape.T, 1));
        }
    }
}