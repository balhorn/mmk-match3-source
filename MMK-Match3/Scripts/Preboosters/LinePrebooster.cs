﻿using MMK.Match3.Preboosters;
using UnityEngine;

namespace MMK.Match3.Preboosters
{
    public class LinePrebooster : Prebooster
    {
        protected override void Awake ()
        {
            base.Awake();
            var offset = Random.Range(2, 4);
            preboosters.Add(new PreboosterInfo((MatchShape)(1 << offset), 1));
        }
    }
}
