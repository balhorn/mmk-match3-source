﻿namespace MMK.Match3.Preboosters
{
    public class DoubleLineBombAndHammerPrebooster : BombLineAndHammerPrebooster
    {
        protected override void Awake()
        {
            base.Awake();
            AddLineBonus();
        }
    }
}