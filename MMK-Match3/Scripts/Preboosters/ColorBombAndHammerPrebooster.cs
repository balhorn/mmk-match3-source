﻿using MMK.Core;
using MMK.Match3;

namespace MMK.Match3.Preboosters
{
    public class ColorBombAndHammerPrebooster : ColorBombPrebooster
    {
        public override bool Save()
        {
            if (!base.Save()) {
                return false;
            }

            Player.Instance.GainBooster(typeof (HammerBooster), 1);
            return true;
        }
    }
}