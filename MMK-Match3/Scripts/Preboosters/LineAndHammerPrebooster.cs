﻿using MMK.Core;
using MMK.Match3;
using UnityEngine;

namespace MMK.Match3.Preboosters
{
    public class LineAndHammerPrebooster : Prebooster
    {
        protected override void Awake ()
        {
            base.Awake();
            var offset = Random.Range(2, 4);
            preboosters.Add(new PreboosterInfo((MatchShape)(1 << offset), 1));
        }

        public override bool Save ()
        {
            if (!base.Save()) {
                return false;
            }

            Player.Instance.GainBooster(typeof(HammerBooster), 1);
            return true;
        }
    }
}

