﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Text;
using MMK.Core;
using MMK.Core.UI;
using MMK.Match3;
using UnityEngine.UI;

namespace MMK.Match3.Preboosters
{
    public abstract class Prebooster : MonoBehaviour
    {
        [Serializable]
        public class PreboosterInfo
        {
            public MatchShape bonus;
            public int amount;

            public PreboosterInfo (MatchShape bonus, int amount)
            {
                this.bonus = bonus;
                this.amount = amount;
            }
        }

        [SerializeField]
        [HideInInspector]
        protected List<PreboosterInfo> preboosters;

        //private Outline outline;
        private PreboosterUIElement uiElement;
        private string preparedInfo;
        public const string PrefsKey = "prebooster";

        protected GetBoosterWindow getBoosterWindow;
        protected int Amount => Match3Player.Instance.GetBoosterAmount(this);

        public GetBoosterWindow GetBoosterWindow { set { getBoosterWindow = value; } }

        private Sprite Image => uiElement.MainImage.sprite;

        public static List<PreboosterInfo> Parse (string input)
        {
            var result = new List<PreboosterInfo>();
            var preboosters = input.Split(';');

            foreach (var prebooster in preboosters) {
                if (string.IsNullOrEmpty(prebooster)) {
                    continue;
                }

                var split = prebooster.Split(':');
                var name = split[0];
                var amount = Convert.ToInt32(split[1]);

                result.Add(new PreboosterInfo((MatchShape)Enum.Parse(typeof(MatchShape), name), amount));
            }

            return result;
        }

        protected virtual void Awake ()
        {
            uiElement = GetComponent<PreboosterUIElement>();
        }

        public void Prepare (bool checkAmount = true)
        {
            if (checkAmount && Amount <= 0) {
                getBoosterWindow.Show(GetType(), Image);
                return;
            }

            uiElement.SwitchOutline();

            if (!string.IsNullOrEmpty(preparedInfo)) {
                preparedInfo = string.Empty;
                return;
            }

            var builder = new StringBuilder();
            foreach (var prebooster in preboosters) {
                builder
                    .Append(prebooster.bonus)
                    .Append(":")
                    .Append(prebooster.amount)
                    .Append(";");
            }

            preparedInfo = builder.ToString();
        }

        public virtual bool Save ()
        {
            if (string.IsNullOrEmpty(preparedInfo)) {
                return false;
            }

            if (PlayerPrefs.HasKey(PrefsKey)) {
                preparedInfo += PlayerPrefs.GetString(PrefsKey);
            }

            PlayerPrefs.SetString(PrefsKey, preparedInfo);
            PlayerPrefs.Save();

            Match3Player.Instance.SpendBooster(this);

            return true;
        }

        private void OnDestroy ()
        {
            
        }

        private void OnApplicationQuit ()
        {
            PlayerPrefs.DeleteKey(PrefsKey);
        }
    }
}
