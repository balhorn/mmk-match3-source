using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MMK.Core.Aims;
using MMK.Match3;
using UnityEngine;
using Graphics = MMK.Match3.TokenComponents.Graphics;

namespace MMK.Match3.Aims
{
    public class IngridientAim : LevelAim
    {
        private SortedList<int, GameObject> ingridients;
        private Sprite uiSprite;

        public void AddIngridient (GameObject ingridient, int score)
        {
            if (ingridients == null) {
                ingridients = new SortedList<int, GameObject>(new DuplicatedKeyComparer());
            }

            if (uiSprite == null) {
                uiSprite = ingridient?.GetComponent<Ingridient>()?.Sprite;
            }

            if (!ingridients.ContainsKey(score)) {
                ingridients.Add(score, ingridient);
            }
        }

        public override void InstantiateUIItem(GameObject parent)
        {
            base.InstantiateUIItem(parent);

            uiElement.sprite = uiSprite;
            uiElement.Refresh();
        }

        private void Start ()
        {
            LevelConfig.withIngridient = true;
        }

        public override void BindEvent()
        {
            Ingridient.IngridientCollected += Increment;
        }

        public override void UnbindEvent()
        {
            Ingridient.IngridientCollected -= Increment;
        }

        protected override Sprite GetSprite(string spriteName)
        {
            return null;
        }

        private void Increment(Sprite sprite)
        {
            if (ingridients != null && ingridients.Values
                .Where (token => token != null)
                .All(token => token.GetComponent<Ingridient>()?.Sprite != sprite)) {
                return;
            }

            ++currentAmount;
            uiElement?.UpdateUI(currentAmount);
            if (currentAmount >= requiredAmount) {
                Level?.AimRiched(this);
            }
        }
    }
}