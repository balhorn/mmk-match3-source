﻿using UnityEngine;
using System.Collections;
using MMK.Core;

namespace MMK.Match3.Aims 
{
	public class ScoreAim : Core.Aims.LevelAim
	{
	    private Level currentLevel;
	    private int scoreToWin;

	    public override bool IsCompleted => currentAmount >= scoreToWin;

        private void Start ()
        {
            currentLevel = Level.Current;
            scoreToWin = requiredAmount;

            LevelConfig.winBonusEnabled = false;
            currentLevel.Constraint.PlayToTheEnd = true;
            uiElement.displayCurrentAmount = false;

            requiredAmount = Mathf.Max(currentLevel.highScore, requiredAmount);
            uiElement.requiredAmount = requiredAmount;
            uiElement.UpdateUI(currentAmount);
        }

        public void Add (int amount)
		{
			currentAmount += amount;
			uiElement.UpdateUI (currentAmount);
			if (currentAmount >= scoreToWin) {
				Level.AimRiched (this);
			}
		}

        private void OnGUI ()
        {
            var rect = new Rect(Screen.width - 200, 0, 200, 50);
            GUI.Box(rect, $"{currentAmount}/{requiredAmount}");
        }

	    protected override Sprite GetSprite(string spriteName)
	    {
	        return null;
	    }

	    public override void BindEvent ()
		{
			ScoreManager.OnScoreAdded += Add;
		}

		public override void UnbindEvent ()
		{
			ScoreManager.OnScoreAdded -= Add;
		}
	}
}