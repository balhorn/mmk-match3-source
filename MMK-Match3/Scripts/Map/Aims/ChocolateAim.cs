﻿using UnityEngine;
using System.Collections;
using MMK.Core.Aims;
using MMK.Match3;

namespace MMK.Match3.Aims
{
    public class ChocolateAim : LevelAim
    {
        public override void BindEvent()
        {
            CreepingChocolate.OnChocolateDestroyed += Increment;
        }

        public override void UnbindEvent()
        {
            CreepingChocolate.OnChocolateDestroyed -= Increment;
        }

        protected override Sprite GetSprite(string spriteName)
        {
            return null;
        }
    }
}


