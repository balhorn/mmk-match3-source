﻿using UnityEngine;
using System.Collections;
using MMK.Match3;

namespace MMK.Match3.Aims
{
	public class CookieAim : Core.Aims.LevelAim
	{
		public override void BindEvent ()
		{
			GlassCell.OnGlassCellDestroyed += Increment;
		}

		public override void UnbindEvent ()
		{
			GlassCell.OnGlassCellDestroyed -= Increment;
		}

	    protected override Sprite GetSprite(string spriteName)
	    {
	        return null;
	    }
	}
}