﻿using UnityEngine;
using System.Collections;
using MMK.Core.Aims;
using MMK.Match3;
using MMK.Match3.TokenComponents;

namespace MMK.Match3.Aims
{
    public class TokensAim : LevelAim
    {
        public TokenColor color;

        public void OnTokenDestroyed (TokenColor color, PositionInfo info, IDamageDealer dealer)
        {
            if (color == this.color) {
                Increment ();
            }
        }

        public override void BindEvent ()
        {
            Durability.OnTokenDestroyed += OnTokenDestroyed;
        }

        public override void UnbindEvent ()
        {
            Durability.OnTokenDestroyed -= OnTokenDestroyed;
        }

        protected override Sprite GetSprite(string spriteName)
        {
            return null;
        }
    }
}
