﻿using UnityEngine;
using System.Collections;
using MMK.Core.Aims;
using MMK.Match3.Aims;
using MMK.Match3;

namespace MMK.Match3.Aims 
{
	public class IceAim : LevelAim
	{

		public override void BindEvent ()
		{
			IceBlocker.OnIceBroken += Increment;
		}

		public override void UnbindEvent ()
		{
			IceBlocker.OnIceBroken -= Increment;
		}

	    protected override Sprite GetSprite(string spriteName)
	    {
	        return null;
	    }
	}
}
