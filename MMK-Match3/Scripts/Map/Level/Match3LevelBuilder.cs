﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Core.Aims;
using MMK.Match3.Aims;
using MMK.Match3.Preboosters;
using MMK.Match3.TokenComponents;
using UnityEngine;
using UnityEngine.UI;
using Color = UnityEngine.Color;

namespace MMK.Match3
{
    public class Match3LevelBuilder : AbstractLevelBuilder
    {
        private Match3PremadeLevel M3PremadeLevel => premadeLevel as Match3PremadeLevel;

        private Level level;
        private FieldOutline fieldOutline;
        private TokenColor[,] colors;

        public List<GameObject> spawningTokens;

        private ITokensCollection tokens;
        private ShapesManager shapesManager;

        private void Start()
        {
            Level = GetComponent<Level> ();
            fieldOutline = GetComponent<FieldOutline>();
            PremadeLevel = Level.PremadeLevel;
        }

        private void InitTokens ()
        {
            shapesManager.Ingridients = new SortedList<int, GameObject>(new DuplicatedKeyComparer());
            foreach (var ingridient in M3PremadeLevel.ingridients) {
                shapesManager.Ingridients.Add(ingridient.score, ingridient.prefab);
            }

            tokens = new TokensArray((int) M3PremadeLevel.size.x, (int) M3PremadeLevel.size.y) {
                BottomPositions = new List<PositionInfo>(),
                SpawnPositions = new List<PositionInfo>(),
                DespawnPositions = new List<PositionInfo>(),
                IngridientSpawners = new List<PositionInfo>(),
                Cells = M3PremadeLevel.cells.ToMatrix()
            };
        }

        public override void BuildPremadeLevel()
        {
            if (M3PremadeLevel == null) {
                return;
            }

            shapesManager = ShapesManager.Instance;
            M3PremadeLevel.number = Level.number;
            M3PremadeLevel.InitLevelConfig();

            InitTokens();

            tokens.SpawnPositions.AddRange(M3PremadeLevel.spawnPositions);
            tokens.DespawnPositions.AddRange(M3PremadeLevel.despawnPositions);
            tokens.IngridientSpawners.AddRange(M3PremadeLevel.ingridientSpawners);

            SetupBottomPositions();
            SetupBoosters();
            SetupConstraint();
            CalculateOrigin();
            CreateDespawnerSprites();
            SetupCellsAndTokens();
            SetupLevelAims();
            FetchColors();
            CorrectLevel();
            ApplyColors();
            StartCoroutine(SetupPreboosters());

            fieldOutline?.Build();
        }

        internal IEnumerator SetupPreboosters()
        {
            yield return new WaitForEndOfFrame();

            if (!PlayerPrefs.HasKey(Prebooster.PrefsKey)) {
                yield break;
            }

            try {
                var preboosters = Prebooster.Parse(PlayerPrefs.GetString(Prebooster.PrefsKey));

                var takenPositions = new List<PositionInfo>();
                foreach (var prebooster in preboosters) {
                    for (int i = 0; i < prebooster.amount; i++) {
                        var position = PositionInfo.GenerateRandom();
                        while (tokens[position].IsBlocked()
                               || tokens[position].HasComponent<Bonus>()
                               || takenPositions.Any(pos => pos.IsNeighbour(position))) {
                            position = PositionInfo.GenerateRandom();
                        }

                        var choosenToken = tokens[position];
                        var bonus = shapesManager.BonusManager.GetBonus(choosenToken.GetColor(), prebooster.bonus);
                        shapesManager.CreateBonus(bonus, position);
                        Destroy(choosenToken);

                        takenPositions.Add(position);
                    }
                }
            } finally {
                PlayerPrefs.DeleteKey(Prebooster.PrefsKey);
            }

        }

        private void SetupBoosters()
        {
            var panel = GameObject.FindGameObjectWithTag("Boosters");
            if (panel == null) {
                Debug.LogError("Boosters panel with tag Boosters wasn't found");
                return;
            }

            var boosterSprites = Match3Player.Settings.Sprites;

            for (int i = 0; i < Player.Settings.maximumBoostersAmount && i < panel.transform.childCount; ++i) {
                if (i < M3PremadeLevel.boosters.Count) {
                    var boosterType = Type.GetType(M3PremadeLevel.boosters[i]);


                    var boosterIcon = panel.transform.GetChild(i);
                    if (boosterIcon != null) {
                        boosterIcon.gameObject.GetComponent<Image>().sprite = boosterSprites[boosterType.Name];
                        boosterIcon.gameObject.AddComponent(boosterType);
                    }
                } else {
                    var icon = panel.transform.GetChild(i);
                    if (icon != null) {
                        icon.gameObject.GetComponent<Image>().sprite = boosterSprites.lockedSprite;
                    }
                }
            }
        }

        private void SetupConstraint()
        {
            //if (MMK.edition == Edition.Community) {
            //    M3PremadeLevel.constraint = typeof (MovesCounter).Name;
            //}

            var baseObject = GameObject.Find("Level constraints");
            var constraint = baseObject.GetComponent(M3PremadeLevel.constraint) as Constraint;
            if (constraint != null) {
                constraint.enabled = true;
            }
        }

        private void SetupBottomPositions ()
        {
            foreach (var spawnPosition in tokens.SpawnPositions) {
                var forwardPos = spawnPosition + tokens.GetGravity(spawnPosition);

                while (!(forwardPos + tokens.GetGravity(spawnPosition)).Overextend()) {
                    forwardPos.UpdateWithGravity(tokens.GetGravity(forwardPos));
                }

                tokens.BottomPositions.Add(forwardPos);
            }
        }

    

        private void SetupLevelAims ()
        {
            var baseObject = GameObject.FindGameObjectWithTag("Aims");

            if (baseObject == null) {
                Debug.LogError("Game object for level aims wasn't found. Please assign tag 'Aims' to some UI panel");
                return;
            }

            Level.aims = new LevelAim[M3PremadeLevel.aims.Count];
            for (int index = 0; index < M3PremadeLevel.aims.Count; index++) {
                var aimInfo = M3PremadeLevel.aims[index];
                //aimInfo.Ingridients = CollectIngridients();

                var aim = aimInfo.AttachTo(baseObject);
                aim.Level = Level;

                Level.aims[index] = aim;
            }
        }

        private List<IngridientAimInfo.IngridientItem> CollectIngridients ()
        {
            var ingridients = M3PremadeLevel.ingridients.ToList();
            ingridients.AddRange(tokens.Items
                .Where(token => token.HasComponent<Ingridient>())
                .Select(ingridient => new IngridientAimInfo.IngridientItem(ingridient, 0)));

            return ingridients;
        }

        private void FetchColors ()
        {
            colors = new TokenColor[(int) M3PremadeLevel.size.x, (int) M3PremadeLevel.size.y];
            for (int row = 0; row < M3PremadeLevel.size.x; row++) {
                for (int column = 0; column < M3PremadeLevel.size.y; column++) {
                    if (M3PremadeLevel.elements[row, column].HasComponent<TokenComponents.Color>()) {
                        var color = M3PremadeLevel.elements[row, column].GetColor();
                        colors[row, column] = color == TokenColor.Random
                            ? Utilites.GetRandomColor()
                            : color;
                    }
                }
            }

            //corrector = new BuildingCorrector(colors, M3PremadeLevel.elements);
        }

        private void SetupCellsAndTokens()
        {
            for (int row = 0; row < M3PremadeLevel.size.x; row++) {
                for (int column = 0; column < M3PremadeLevel.size.y; ++column) {
                    var position = fieldOrigin + new Vector2(column * Match3Player.Settings.tokenSize.x, row * Match3Player.Settings.tokenSize.y);
                
                    if (M3PremadeLevel.background[row, column] != null) {
                        Instantiate(M3PremadeLevel.background[row, column], position, Quaternion.identity);
                    }

                    if (M3PremadeLevel.cells[row, column] != null) {
                        var cell = Instantiate(M3PremadeLevel.cells[row, column], position, Quaternion.identity) as GameObject;
                        cell.GetComponent<Movement>().Assign(row, column);
                    }

                    if (M3PremadeLevel.elements[row, column] != null) {
                        tokens[row, column] = Instantiate(M3PremadeLevel.elements[row, column], position,
                            Quaternion.identity) as GameObject;
                        if (tokens[row, column].HasComponent<Movement>()) {
                            tokens[row, column].GetComponent<Movement>().Assign(row, column);
                        }
                    }

                    if (M3PremadeLevel.attachments[row, column] != null) {
                        ElementFactory.Attach(tokens[row, column], M3PremadeLevel.attachments[row, column].name);
                    }
                }
            }
        }


        private void CalculateOrigin ()
        {
            var halfRows = M3PremadeLevel.size.x / 2;
            var halfColumns = M3PremadeLevel.size.y / 2;
            var horizontalCenter = (float) Camera.main.pixelWidth / 2;
            var verticalCenter = (float) Camera.main.pixelHeight / 2;

            var screenCenter = Camera.main.ScreenToWorldPoint(new Vector2(horizontalCenter, verticalCenter));

            var xPosition = screenCenter.x - (int) halfColumns * Match3Player.Settings.tokenSize.x;
            var yPosition = screenCenter.y - (int) halfRows * Match3Player.Settings.tokenSize.y;

            //Correcting for even and odd rows and columns
            xPosition += Mathf.Approximately(halfColumns - (int) halfColumns, 0) ? Match3Player.Settings.tokenSize.x / 2 : 0;
            yPosition += Mathf.Approximately(halfRows - (int) halfRows, 0) ? Match3Player.Settings.tokenSize.y / 2 : 0;

            fieldOrigin = new Vector2(xPosition, yPosition);
            Movement.FieldOrigin = fieldOrigin;
        }

        private void CorrectLevel()
        {
            for (int row = 0; row < M3PremadeLevel.size.x; ++row) {
                for (int column = 0; column < M3PremadeLevel.size.y; ++column) {
                    var color = M3PremadeLevel.elements[row, column].GetColor();
                    if (color == TokenColor.Random) {
                        EnsureNoMatches(row, column);
                    }
                }
            }
        }

        private void ApplyColors ()
        {
            for (int row = 0; row < M3PremadeLevel.size.x; row++) {
                for (int column = 0; column < M3PremadeLevel.size.y; column++) {
                    var color = tokens[row, column] ? tokens[row, column].GetComponent<TokenComponents.Color>() : null;
                    if (color != null) {
                        color.Value = colors[row, column];
                    }
                }
            }
        }

        private void EnsureNoMatches (int row, int column)
        {
            var withoutColor = colors[row, column];

            while (SmallCross(withoutColor, row, column)) {
                colors[row, column] = Utilites.GetRandomColor(withoutColor);
                withoutColor = colors[row, column];
            }

            while (BigCross(withoutColor, row, column)) {
                colors[row, column] = Utilites.GetRandomColor(withoutColor);
                withoutColor = colors[row, column];
            }
        }

        private bool BigCross(TokenColor withoutColor, int row, int column)
        {
            bool top = row < LevelConfig.rows - 2
                       && M3PremadeLevel.elements[row + 1, column] != null && M3PremadeLevel.elements[row + 2, column] != null
                       && colors[row + 1, column] == withoutColor
                       && colors[row + 2, column] == withoutColor;

            bool bottom = row > 1
                          && M3PremadeLevel.elements[row - 1, column] != null && M3PremadeLevel.elements[row - 2, column] != null
                          && colors[row - 1, column] == withoutColor
                          && colors[row - 2, column] == withoutColor;

            bool left = column > 1
                        && M3PremadeLevel.elements[row , column - 1] != null && M3PremadeLevel.elements[row , column - 2] != null
                        && colors[row, column - 1] == withoutColor
                        && colors[row, column - 2] == withoutColor;

            bool right = column < LevelConfig.columns - 2
                         && M3PremadeLevel.elements[row, column + 1] != null && M3PremadeLevel.elements[row, column + 2] != null
                         && colors[row, column + 1] == withoutColor
                         && colors[row, column + 2] == withoutColor;

            return top || bottom || left || right;
        }

        private bool SmallCross(TokenColor withoutColor, int row, int column)
        {
            bool vertical = row > 0 && row < LevelConfig.rows - 1
                            && M3PremadeLevel.elements[row + 1, column] != null && M3PremadeLevel.elements[row - 1, column] != null
                            && colors[row + 1, column] == withoutColor
                            && colors[row - 1, column] == withoutColor;

            bool horizontal = column > 0 && column < LevelConfig.columns - 1
                              && M3PremadeLevel.elements[row, column + 1] != null && M3PremadeLevel.elements[row, column - 1] != null
                              && colors[row, column + 1] == withoutColor
                              && colors[row, column - 1] == withoutColor;

            return vertical || horizontal;
        }

        private void CreateDespawnerSprites ()
        {
            foreach (var position in M3PremadeLevel.despawnPositions) {
                var baseObject = new GameObject("Despawner");
                var offset = Match3Player.Settings.Sprites.despawnerOffset;
                var worldPosition = position.Coordinates(FieldOrigin) + new Vector2(0f, offset);
                baseObject.transform.position = worldPosition;

                var spriteRenderer = baseObject.AddComponent<SpriteRenderer>();
                var sprite = Match3Player.Settings.Sprites.despawnerSprite;
                spriteRenderer.sprite = sprite;
                spriteRenderer.sortingLayerName = "Attachments";
                spriteRenderer.sortingOrder = 1;
            }
        }
    }
}