﻿using MMK.Core;

namespace MMK.Match3
{
    public class Match3Level : Level
    {
        public override TLevelType LoadPremadeLevel<TLevelType>()
        {
            return LevelProvider<TLevelType>.GetLevel(number);
        }
    }
}