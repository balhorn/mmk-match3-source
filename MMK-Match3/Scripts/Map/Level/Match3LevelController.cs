﻿using MMK.Core;

namespace MMK.Match3
{
    public class Match3LevelController : LevelController
    {
        protected override PremadeLevel GetLevelAsset(Level level)
        {
            return LevelProvider<Match3PremadeLevel>.GetLevel(level.number);
        }
    }
}