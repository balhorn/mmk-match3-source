﻿using System.Collections.Generic;
using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MMK.Match3
{
    [ElementEditorMenu("Durability/Creeping chocolate")]
    [RequireComponent(typeof (Movement))]
    public class CreepingChocolate : Durability, IEventListener
    {
        private Movement movement;
        private ITokensCollection tokens;

        private static bool wasChocolateDestroyed;
        private static bool isCreeping;

        private static readonly GravityVector[] Directions = {
            GravityVector.DirectionDown,
            GravityVector.DirectionLeft,
            GravityVector.DirectionRight,
            GravityVector.DirectionUp
        };

        public static event MMK.Core.Delegates.VoidDelegate OnChocolateDestroyed;

        protected override void InitComponents()
        {
            base.InitComponents();
            movement = GetComponent<Movement>();
            tokens = Tokens;
        }

        protected override void SetupScore()
        {
            base.SetupScore();
            Score = Match3Player.Settings.chocolateScore;
        }

        protected override void Start()
        {
            base.Start();
            movement.Block(this);
            BindEvent();
        }

        private void Creep ()
        {
            if (isCreeping || wasChocolateDestroyed) {
                return;
            }

            PositionInfo targetPosition;
            if (!CanCreep(out targetPosition)) {
                return;
            }

            isCreeping = true;

            var targetWorldPosition = tokens[targetPosition].transform.position;
            Destroy(tokens[targetPosition]);

            CreateNew(targetPosition, targetWorldPosition);
            gameObject.SendMessage("OnSpawn", SendMessageOptions.DontRequireReceiver);
        }

        private void CreateNew (PositionInfo targetPosition, Vector3 targetWorldPosition)
        {
            var newChocolate = Instantiate(gameObject, targetWorldPosition, Quaternion.identity) as GameObject;

            Destroy(newChocolate.GetComponent<IceBlocker>());
        
            newChocolate.GetComponent<Movement>().Assign(targetPosition.Row, targetPosition.Column);
            tokens[targetPosition] = newChocolate;
        }

        private bool CanCreep (out PositionInfo creepingPosition)
        {
            var indicies = new List<int>(Directions.Length);
            for (int i = 0; i < Directions.Length; i++) {
                indicies.Add(i);
            }

            var randomIndex = indicies[Random.Range(0, indicies.Count)];
            var chosenDirection = Directions[randomIndex];
            while (!CanCreepInDirection(chosenDirection)) {
                indicies.Remove(randomIndex);

                if (indicies.Count == 0) {
                    creepingPosition = null;
                    return false;
                }

                randomIndex = indicies[Random.Range(0, indicies.Count)];
                chosenDirection = Directions[randomIndex];
            }

            creepingPosition = movement.Position + chosenDirection;
            return true;
        }

        private bool CanCreepInDirection (GravityVector direction)
        {
            var targetPosition = movement.Position + direction;
            var token = tokens[targetPosition];

            if (token == null) {
                return false;
            }

            var chocolate = token.GetComponent<CreepingChocolate>();
            if (chocolate != null) {
                return chocolate.durability < durability;
            }

            return !token.IsBlocked() && !token.Undestroyable();
        }

        public void BindEvent()
        {
            OnTokenDestroyed += Receiver;
            ShapesManager.OnTurnStarted += Creep;
            ShapesManager.OnCollapsingFinish += SwitchVariables;
        }

        private static void SwitchVariables()
        {
            wasChocolateDestroyed = false;
            isCreeping = false;
        }

        private void Receiver(TokenColor color, PositionInfo info, IDamageDealer dealer)
        {
            if (!TheLast) {
                return;
            }

            if ((info.Row != movement.Row || Mathf.Abs(info.Column - movement.Column) != 1)
                && (info.Column != movement.Column || Mathf.Abs(info.Row - movement.Row) != 1)) {
                return;
            }

            if (!(dealer is Match) && !(dealer is ShapesManager)) {
                return;
            }

            TakeDamage(dealer);
        }

        public void UnbindEvent()
        {
            OnTokenDestroyed -= Receiver;
            ShapesManager.OnTurnStarted -= Creep;
            ShapesManager.OnCollapsingFinish -= SwitchVariables;
        }

        protected override void Disable()
        {
            base.Disable();
            ScoreManager.RecordScore(this);
            wasChocolateDestroyed = true;
        }

        protected override void FireEvent()
        {
            if (OnChocolateDestroyed != null) {
                OnChocolateDestroyed();
            }
        }

        private void OnDestroy ()
        {
            UnbindEvent();
        }
    }
}
