﻿using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    [AddComponentMenu("Match3/Cells/Cell")]
    [ElementEditorMenu("Cells/Regular cell")]
    [RequireComponent(typeof (Movement))]
    public class Cell : Durability, IEventListener
    {
        protected virtual void TokenDestroyed(TokenColor color, PositionInfo info, IDamageDealer dealer)
        {

        }

        protected override void Start ()
        {
            base.Start();
            BindEvent();
        }

        private void OnDestroy()
        {
            UnbindEvent();
        }

        public void BindEvent()
        {
            OnTokenDestroyed += TokenDestroyed;
        }

        public void UnbindEvent()
        {
            OnTokenDestroyed -= TokenDestroyed;
        }
    }
}
