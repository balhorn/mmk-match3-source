﻿using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    public class FieldOutline : MonoBehaviour 
    {
        public Sprite line;
        public Sprite angle;
        public float lineOffset;
        public float cornerOffset;

        public Sprite Line
        {
            get { return line; }
            set { line = value; }
        }

        public Sprite Angle
        {
            get { return angle; }
            set { angle = value; }
        }

        public float LineOffset
        {
            get { return lineOffset; }
            set { lineOffset = value; }
        }

        public float CornerOffset
        {
            get { return cornerOffset; }
            set { cornerOffset = value; }
        }

        public void Build ()
        {
            for (int row = 0; row < LevelConfig.rows; row++) {
                for (int column = 0; column < LevelConfig.columns; column++) {
                    BuildToken(row, column);
                }
            }
        }

        private static Vector2 TokenSize => Match3Player.Settings.tokenSize;

        private static ITokensCollection Tokens => ElementComponent.Tokens;


        private void BuildToken (int row, int column)
        {
            if (Tokens[row, column] == null) {
                return;
            }
        
            var leftSideEmpty = IsNullOrOut(row, column - 1);
            var rightSideEmpty = IsNullOrOut(row, column + 1);
            var topSideEmpty = IsNullOrOut(row - 1, column);
            var bottomSideEmpty = IsNullOrOut(row + 1, column);

            var neCornerSet = false;
            var nwCornerSet = false;
            var seCornerSet = false;
            var swCornerSet = false;

            //Check two top corners and place line if necessary
            if (topSideEmpty && leftSideEmpty) {
                SetNWCorner(GetPosition(row, column));
                nwCornerSet = true;
            } else if (topSideEmpty && rightSideEmpty) {
                SetNECorner(GetPosition(row, column));
                neCornerSet = true;
            } else if (topSideEmpty) {
                SetHorizontalLine(GetPosition(row, column), Vector2.up);
            }

            //Check two bottom corners and place line if necessary
            if (bottomSideEmpty && rightSideEmpty) {
                SetSECorner(GetPosition(row, column));
                seCornerSet = true;
            } else if (!neCornerSet && rightSideEmpty) {
                SetVerticalLine(GetPosition(row, column), Vector2.right);
            }

            //Check two down corners and place line if necessary
            if (bottomSideEmpty && leftSideEmpty) {
                SetSWCorner(GetPosition(row, column));
                swCornerSet = true;
            } else if (!seCornerSet && bottomSideEmpty) {
                SetHorizontalLine(GetPosition(row, column), Vector2.down);
            }

            //Check if the left line should be spawned
            if (!swCornerSet && !nwCornerSet && leftSideEmpty) {
                SetVerticalLine(GetPosition(row, column), Vector2.left);
            }
        }

        private void SetHorizontalLine (Vector2 tokenPosition, Vector2 offsetDirection)
        {
            SetLine(tokenPosition, offsetDirection, false);
        }

        private void SetVerticalLine (Vector2 tokenPosition, Vector2 offsetDirection)
        {
            SetLine(tokenPosition, offsetDirection, true);
        }

        private void SetLine (Vector2 tokenPosition, Vector2 offsetDirection, bool flip)
        {
            var line = NewLine(flip);
            var tokenSize = Mathf.Abs(offsetDirection.x) > 1 ? TokenSize.x : TokenSize.y;

            line.transform.position = tokenPosition + offsetDirection * (tokenSize + LineOffset);
        }

        /// <summary>
        /// North-west corner
        /// </summary>
        /// <param name="tokenPosition"></param>
        private void SetNWCorner (Vector2 tokenPosition)
        {
            var offset = (Vector2.up + Vector2.left).normalized;
            SetCorner(tokenPosition, offset, false, false);
        }

        /// <summary>
        /// North-east corner
        /// </summary>
        /// <param name="tokenPosition"></param>
        private void SetNECorner (Vector2 tokenPosition)
        {
            var offset = (Vector2.up + Vector2.right).normalized;
            SetCorner(tokenPosition, offset, false, true);
        }

        /// <summary>
        /// South-west corner
        /// </summary>
        /// <param name="tokenPosition"></param>
        private void SetSWCorner (Vector2 tokenPosition)
        {
            var offset = (Vector2.down + Vector2.left).normalized;
            SetCorner(tokenPosition, offset, true, false);
        }

        /// <summary>
        /// South-east corner
        /// </summary>
        /// <param name="tokenPosition"></param>
        private void SetSECorner (Vector2 tokenPosition)
        {
            var offset = (Vector2.down + Vector2.right).normalized;
            SetCorner(tokenPosition, offset, true, true);
        }

        private void SetCorner (Vector2 tokenPosition, Vector2 offsetDirection, bool xFlip, bool yFlip)
        {
            var corner = NewCorner(xFlip, yFlip);

            corner.transform.position = tokenPosition + offsetDirection * CornerOffset;
        }

        /// <summary>
        /// Generates a line with the specified flip
        /// </summary>
        /// <param name="flip">Whether to flip this line around z-axis</param>
        /// <returns></returns>
        private GameObject NewLine (bool flip)
        {
            var result = new GameObject("outline_line");
            result.transform.rotation = Quaternion.AngleAxis(flip ? 90f : 0, Vector3.forward);

            var spriteRend = result.AddComponent<SpriteRenderer>();
            spriteRend.sprite = line;
            spriteRend.sortingLayerName = "Background";
            spriteRend.sortingOrder = 1;

            return result;
        }

        /// <summary>
        /// Generates a corner flipped in the specified direction
        /// </summary>
        /// <param name="flipX">Flip X parameter of the sprite renderer</param>
        /// <param name="flipY">Flip Y parameter of the sprite renderer</param>
        /// <returns></returns>
        private GameObject NewCorner (bool flipX, bool flipY)
        {
            var result = new GameObject("outline_corner");
            var spriteRend = result.AddComponent<SpriteRenderer>();
            spriteRend.sprite = angle;
            spriteRend.flipY = flipY;
            spriteRend.flipX = flipX;
            spriteRend.sortingLayerName = "Background";
            spriteRend.sortingOrder = 1;

            return result;
        }

        private static bool IsNullOrOut (int row, int column)
        {
            return row < 0 || row >= LevelConfig.rows || column < 0 || column >= LevelConfig.columns ||
                   Tokens[row, column] == null;
        }

        private static Vector2 GetPosition (int row, int column)
        {
            return new PositionInfo(row, column).Coordinates(Level.Current.Builder.FieldOrigin);
        }
    }
}
