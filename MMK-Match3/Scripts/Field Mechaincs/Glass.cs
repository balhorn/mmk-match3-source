﻿using MMK.Core;
using MMK.Match3.TokenComponents;

namespace MMK.Match3
{
    [ElementEditorMenu("Durability/Glass")]
    public class Glass : Durability, IEventListener
         {
        private Movement movement;

        public static event Core.Delegates.VoidDelegate OnGlassBroken;

        protected override void Start()
        {
            base.Start();

            movement.Block(this);

            BindEvent();
        }

        public override void TakeDamage(IDamageDealer damageDealer)
        {
            if (damageDealer != null) {
                return;
            }

            if (--durability > 0) {
                UpdateAnimation();
            } else {
                Disable();
            }
        }

        protected override void InitComponents()
        {
            base.InitComponents();
            movement = GetComponent<Movement>();
        }

        protected override void Disable()
        {
            UpdateAnimation();
            FireEvent();
            Graphics.RenderLandingObject(false);

            movement.Release(this);
            enabled = false;

            gameObject.SendMessage("OnDurabilityDisabled");
        }

        protected override void FireEvent()
        {
            if (OnGlassBroken != null) {
                OnGlassBroken();
            }
        }

        private void EventReciever (TokenColor color, PositionInfo info, IDamageDealer dealer)
        {
            var position = GetComponent<Movement>().Position;
            if (position.Row == info.Row 
                && (position.Column == info.Column - 1 || position.Column == info.Column + 1)
                || (position.Column == info.Column) 
                && (position.Row == info.Row - 1 || position.Row == info.Row + 1)) {
                TakeDamage(null);
            }
        }

        public void BindEvent()
        {
            OnTokenDestroyed += EventReciever;
        }

        public void UnbindEvent()
        {
            OnTokenDestroyed -= EventReciever;
        }

        private void OnDestroy ()
        {
            UnbindEvent();
        }
    }
}
