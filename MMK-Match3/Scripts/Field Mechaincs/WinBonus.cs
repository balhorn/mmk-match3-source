﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MMK.Match3
{
    public class WinBonus : MonoBehaviour
    {
        [Tooltip("Delay between level end and win bonus beginning")]
        public float delayBeforeRunning = 1f;
        [Tooltip("Delay between explosion of existing bonuses and placing new ones")]
        public float delayAfterFirstExplosion = 0f;
        [Tooltip("Delay between placing two new bonuses")]
        public float delayBetweenPlacingBonuses = 0.5f;
        [Tooltip("Delay between placing new bonuses and exploding them")]
        public float delayAfterPlacingNewBonuses = 0f;
        [Tooltip("Delay between explosion and level end")]
        public float delayAfterRunning = 2f;

        private ShapesManager shapesManager;
        private BonusManager bonusManager;
        private ITokensCollection Tokens => ElementComponent.Tokens;

        private bool BonusesExist
        {
            get { return Tokens.Items.Any(token => token.HasComponent<Bonus>()); }
        }

        private bool isCollapsing;

        private Func<bool> collapsingFinished;

        private void Start ()
        {
            shapesManager = ShapesManager.Instance;
            bonusManager = BonusManager.Instance;
            collapsingFinished = () => !isCollapsing;
        }

        internal void OnCollapsingFinished ()
        {
            isCollapsing = false;
        }

        internal void OnCollapsingStarted ()
        {
            isCollapsing = true;
        }

        internal IEnumerator RunningCoroutine ()
        {
            yield return new WaitUntil(collapsingFinished);
            yield return new WaitForSeconds(delayBeforeRunning);
            yield return StartCoroutine(ExplodeExistedBonuses());
            yield return new WaitForSeconds(delayAfterRunning);
        }

        private int time;
        private List<Bonus> bonuses = new List<Bonus>(); 
        private IEnumerable<Bonus> Bonuses
        {
            get
            {
                if (bonuses.Any(bonus => bonus != null)) {
                    return bonuses.Where(bonus => bonus != null);
                }

                bonuses = Tokens.Items
                    .Where(item => item != null)
                    .Select(item => item.GetComponent<Bonus>())
                    .Where(bonus => bonus != null && bonus.explosionOrder == time)
                    .ToList();

                return bonuses;
            }
        }

        private Constraint Constraint => Constraint.Current;

        private IEnumerator ExplodeExistedBonuses ()
        {
            while (BonusesExist) {
                for (time = 1; time <= Bonus.BonusesAmount; ++time) {
                    foreach (var bonus in Bonuses) {
                        bonus.Activate();
                        yield return new WaitForSeconds(bonus.explosionDelay);
                    }
                }
            }

            yield return new WaitUntil(collapsingFinished);

            if (Constraint.Amount <= 0 || !LevelConfig.winBonusEnabled) {
                yield break;
            }

            yield return new WaitForSeconds(delayAfterFirstExplosion);
            yield return StartCoroutine(PlaceNewBonuses());
        }


        private IEnumerator PlaceNewBonuses ()
        {
            var spaceAvailable = Tokens.Items.Count(token => !token.IsBlocked() && !token.Undestroyable());
            var totalAmount = Constraint.Amount;
            for (int iterations = Constraint.Amount; iterations > 0; --iterations) {
                if (totalAmount - iterations >= spaceAvailable) {
                    break;
                }

                PositionInfo position;

                do {
                    var row = Random.Range(0, LevelConfig.rows);
                    var column = Random.Range(0, LevelConfig.columns);
                    position = new PositionInfo { Row = row, Column = column };
                } while (Tokens[position].HasComponent<Bonus>() || Tokens[position].IsBlocked() || Tokens[position].Undestroyable());

                var color = Tokens[position].GetColor();
                var bonusPrefab = bonusManager.GetWinBonus(color);

                Destroy(Tokens[position]);
                shapesManager.CreateBonus(bonusPrefab, position);

                Constraint.Decrement();

                yield return new WaitForSeconds(delayBetweenPlacingBonuses);
            }

            yield return new WaitForSeconds(delayAfterPlacingNewBonuses);
            yield return StartCoroutine(ExplodeExistedBonuses());
        }
    }
}
