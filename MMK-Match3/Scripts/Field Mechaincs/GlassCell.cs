﻿using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    [ElementEditorMenu("Cells/Glass cell")]
    public class GlassCell : Cell
    {
        private Animator anim;
        private Movement movement;

        public static event Core.Delegates.VoidDelegate OnGlassCellDestroyed;

        protected override void Start ()
        {
            base.Start ();
            movement = GetComponent<Movement>();
            anim = GetComponent<Animator> ();
        }

        protected override void SetupScore()
        {
            DependsOnCascade = true;
            Score = Match3Player.Settings.cookieScore;
        }

        protected override void TokenDestroyed (TokenColor color, PositionInfo info, IDamageDealer dealer)
        {
            if (movement.Position.Equals(info)) {
                Break();
            }
        }

        private void Break()
        {
            if (--durability >= 0) {
                UpdateAnimation();
            }

            ScoreManager.RecordScore(this);

            if (durability > 0) {
                return;
            }

            enabled = false;
        }

        protected override void UpdateAnimation ()
        {
            if (Animation == null) {
                Debug.Log($"Missing animation on {gameObject.name}. Unable to update");
                return;
            }

            int hash = Animation.StartAnimation();
            Animation.SetInteger(DurabilityParameter, durability);
            Animation.Trigger();
            Animation.EndAnimation(hash, () => {
                if (durability > 0) {
                    return;
                }

                Destroy(gameObject);
                OnGlassCellDestroyed?.Invoke();
            });
        }
    }
}
