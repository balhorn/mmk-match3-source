﻿using System.Linq;
using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;
using Animation = MMK.Match3.TokenComponents.Animation;
using Color = UnityEngine.Color;
using Graphics = MMK.Match3.TokenComponents.Graphics;

namespace MMK.Match3
{
    [RequireComponent(typeof (Movement))]
    [AddComponentMenu("Match3/Ingridient")]
    public class Ingridient : ElementComponent, IGraphicsHandler, IMatch3ScoreEntity
    {
        public Sprite uiSprite;

        private Movement movement;

        [SerializeField] [HideInInspector] private Graphics graphics;
        [SerializeField] [HideInInspector] private Animation animation;

        public Graphics Graphics { get { return graphics; } set { graphics = value; } }
        public Animation Animation { get { return animation; } set { animation = value; } }

        public Sprite Sprite
        {
            get
            {
                if (uiSprite == null) {
                    return Graphics?.Sprite;
                }

                return uiSprite;
            }
        }

        public static event Core.Delegates.SpriteDelegate IngridientCollected;

        public int Score { get; set; }
        public bool DependsOnCascade { get; set; }
        public Color TextColor { get; set; }
        public Vector2 Coordinates { get; set; }

        private void Start ()
        {
            DependsOnCascade = false;
            Score = Match3Player.Settings.ingridientScore;

            Utilites.FindGraphicsAndAnimation(gameObject, this);

            movement = GetComponent<Movement>();
        }

        private void OnMovementFinished ()
        {
            if (!Tokens.DespawnPositions.Any(position => position.Equals(movement.Position))) {
                return;
            }

            Collect();
        }

        private void Collect ()
        {
            Coordinates = transform.position;
            ScoreManager.RecordScore(this);
            FireEvent();
            StartCoroutine(Animation.Play());
        }

        public void SetupGraphics(Graphics graphics)
        {
            Graphics = graphics;
        }

        public void SetupAnimation(Animation animation)
        {
            Animation = animation;
            Animation.graphics = Graphics;
        }

        private void FireEvent ()
        {
            IngridientCollected?.Invoke(Sprite);
        }
    }
}
