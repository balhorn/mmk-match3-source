﻿using MMK.Core;
using MMK.Match3.TokenComponents;

using UnityEngine;
using Animation = MMK.Match3.TokenComponents.Animation;

namespace MMK.Match3
{
    [ElementEditorMenu("Durability/Ice blocker")]
    public class IceBlocker : Durability
    {
        private Movement movement;

        public static event Core.Delegates.VoidDelegate OnIceBroken;

        protected override void Start()
        {
            base.Start();
            movement.Block(this);
        }

        protected override void InitComponents()
        {
            base.InitComponents();
            movement = GetComponent<Movement>();
        }

        protected override void Disable()
        {
            UpdateAnimation();
            FireEvent();
            Graphics.RenderLandingObject(false);

            movement.Release(this);
            enabled = false;

            gameObject.SendMessage("OnDurabilityDisabled");
        }

        protected override void UpdateAnimation ()
        {
            if (Animation == null) {
                Debug.Log($"Missing animation on {gameObject.name}. Unable to update");
                return;
            }

            int hash = Animation.StartAnimation();
            Animation.SetInteger(DurabilityParameter, durability);
            Animation.Trigger();
            Animation.EndAnimation(hash, () => Graphics.RenderLandingObject(false));
        }

        protected override void FireEvent ()
        {
            if (OnIceBroken != null) {
                OnIceBroken();
            }
        }

        public override void SetupAnimation(Animation animation)
        {
            base.SetupAnimation(animation);
            this.Animation.graphics = Graphics;
        }

        protected override void SetupScore()
        {
            Score = Match3Player.Settings.iceScore;
            DependsOnCascade = false;
        }

        private void OnDestroy ()
        {
            Graphics.RenderLandingObject(false);
            movement.Release(this);
        }
    }
}
