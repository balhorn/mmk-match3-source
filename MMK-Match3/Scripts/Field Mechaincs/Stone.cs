﻿using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    [RequireComponent(typeof(Movement))]
    [ElementEditorMenu("Durability/Stone")]
    public class Stone : Durability, IEventListener
    {
        private Movement movement;
        public static event Core.Delegates.VoidDelegate OnStoneBroken;

        protected override void Start()
        {
            movement = GetComponent<Movement>();
            movement.Block(this);

            base.Start();

            BindEvent();
        }

        private void OnDestroy ()
        {
            UnbindEvent();
        }

        protected override void Disable()
        {
            Animation.SetInteger(DurabilityParameter, durability);
            ScoreManager.RecordScore(this);
            StartCoroutine(Animation.Play());
        }

        protected override void SetupScore()
        {
            DependsOnCascade = false;
            Score = Match3Player.Settings.stoneScore;
        }

        protected override void FireEvent()
        {
            if (OnStoneBroken != null) {
                OnStoneBroken();
            }
        }

        private void TokenDestroyed (TokenColor color, PositionInfo info, IDamageDealer dealer)
        {
            if (!TheLast) {
                return;
            }

            var position = movement.Position;

            if ((position.Row != info.Row || Mathf.Abs(info.Column - position.Column) != 1) 
                && (position.Column != info.Column || Mathf.Abs(position.Row - info.Row) != 1)) {
                return;
            }

            if (dealer is Match || dealer is ShapesManager) {
                TakeDamage(dealer);
            }
        }

        public void BindEvent()
        {
            OnTokenDestroyed += TokenDestroyed;
        }

        public void UnbindEvent()
        {
            OnTokenDestroyed -= TokenDestroyed;
        }
    }
}
