﻿using UnityEngine;
using System.Collections.Generic;
using MMK.Core;
using MMK.Match3.Aims;

namespace MMK.Match3
{
    public class Match3PremadeLevel : PremadeLevel
    {
        public Vector2 size;

        public bool chessBackground = true/*MMK.edition != Edition.Community*/;

        public string progressBarType;

        public List<BoosterInfo> preboosters = new List<BoosterInfo>();
        public List<IngridientAimInfo.IngridientItem> ingridients = new List<IngridientAimInfo.IngridientItem>();

        public GameObjectsSerializedMatrix cells;
        public GameObjectsSerializedMatrix background;
        public GameObjectsSerializedMatrix elements;
        public GameObjectsSerializedMatrix attachments;

        public List<PositionInfo> spawnPositions = new List<PositionInfo>();
        public List<PositionInfo> despawnPositions = new List<PositionInfo>();
        public List<PositionInfo> ingridientSpawners = new List<PositionInfo>();

        public LevelType type;

        public void InitLevelConfig ()
        {
            LevelConfig.rows = (int)size.x;
            LevelConfig.columns = (int)size.y;
            LevelConfig.turns = constraintAmount;
            LevelConfig.oneStarScore = progressValues[0];
            LevelConfig.twoStarsScore = progressValues[1];
            LevelConfig.threeStarsScore = progressValues[2];
        }
    }
}