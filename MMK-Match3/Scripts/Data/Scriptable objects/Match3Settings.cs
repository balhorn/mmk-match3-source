﻿using System.Collections.Generic;
using MMK.Core;
using MMK.Core.Ads;
using UnityEngine;

namespace MMK.Match3
{
    [CreateAssetMenu(menuName = "MMK/Game settings", fileName = "GameSettings")]
    public class Match3Settings : GameSettings
    {
        public float pixelsPerUnit = 216f;

        [Tooltip("Adjust this property until your tokens don't fill the board tightly")]
        public Vector2 tokenSize = new Vector2(0.635f, 0.635f);

        public int maximumIceDurability = 2;
        public int maximumStoneDurability = 2;
        public int maximumCellDurability = 1;

        public float tokensMovementSpeed = 15f;

        public string regularTokenName = "Regular token";
        public string regularCellName = "Regular cell";

        public GameObject defaultTokenPrefab;
        public GameObject defaultBackroundPrefab;
        public GameObject defaultHolePrefab;


        public int matchThreeScore = 60;
        public int matchFourScore = 120;
        public int matchSquareScore = 120;
        public int matchFiveScore = 200;
        public int matchTScore = 200;
        public int matchCornerScore = 200;

        public int bombExplosionScore = 540;

        public int iceScore = 0;
        public int cookieScore = 1000;
        public int stoneScore = 20;
        public int chocolateScore = 20;
        public int ingridientScore = 10000;

        public string rateUsURL;

        [Tooltip("Cost of additional turns in hard currency")]
        public int additionalTurnCost = 9;

        [Tooltip("This value will be added to the additional turn cost if player continues to lose repeatively." +
                 " Also, it will be incremented by one on each further loss")]
        public int baseCostStep = 3;

        [Tooltip("This value will be added to the step on each loss after the second one")]
        public int incrementStepBy = 1;

        public int additionalTurnsAmount = 5;

        [HideInInspector]
        public AdsSettings adsSettings = new AdsSettings();

        public SpriteSettings Sprites { get; private set; }
        public TextSettings Texts { get; private set; }

        public const string SpriteSettingsName = "Sprite settings";
        public const string TextSettingsName = "Text settings";
        public const string ShopSettingsName = "Monetization Settings";


        protected override void OnEnable ()
        {
            base.OnEnable();

            Sprites = Resources.Load<SpriteSettings>(SpriteSettingsName);
            Texts = Resources.Load<TextSettings>(TextSettingsName);

            if (Sprites == null) {
                Debug.LogError($"Could not load sprites settings object. Please, change its name to '{SpriteSettingsName}' or create a new one");
            }

            if (Texts == null) {
                Debug.LogError($"Could not load text settings object. Please, change its name to '{TextSettingsName}' or create a new one");
            }
        }
    }
}