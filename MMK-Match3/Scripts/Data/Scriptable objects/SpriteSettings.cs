﻿using System;
using UnityEngine;
using System.Collections.Generic;
using MMK.Core.Boosters;

namespace MMK.Match3
{
    [CreateAssetMenu(menuName = "MMK/Sprite settings")]
    public class SpriteSettings : ScriptableObject
    {
        public Sprite lockedSprite;
        public Sprite numberIcon;
        public Sprite plusIcon;

        [HideInInspector] public Sprite despawnerSprite;
        [HideInInspector] public float  despawnerOffset;
        [HideInInspector] public GameObject whiteBackground;
        [HideInInspector] public GameObject blackBackground;

        [HideInInspector] public Sprite ingridientDespawnerSprite;

        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        [SerializeField] [HideInInspector] private List<TypeToSpriteItem> typeToSprites =
            new List<TypeToSpriteItem>();

        [HideInInspector] public List<TokenSpriteItem> tokenSprites = new List<TokenSpriteItem>();
        [HideInInspector] public List<BonusSpriteItem> bonusSprites = new List<BonusSpriteItem>();

        #region Data classes

        [Serializable]
        private class TypeToSpriteItem
        {
            public string name;
            public Sprite sprite;

            public TypeToSpriteItem(string name, Sprite sprite)
            {
                this.name = name;
                this.sprite = sprite;
            }
        }

        [Serializable]
        public class TokenSpriteItem
        {
            public TokenColor color;
            public Sprite sprite;
            public GameObject particlePrefab;

            public TokenSpriteItem()
            {

            }

            public TokenSpriteItem(TokenColor color, Sprite sprite, GameObject particlePrefab)
            {
                this.color = color;
                this.sprite = sprite;
                this.particlePrefab = particlePrefab;
            }
        }

        [Serializable]
        public class BonusSpriteItem
        {
            public TokenColor color;
            public MatchShape shape;
            public Sprite sprite;
            public GameObject particlePrefab;

            public BonusSpriteItem()
            {

            }

            public BonusSpriteItem(TokenColor color, MatchShape shape, Sprite sprite, GameObject particlePrefab)
            {
                this.color = color;
                this.shape = shape;
                this.sprite = sprite;
                this.particlePrefab = particlePrefab;
            }
        }

        #endregion

        public void Set(string key, Sprite value)
        {
            var element = typeToSprites.Find(item => item.name == key);
            if (element != null) {
                element.sprite = value;
            } else {
                typeToSprites.Add(new TypeToSpriteItem(key, value));
            }
        }

        public Sprite this[string key]
        {
            get
            {
                var element = typeToSprites.Find(item => item.name == key);
                return element != null ? element.sprite : null;
            }
        }

        public Sprite this[AbstractBooster key]
        {
            get
            {
                var typeName = key.GetType().Name;
                return this[typeName];
            }
        }

        public Sprite GetSprite (TokenColor color)
        {
            return tokenSprites.Find(item => item.color == color)?.sprite;
        }

        public Sprite GetSprite (TokenColor color, MatchShape shape)
        {
            return bonusSprites.Find(item => item.color == color && item.shape == shape)?.sprite;
        }

        public GameObject GetParticles (TokenColor color)
        {
            return tokenSprites.Find(item => item.color == color)?.particlePrefab;
        }

        public GameObject GetParticles (TokenColor color, MatchShape shape)
        {
            return bonusSprites.Find(item => item.color == color && item.shape == shape)?.particlePrefab;
        }
    }
}