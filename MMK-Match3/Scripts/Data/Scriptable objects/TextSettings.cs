﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MMK.Match3
{
    [CreateAssetMenu(menuName = "MMK/Text settings")]
    public class TextSettings : ScriptableObject
    {
        [Serializable]
        public class LevelTypeText
        {
            public LevelType levelType;
            public Sprite sprite;
            public string text;

            public LevelTypeText()
            {

            }

            public LevelTypeText(LevelType levelType, Sprite sprite, string text)
            {
                this.levelType = levelType;
                this.sprite = sprite;
                this.text = text;
            }
        }

        public List<LevelTypeText> texts = new List<LevelTypeText>();

        public LevelTypeText GetLevelTypeText (LevelType type)
        {
            return texts.Find(text => text.levelType == type);
        }

        public void SetLevelTypeText (LevelType type, string text, Sprite sprite)
        {
            var textItem = GetLevelTypeText(type);
            if (textItem != null) {
                textItem.text = text;
                textItem.sprite = sprite;
                return;
            }

            textItem = new LevelTypeText(type, sprite, text);
            texts.Add(textItem);
        }
    }
}