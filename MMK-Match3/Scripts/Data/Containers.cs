﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using MMK.Match3;
using MMK.Match3.Preboosters;
using UnityEngine;

namespace MMK.Match3
{
    [Serializable]
    public class PreboosterSprite
    {
        public Sprite sprite;
        public int amount = 1;
    }
}