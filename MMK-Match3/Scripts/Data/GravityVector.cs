﻿using System;
using UnityEngine;

namespace MMK.Match3
{
    /// <summary>
    /// Data structure to represent movement direction for tokens.
    /// Consider this as Vector2 which components can have value of 0, 1 or -1
    /// </summary>
    [Serializable]
    public class GravityVector
    {
        public static readonly GravityVector DirectionUp = new GravityVector(Vector2.up);
        public static readonly GravityVector DirectionDown = new GravityVector(Vector2.down);
        public static readonly GravityVector DirectionLeft = new GravityVector(Vector2.left);
        public static readonly GravityVector DirectionRight = new GravityVector(Vector2.right);

        [SerializeField]
        private Vector2 gravity;

        public GravityVector (int x, int y)
        {
            gravity = NormalizeVector(new Vector2(x, y));
        }

        public GravityVector (Vector2 gravity)
        {
            this.gravity = NormalizeVector(gravity);
        }

        /// <summary>
        /// Adds another Gravity vector to current.
        /// </summary>
        /// <param name="vector">Vector to add</param>
        /// <returns></returns>
        public Vector2 Combine (Vector2 vector)
        {
            return NormalizeVector(gravity + vector);
        }

        public Vector2 Value
        {
            get { return NormalizeVector(gravity); }
            set { gravity = NormalizeVector(value); }
        }

        /// <summary>
        /// Switches the sign of both components of vector and returns result
        /// </summary>
        public GravityVector Revert => new GravityVector(-Value);

        /// <summary>
        /// Returns vector at 45 degrees clockwise from current
        /// </summary>
        public GravityVector Left
        {
            get
            {
                var currentGravity = Value;
                var result = currentGravity;
                if (currentGravity == Vector2.down
                    || currentGravity == Vector2.one
                    || currentGravity == Vector2.up
                    || currentGravity == -Vector2.one) {
                    result.x -= result.y;
                }

                if (currentGravity == Vector2.left
                    || currentGravity == Vector2.right + Vector2.down
                    || currentGravity == Vector2.right
                    || currentGravity == Vector2.up + Vector2.left) {
                    result.y += result.x;
                }

                return new GravityVector(result);
            }
        }

        /// <summary>
        /// Returns vector at 45 degrees counter-clockwise from current
        /// </summary>
        public GravityVector Right
        {
            get
            {
                var currentGravity = Value;
                var result = currentGravity;
                if (currentGravity == Vector2.up
                    || currentGravity == Vector2.down + Vector2.right
                    || currentGravity == Vector2.down
                    || currentGravity == Vector2.up + Vector2.left) {
                    result.x += result.y;
                }

                if (currentGravity == Vector2.right + Vector2.up
                    || currentGravity == Vector2.left + Vector2.down
                    || currentGravity == Vector2.right
                    || currentGravity == Vector2.left) {
                    result.y -= result.x;
                }

                return new GravityVector(result);
            }
        }

        /// <summary>
        /// Normalize vector component-vise, so that each component has value 0, 1 or -1
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        private static Vector2 NormalizeVector (Vector2 vector)
        {
            var x = Mathf.Abs(vector.x) == 1 || vector.x == 0 ? vector.x : vector.x / Mathf.Abs(vector.x);
            var y = Mathf.Abs(vector.y) == 1 || vector.y == 0 ? vector.y : vector.y / Mathf.Abs(vector.y);

            return new Vector2(x, y);
        }
    }
}