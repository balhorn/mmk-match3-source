﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MMK.Core;
using MMK.Match3.TokenComponents;
using Random = UnityEngine.Random;

namespace MMK.Match3
{
    public class TokensArray : ITokensCollection
    {
        private readonly GameObject[,]    tokens;
        private readonly GravityVector[,] gravity;

        private int rows, columns;

        public TokensArray (int rows, int columns)
        {
            this.rows = rows;
            this.columns = columns;

            //Movement.TokensCollection = this;
            ElementComponent.Tokens = this;
            tokens = new GameObject[rows, columns];
            gravity = new GravityVector[rows, columns];

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    gravity[i, j] = new GravityVector(Vector2.down);
                }
            }
        }

        public GameObject this[PositionInfo info]
        {
            get
            {
                try {
                    return tokens[info.Row, info.Column];
                } catch (IndexOutOfRangeException) {
                    return null;
                }
            }
            set
            {
                try {
                    tokens[info.Row, info.Column] = value;
                } catch (IndexOutOfRangeException) {
                }
            }
        }

        public GameObject this[int row, int column]
        {
            get
            {
                try {
                    return tokens[row, column];
                } catch (IndexOutOfRangeException) {
                    return null;
                }
            }
            set
            {
                try {
                    tokens[row, column] = value;
                } catch (IndexOutOfRangeException) {
                }
            }
        }

        public List<PositionInfo> SpawnPositions { get; set; }
        public List<PositionInfo> BottomPositions { get; set; }
        public List<PositionInfo> DespawnPositions { get; set; }
        public List<PositionInfo> IngridientSpawners { get; set; }

        public GameObject[,] Cells { get; set; }

        private GameObject backupG1;
        private GameObject backupG2;

        public void Swap (GameObject first, GameObject second)
        {
            var firstMovement = first.GetComponent<Movement>();
            var secondMovement = second.GetComponent<Movement>();

            if (firstMovement == null || secondMovement == null) {
                return;
            }

            backupG1 = first;
            backupG2 = second;

            var firstPosition = new PositionInfo(firstMovement);
            var secondPosition = new PositionInfo(secondMovement);

            var temp = tokens[firstPosition.Row, firstMovement.Column];
            tokens[firstPosition.Row, firstPosition.Column] = tokens[secondPosition.Row, secondPosition.Column];
            tokens[secondPosition.Row, secondPosition.Column] = temp;

            Movement.SwapPosition(firstMovement, secondMovement);
        }

        public void UndoSwap ()
        {
            if (backupG1 == null || backupG2 == null) {
                throw new Exception("Backup is null");
            }

            Swap(backupG1, backupG2);
        }

        public GravityVector GetGravity (PositionInfo info)
        {
            return gravity[info.Row, info.Column];
        }

        public IEnumerable<GameObject> GetMatches (IEnumerable<GameObject> gos)
        {
            List<GameObject> matches = new List<GameObject>();
            foreach (var go in gos) {
                matches.AddRange(GetMatches(go).MatchedTokens);
            }
            return matches.Distinct();
        }

        public TokensBundle GetMatches (GameObject go)
        {
            TokensBundle matchesInfo = new TokensBundle();

            var horizontalMatches = GetMatchesHorizontally(go);
            matchesInfo.AddObjectRange(horizontalMatches);

            var verticalMatches = GetMatchesVertically(go);
            matchesInfo.AddObjectRange(verticalMatches);

            return matchesInfo;
        }

        private IEnumerable<GameObject> GetMatchesVertically (GameObject go)
        {
            List<GameObject> matches = new List<GameObject>();
            if (go == null) {
                return matches;
            }
            matches.Add(go);
            var movement = go.GetComponent<Movement>();
            //check bottom
            if (movement.Row != 0)
                for (int row = movement.Row - 1; row >= 0; row--) {
                    if (tokens[row, movement.Column] != null &&
                        tokens[row, movement.Column].TheSameColor(go) && tokens[row, movement.Column].IsMatch()) {
                        matches.Add(tokens[row, movement.Column]);
                    } else {
                        break;
                    }
                }

            //check top
            if (movement.Row != LevelConfig.rows - 1)
                for (int row = movement.Row + 1; row < LevelConfig.rows; row++) {
                    if (tokens[row, movement.Column] != null &&
                        tokens[row, movement.Column].TheSameColor(go) && tokens[row, movement.Column].IsMatch()) {
                        matches.Add(tokens[row, movement.Column]);
                    } else {
                        break;
                    }
                }


            if (matches.Count < Constants.MinimumMatches)
                matches.Clear();

            return matches.Distinct();
        }

        private IEnumerable<GameObject> GetMatchesHorizontally (GameObject go)
        {
            List<GameObject> matches = new List<GameObject>();
            if (go == null) {
                return matches;
            }
            matches.Add(go);
            var movement = go.GetComponent<Movement>();
            //check left
            if (movement.Column != 0) {
                for (int column = movement.Column - 1; column >= 0; column--) {
                    if (tokens[movement.Row, column] != null
                        && tokens[movement.Row, column].TheSameColor(go) && tokens[movement.Row, column].IsMatch()) {
                        matches.Add(tokens[movement.Row, column]);
                    } else {
                        break;
                    }
                }
            }

            //check right
            if (movement.Column != LevelConfig.columns - 1) {
                for (int column = movement.Column + 1; column < LevelConfig.columns; column++) {
                    if (tokens[movement.Row, column] != null
                        && tokens[movement.Row, column].TheSameColor(go) && tokens[movement.Row, column].IsMatch()) {
                        matches.Add(tokens[movement.Row, column]);
                    } else {
                        break;
                    }
                }
            }

            //we want more than three matches
            if (matches.Count < Constants.MinimumMatches)
                matches.Clear();

            return matches.Distinct();
        }

        public IEnumerable<GameObject> GetEntireRow (GameObject go)
        {
            return go.IsBlocked() ? new List<GameObject>() : GetEntireRow(go.GetComponent<Movement>().Row);
        }

        public IEnumerable<GameObject> GetEntireRow (int number)
        {
            List<GameObject> matches = new List<GameObject>();
            for (int column = 0; column < LevelConfig.columns; column++) {
                if (tokens[number, column] != null && !tokens[number, column].Undestroyable()) {
                    matches.Add(tokens[number, column]);
                }
            }
            return matches;
        }

        public IEnumerable<GameObject> GetEntireColumn (GameObject go)
        {
            return go.IsBlocked() ? new List<GameObject>() : GetEntireColumn(go.GetComponent<Movement>().Column);
        }

        public IEnumerable<GameObject> GetEntireColumn (int number)
        {
            List<GameObject> matches = new List<GameObject>();
            for (int row = 0; row < LevelConfig.rows; row++) {
                if (tokens[row, number] != null && !tokens[row, number].Undestroyable()) {
                    matches.Add(tokens[row, number]);
                }
            }
            return matches;
        }

        public IEnumerable<GameObject> GetColumns (IEnumerable<int> numbers)
        {
            List<GameObject> tokens = new List<GameObject>();
            foreach (var number in numbers.Where(number => number >= 0 && number < LevelConfig.columns)) {
                tokens.AddRange(GetEntireColumn(number));
            }

            return tokens.Distinct();
        }

        public IEnumerable<PositionInfo> GetEmptyItems (PositionInfo spawnPosition)
        {
            var result = new List<PositionInfo>();
            var position = new PositionInfo(spawnPosition);

            while (!position.Overextend()) {
                if (this[position] == null) {
                    result.Add(new PositionInfo(position));
                } else if (this[position].HasComponent<Volatile>()) {
                    position.UpdateWithGravity(GetGravity(position));
                    continue;
                } else {
                    break;
                }

                if (BottomPositions.Any(bottomPos => position.Equals(bottomPos))) {
                    break;
                }

                position.UpdateWithGravity(GetGravity(position));
            }

            return result;
        }

        public IEnumerable<GameObject> GetRows (IEnumerable<int> numbers)
        {
            List<GameObject> tokens = new List<GameObject>();
            foreach (var number in numbers.Where(number => number >= 0 && number < LevelConfig.rows)) {
                tokens.AddRange(GetEntireRow(number));
            }

            return tokens.Distinct();
        }

        private bool requireAvoidanceCascade;

        public AlteredTokensInfo Cascade ()
        {
            var info = new AlteredTokensInfo();
            var dontFallDown = true;
            requireAvoidanceCascade = false;
            foreach (var newPosition in BottomPositions.Select(position => new PositionInfo(position))) {
                while (true) {
                    var token = this[newPosition];

                    if (token.HasComponent<Volatile>()) {
                        newPosition.UpdateWithGravity(GetGravity(newPosition).Revert);
                        continue;
                    }

                    var movement = token != null ? token.GetComponent<Movement>() : null;
                    if (movement != null && movement.enabled && movement.MoveForvard()) {
                        dontFallDown = false;
                        info.AddToken(movement.gameObject);
                        info.AddDistance(movement.DistancePassed);
                    }

                    if (SpawnPositions.Any(pos => pos.Equals(newPosition)) || newPosition.Overextend())
                        break;

                    newPosition.UpdateWithGravity(GetGravity(newPosition).Revert);
                }
            }

            requireAvoidanceCascade = dontFallDown;
            return info;
        }

        public AlteredTokensInfo CascadeAvoidance ()
        {
            var info = new AlteredTokensInfo();

            if (!requireAvoidanceCascade) {
                return info;
            }

            var movingTokens = Items
                .Where(obj => obj != null && obj.HasComponent<Movement>())
                .Select(token => token.GetComponent<Movement>())
                .Where(movement => movement.CanMoveAsight())
                .ToList();

            SeparateLowestTokens(movingTokens);

            foreach (var movement in movingTokens) {
                movement.MoveAsight();

                info.AddToken(movement.gameObject);
                info.AddDistance(movement.DistancePassed);
            }

            Movement.SwitchDirection();

            return info;
        }

        private void SeparateLowestTokens (List<Movement> movingTokens)
        {
            var positions = movingTokens.Select(movement => movement.Position).ToList();

            foreach (var positionInfo in positions) {
                var position = new PositionInfo(positionInfo);
                while (BottomPositions.All(pos => !position.Equals(pos)) && !position.Overextend()) {
                    position.UpdateWithGravity(GetGravity(position));
                    if (positions.Contains(position)) {
                        movingTokens.RemoveAll(movement => movement.Position.Equals(positionInfo));
                        break;
                    }
                }
            }
        }

        public bool Stable ()
        {
            return TokensDontFall() && NoHolesOnTop();
        }

        private List<Movement> Movements
        {
            get
            {
                return Items
                .Where(token => token != null && token.HasComponent<Movement>())
                .Select(token => token.GetComponent<Movement>())
                .ToList();
            }
        }  

        private bool TokensDontFall ()
        {
            return Items
                .Where(token => token != null && token.HasComponent<Movement>())
                .Select(token => token.GetComponent<Movement>())
                .All(movement => !movement.CanMoveForward() && !movement.CanMoveLeftOrRight());
        }

        private bool NoHolesOnTop ()
        {
            return SpawnPositions.All(position => this[position] != null);
        }

        public AlteredTokensInfo Shuffle ()
        {
            var tokensInfo = new AlteredTokensInfo();
            Shuffle(tokensInfo);

            tokensForShuffle = null;

            return tokensInfo;
        }

        private IMatchesCorrector corrector;

        private void Shuffle(AlteredTokensInfo tokensInfo)
        {
            corrector = new ShuffleCorrector(TokensForShuffle);

            for (int row = 0; row < rows; ++row) {
                for (int column = 0; column < columns; ++column) {
                    var randomIndex = Random.Range(0, TokensForShuffle.Count);

                    if (!positions.Contains(new PositionInfo(row, column))/*this[row, column] != null*/) {
                        continue;
                    }

                    this[row, column] = TokensForShuffle[randomIndex];
                    corrector.EnsureNoMatches(new PositionInfo {Column = column, Row = row});

                    var newToken = this[row, column];
                    tokensInfo.AddToken(newToken);
                    newToken.GetComponent<Movement>().Assign(row, column);
                }
            }
        }

        private List<GameObject>   tokensForShuffle;
        private List<PositionInfo> positions; 

        private List<GameObject> TokensForShuffle
        {
            get
            {
                if (tokensForShuffle != null) {
                    return tokensForShuffle;
                }

                tokensForShuffle = new List<GameObject>();
                positions = new List<PositionInfo>();
                foreach (var token in Items.Where(token => !token.IsBlocked() && !token.HasComponent<Ingridient>() && !token.HasComponent<Bonus>())) {
                    tokensForShuffle.Add(token);
                    var pos = token.GetComponent<Movement>().Position;
                    positions.Add(pos);
                    this[pos] = null;
                }

                return tokensForShuffle;
            }
        } 

        public IEnumerable<GameObject> Items
        {
            get
            {
                var list = new List<GameObject>();
                for (int row = 0; row < rows; row++) {
                    for (int column = 0; column < columns; ++column) {
                        if (tokens[row, column] != null) {
                            list.Add(tokens[row, column]);
                        }
                    }
                }

                return list;
            }
        } 

        public void Remove (GameObject item)
        {
            var info = item.GetComponent<Movement>().Position;

            if (tokens[info.Row, info.Column] == item) {
                this[info] = null;
            }
        }
    }
}