using System;
using System.Collections.Generic;
using MMK.Core.Aims;
using UnityEngine;

namespace MMK.Match3.Aims
{
    public class IngridientAimInfo
    {
        [Serializable]
        public class IngridientItem
        {
            public GameObject prefab;
            public int score;

            public IngridientItem(GameObject prefab, int score)
            {
                this.prefab = prefab;
                this.score = score;
            }
        }

        private class ObjectsBySpriteComparer : IEqualityComparer<GameObject>
        {
            public bool Equals(GameObject x, GameObject y)
            {
                if (x == null || y == null) {
                    return false;
                }

                var sprite1 = x.GetComponent<Ingridient>()?.Sprite;
                var sprite2 = y.GetComponent<Ingridient>()?.Sprite;

                return sprite1 != null && sprite1.Equals(sprite2);
            }

            public int GetHashCode(GameObject obj)
            {
                return obj.GetComponent<Ingridient>().Sprite.GetHashCode();
            }
        }

        public List<IngridientItem> items = new List<IngridientItem>();
        private readonly Dictionary<GameObject, List<IngridientItem>> sortedItems;

        public IngridientAimInfo()
        {
            sortedItems = new Dictionary<GameObject, List<IngridientItem>>(new ObjectsBySpriteComparer());
        }

        public LevelAim AttachTo(GameObject obj)
        {
            var aggregatorAim = obj.AddComponent<IngridientAim>();
            aggregatorAim.requiredAmount = items.Count;
            aggregatorAim.UnbindEvent();
            aggregatorAim.BindEvent();

            SortItems();

            foreach (var ingridientItem in sortedItems.Values) {
                var aim = obj.AddComponent<IngridientAim>();

                aim.requiredAmount = ingridientItem.Count;
                aim.UnbindEvent();
                aim.BindEvent();

                foreach (var item in ingridientItem) {
                    aim.AddIngridient(item.prefab, item.score);
                }

                aim.InstantiateUIItem(obj);
            }

            return aggregatorAim;
        }

        private void SortItems ()
        {
            foreach (var item in items) {
                if (!sortedItems.ContainsKey(item.prefab)) {
                    sortedItems.Add(item.prefab, new List<IngridientItem>());
                }

                sortedItems[item.prefab].Add(item);
            }
        }
    }
}