﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using MMK.Match3;
using UnityEngine;

namespace MMK.Match3
{
    public class SerializedMatrix<T>
    {
        public T[] objects;

        [HideInInspector] [SerializeField] private int rows;
        [HideInInspector] [SerializeField] private int columns;

        public SerializedMatrix ()
        {

        }

        public SerializedMatrix(int rows, int columns)
        {
            this.rows = rows;
            this.columns = columns;
            objects = new T[rows * columns];
        }

        public SerializedMatrix (List<List<T>> lists)
        {
            if (lists.Count == 0) {
                return;
            }

            rows = lists.Count;
            columns = lists[0].Count;
            objects = new T[rows * columns];
            for (int row = 0; row < rows; row++) {
                for (int column = 0; column < columns; column++) {
                    objects[row * columns + column] = lists[row][column];
                }
            }
        }

        public T this[int row, int column]
        {
            get { return objects[columns * row + column]; }
            set { objects[columns * row + column] = value; }
        }

        public T[,] ToMatrix ()
        {
            var result = new T[rows, columns];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    result[i, j] = objects[columns * i + j];
                }
            }

            return result;
        }

        public List<List<T>> ToDoubleList ()
        {
            var result = new List<List<T>>();
            for (int row = 0; row < rows; row++) {
                result.Add(new List<T>());
                for (int column = 0; column < columns; column++) {
                    result[row].Add(objects[row * columns + column]);
                }
            }

            return result;
        }
    }

    [Serializable]
    public class GameObjectsSerializedMatrix : SerializedMatrix<GameObject>
    {
        public GameObjectsSerializedMatrix(int row, int column)
            : base(row, column)
        {
            
        }

        public GameObjectsSerializedMatrix (List<List<GameObject>> lists)
            : base(lists)
        {

        }
    }

    [Serializable]
    public class GravitySerializedMatrix : SerializedMatrix<GravityVector>
    {
        public GravitySerializedMatrix(int rows, int columns) : base(rows, columns)
        {
        }

        public GravitySerializedMatrix (List<List<GravityVector>> lists)
            : base(lists)
        {

        }
    }

}