﻿namespace MMK.Match3
{
    internal interface IMatchesCorrector
    {
        void EnsureNoMatches(PositionInfo info);
    }
}