﻿using MMK.Core;

namespace MMK.Match3
{
    public interface IMatch3ScoreEntity : IScoreEntity
    {
        /// <summary>
        /// If this is true, score manager will apply multiplier to the score value,
        /// so it will change as cascade lasts
        /// </summary>
        bool DependsOnCascade { get; set; }
    }
}