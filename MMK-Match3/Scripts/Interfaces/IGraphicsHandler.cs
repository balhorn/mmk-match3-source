﻿using MMK.Match3.TokenComponents;

namespace MMK.Match3
{
    /// <summary>
    /// Interface for components that can handle graphics or animation
    /// </summary>
    public interface IGraphicsHandler
    {
        /// <summary>
        /// Property to acces Graphics component.
        /// Important: serializeble field should be created, otherwise it's value won't be saved
        /// </summary>
        Graphics Graphics { get; set; }

        /// <summary>
        /// Property to acces Animation component.
        /// Important: serializeble field should be created, otherwise it's value won't be saved
        /// </summary>
        Animation Animation { get; set; }

        void SetupGraphics(Graphics graphics);
        void SetupAnimation(Animation animation);
    }
}