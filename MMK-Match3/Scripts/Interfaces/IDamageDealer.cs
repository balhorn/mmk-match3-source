﻿using System.Collections.Generic;
using UnityEngine;

namespace MMK.Match3
{
    /// <summary>
    /// Describes entity to deal damage to tokens
    /// </summary>
    public interface IDamageDealer
    {
        int DamageAmount { get; set; }

        void DealDamage(IEnumerable<GameObject> objects);
    }
}

