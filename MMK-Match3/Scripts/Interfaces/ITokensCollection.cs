﻿using System.Collections.Generic;
using UnityEngine;

namespace MMK.Match3
{
    public interface ITokensCollection
    {
        GameObject this[PositionInfo info] { get; set; }
        GameObject this[int row, int column] { get; set; }

        IEnumerable<GameObject> Items { get; }

        List<PositionInfo> SpawnPositions { get; set; }
        List<PositionInfo> BottomPositions { get; set; }
        List<PositionInfo> IngridientSpawners { get; set; }
        List<PositionInfo> DespawnPositions { get; set; }

        GameObject[,] Cells { get; set; }

        void Swap (GameObject first, GameObject second);
        void UndoSwap ();

        GravityVector GetGravity (PositionInfo info);

        IEnumerable<PositionInfo> GetEmptyItems (PositionInfo spawnPostion);

        IEnumerable<GameObject> GetMatches (IEnumerable<GameObject> gos);
        TokensBundle GetMatches (GameObject go);

        IEnumerable<GameObject> GetEntireRow (GameObject go);
        IEnumerable<GameObject> GetEntireRow (int number);

        IEnumerable<GameObject> GetEntireColumn (GameObject go);
        IEnumerable<GameObject> GetEntireColumn (int number);

        IEnumerable<GameObject> GetColumns (IEnumerable<int> numbers);
        IEnumerable<GameObject> GetRows (IEnumerable<int> numbers);

        AlteredTokensInfo CascadeAvoidance ();
        AlteredTokensInfo Cascade ();
        AlteredTokensInfo Shuffle ();

        void Remove (GameObject item);
        bool Stable ();
    }

}

