﻿using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Core.UI;

namespace MMK.Match3
{
    public class Match3ScoreManager : ScoreManager
    {
        public static int Multiplier => Instance.scoreMultiplier;

        private static Match3ScoreManager Instance => instance as Match3ScoreManager;

        private int scoreMultiplier = 1;
        private List<ComplimentaryWords> sortedComplimentaryWords;

        public static void NewCascadeLoop()
        {
            Instance.scoreMultiplier++;
        }

        public static void ResetCascadeLoop()
        {
            Instance.ShowComplimentaryWords();
            Instance.scoreMultiplier = 1;
        }

        protected override void RecordScoreInternal(IScoreEntity scoreEntity)
        {
            var m3ScoreEntity = scoreEntity as IMatch3ScoreEntity;

            if (m3ScoreEntity != null) {
                m3ScoreEntity.Score = m3ScoreEntity.DependsOnCascade
                    ? m3ScoreEntity.Score * Instance.scoreMultiplier
                    : m3ScoreEntity.Score;
            }

            base.RecordScoreInternal(scoreEntity);
        }

        private void ShowComplimentaryWords ()
        {
            var words = sortedComplimentaryWords.LastOrDefault(word => scoreMultiplier >= word.matchesCount);
            if (words != null) {
                words.Show();
            }
        }

        protected override void Start()
        {
            base.Start();
            sortedComplimentaryWords = complimentaryWords.OrderBy(words => words.matchesCount).ToList();
        }
    }
}