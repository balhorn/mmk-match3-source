﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Match3.TokenComponents;
using UnityEngine;
using Color = MMK.Match3.TokenComponents.Color;
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace MMK.Match3
{
    public class BonusManager : MonoBehaviour
    {
        public List<GameObject> bonuses;

        public static BonusManager Instance { get; private set; }

        private System.Random random = new System.Random();
        private ShapesManager shapesManager;
        private readonly MatchShape[] nonBonusShapes = {MatchShape.None, MatchShape.Three, MatchShape.Square};
        private readonly MatchShape[] nonColoredShapes = {MatchShape.Five};
        private Dictionary<MatchShape, string> attachmentNames = new Dictionary<MatchShape, string>();
        private GameObject[] attachments;

        private void Awake ()
        {
            InitInstance();

            shapesManager = GetComponent<ShapesManager>();
            attachments = Resources.LoadAll<GameObject>(Constants.AttachmentsFolder)
                .Where(prefab => prefab.HasComponent<Bonus>())
                .ToArray();
            foreach (var attachment in attachments) {
                var bonusShape = attachment.GetComponent<Bonus>().matchShape;
                foreach (var flag in bonusShape.GetFlags()) {
                    attachmentNames[(MatchShape) flag] = attachment.name;
                }
            }
        }

        private void InitInstance()
        {
            if (Instance != null) {
                Destroy(this);
            }

            Instance = this;
        }

        public void AttachTo (GameObject token, MatchShape bonusShape)
        {
            var name = attachmentNames[bonusShape];
            ElementFactory.Attach(token, name);
        }

        internal GameObject GetBonus (ShapedBundle bundle)
        {
            TokenColor color = bundle.Color;
            MatchShape shape = bundle.Shape;

            return GetBonus(color, shape);
        }

        public GameObject GetBonus (TokenColor color, MatchShape shape)
        {
            return nonBonusShapes.Contains(shape) ? null : LoadOrGetFromCache(color, shape);
        }

        public GameObject GetLineBonus (TokenColor color)
        {
            var offset = UnityEngine.Random.Range(2, 4); //Generating vertical or horizontal match shape
            var shape = (MatchShape) (1 << offset);

            return LoadOrGetFromCache(color, shape);
        }

        public GameObject GetBombBonus (TokenColor color)
        {
            return LoadOrGetFromCache(color, MatchShape.T);
        }

        private GameObject LoadOrGetFromCache (TokenColor color, MatchShape shape)
        {
            try {
                GameObject bonusPrefab = bonuses.First(bonus => bonus.GetComponent<Bonus>().matchShape.HasFlag(shape)
                                                                && (nonColoredShapes.Contains(shape) || bonus.GetColor() == color));

                return bonusPrefab;
            } catch (InvalidOperationException) {
                var token = Instantiate(shapesManager.tokensPrefabs.First(prefab => prefab.GetColor() == color));
                var attachmentName = attachmentNames[shape];

                var result = ElementFactory.Attach(token, attachmentName);
                result.SetActive(false);
                bonuses.Add(result);

                return result;
            }
        }

        internal GameObject GetWinBonus(TokenColor color)
        {
            GameObject bonusPrefab;
            var bonusNumber = random.NextDouble() <= 0.1 ? 1 : 2;
            var requiredBonuses = bonuses
                .Where (bonus => bonus.GetComponent<Bonus> ().explosionOrder == bonusNumber)
                .ToList();

            if (requiredBonuses.Count != 0) {
                var index = UnityEngine.Random.Range(0, requiredBonuses.Count);
                bonusPrefab = requiredBonuses[index];
            } else {
                var token = Instantiate(shapesManager.tokensPrefabs.First(prefab => prefab.GetColor() == color));
                var requiredPrefabs = attachments
                    .Where(attachment => attachment.GetComponent<Bonus>().explosionOrder == bonusNumber)
                    .ToList();
                var index = UnityEngine.Random.Range(0, requiredPrefabs.Count);
                var attachmentName = requiredPrefabs[index].name;

                bonusPrefab  = ElementFactory.Attach(token, attachmentName);
                bonusPrefab.SetActive(false);
                bonuses.Add(bonusPrefab);
            }

            bonusPrefab.GetComponent<Color> ().Value = color;

            return bonusPrefab;
        }
    }
}