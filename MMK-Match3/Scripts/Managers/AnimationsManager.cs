﻿using System.Collections;
using System.Collections.Generic;
using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    public class AnimationsManager : MonoBehaviour
    {
        public static AnimationsManager Instance { get; private set; }

        public float emergencyFlushDelay = 1.5f;
        private HashSet<int> animations;

        private ShapesManager shapesManager;
        private IEnumerator flushCoroutine;

        private void Awake ()
        {
            InitInstance();
        }

        private void InitInstance ()
        {
            if (Instance != null) {
                Destroy(this);
                return;
            }

            Instance = this;
        }

        private void Start ()
        {
            animations = new HashSet<int>();
            shapesManager = ShapesManager.Instance;
        }

        private IEnumerator EmergencyFlush ()
        {
            yield return new WaitForSeconds(emergencyFlushDelay);

            animations.Clear();
            StartCoroutine(CollapseTokens());
        }

        public int StartAnimation (int hash)
        {
            animations.Add(hash);


            shapesManager.StopCheckForPotentialMatches();

            return hash;
        }

        private void StartFlushCoroutine ()
        {
            if (flushCoroutine != null) {
                StopCoroutine(flushCoroutine);
            }

            flushCoroutine = EmergencyFlush();
            StartCoroutine(flushCoroutine);
        }

        public void StopAnimation (int hash)
        {
            animations.Remove(hash);

            if (GameController.state == GameState.WinBonus) {
                StartCoroutine(CollapseTokens());
            } else if (animations.Count == 0) {
                StartCoroutine(CollapseTokens());
            } else {
                StartFlushCoroutine();
            }
        }

        private IEnumerator collapsingCoroutine;

        private IEnumerator CollapseTokens ()
        {
            if (collapsingCoroutine != null) {
                yield break;
            }

            collapsingCoroutine = shapesManager.CollapseTokens();
            yield return StartCoroutine(collapsingCoroutine);

            collapsingCoroutine = null;
        }
    }
}
