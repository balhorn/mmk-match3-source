﻿using UnityEngine;

namespace MMK.Match3
{
    public class Match3SoundManager : SoundManager
    {
        private static Match3SoundManager Instance => instance as Match3SoundManager;

        public static void Match ()
        {
            if (Instance == null) {
                Debug.Log("Sound manager instance does not exist");
                return;
            }

            Instance.Clip = Match3ScoreManager.Multiplier <= 2
                ? Match3Player.Settings.matchSound
                : Match3Player.Settings.furtherMatchSound;
        }
    }
}