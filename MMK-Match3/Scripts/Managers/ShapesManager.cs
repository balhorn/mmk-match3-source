﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Match3;
using MMK.Match3.TokenComponents;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MMK.Match3
{
    public class ShapesManager : MonoBehaviour, IDamageDealer, IEventListener
    {
        public ITokensCollection Tokens => ElementComponent.Tokens;
        public static ShapesManager Instance { get; private set; }

        private GameObject hitGo = null;

        public GameObject[] tokensPrefabs;

        public SortedList<int, GameObject> Ingridients { get; set; }

        private IEnumerator checkPotentialMatchesCoroutine;
        private IEnumerator animatePotentialMatchesCoroutine;

        private IEnumerable<GameObject> potentialMatches;

        private readonly MatchHelper matchHelper = new MatchHelper();

        private AnimationsManager animationsManager;
        private ScoreManager      scoreManager;
        private WinBonus          winBonus;
        private BonusManager      bonusManager;
        private Level             level;

        private Vector2 FieldOrigin => level.Builder.FieldOrigin;

        public AnimationsManager AnimationsManager => animationsManager;
        public ScoreManager ScoreManager => scoreManager;
        public BonusManager BonusManager => bonusManager;

        public int DamageAmount { get; set; }
        public int ScoreMultiplier { get; set; }

        private Match3Constraint Constraint => Core.Constraint.Current as Match3Constraint;

        public static event Core.Delegates.VoidDelegate OnCollapsingFinish;
        public static event Core.Delegates.VoidDelegate OnTurnStarted;

        private void Awake()
        {
            InitComponents ();
            BindEvent();
            InitInstance();
        }

        private void OnDestroy ()
        {
            UnbindEvent();
        }

        private void Start ()
        {
            GameController.state = GameState.Idle;
            Time.timeScale = 1;
            InitLevel();
            InitializeTokens();
        }

        public void BindEvent()
        {
            ShapedBundle.OnShapedMatch += OnShapedMatch;
        }

        public void UnbindEvent()
        {
            ShapedBundle.OnShapedMatch -= OnShapedMatch;
        }

        private void InitComponents()
        {
            winBonus = GetComponent<WinBonus>();
            bonusManager = GetComponent<BonusManager> ();
            animationsManager = GetComponent<AnimationsManager> ();
            scoreManager = GetComponent<ScoreManager> ();

            level = Level.Current;
            DamageAmount = 1;
            //ScoreMultiplier = 1;
        }

        private void InitInstance ()
        {
            if (Instance != null) {
                Destroy(this);
                return;
            }

            Instance = this;
        }

        public void InitializeTokens ()
        {
            scoreManager.ShowScore();

            matchHelper.Fetch();
        }

        private void InitLevel ()
        {
            level.Builder.BuildPremadeLevel();
        }

        // Update is called once per frame
        private void Update ()
        {
            switch (GameController.state) {
                case GameState.Idle:
                    //user has clicked or touched
                    if (Input.GetMouseButtonDown(0)) {
                        //get the hit position
                        var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                        if (hit.collider != null) {
                            GameObject go = hit.collider.gameObject;
                            if(!go.IsBlocked()){
                                hitGo = go;
                                GameController.state = GameState.SelectionStarted;
                            }
                        }
                    }
                    break;
                case GameState.SelectionStarted:
                    //user dragged
                    if (Input.GetMouseButton(0)) {

                        var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                        if(hit.collider != null){
                            GameObject go = hit.collider.gameObject;
                            if(!go.IsBlocked()){
                                //we have a hit
                                if (hitGo != go) {

                                    //user did a hit, no need to show him hints 
                                    StopCheckForPotentialMatches();

                                    //if the two Tokens are diagonally aligned (different row and column), just return
                                    if (!Utilites.AreVerticalOrHorizontalNeighbors(hitGo.GetComponent<Movement>(),
                                        hit.collider.gameObject.GetComponent<Movement>())) {
                                        GameController.state = GameState.Idle;
                                    } else {
                                        GameController.state = GameState.Animating;
                                        FixSortingLayer(hitGo, hit.collider.gameObject);
                                        StartCoroutine(FindMatchesAndCollapse(hit));
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Modifies sorting layers for better appearance when dragging/animating
        /// </summary>
        /// <param name="hitGo"></param>
        /// <param name="hitGo2"></param>
        private void FixSortingLayer (GameObject hitGo, GameObject hitGo2)
        {
            SpriteRenderer sp1 = hitGo.GetComponent<SpriteRenderer>();
            SpriteRenderer sp2 = hitGo2.GetComponent<SpriteRenderer>();
            if (sp1.sortingOrder <= sp2.sortingOrder) {
                sp1.sortingOrder = 2;
                sp2.sortingOrder = 1;
            }   
        }

        private IEnumerator FindMatchesAndCollapse (RaycastHit2D hit2)
        {
            //get the second item that was part of the swipe
            var hitGo2 = hit2.collider.gameObject;
            Tokens.Swap(hitGo, hitGo2);

            //move the swapped ones
            hitGo.transform.positionTo(Constants.AnimationDuration, hitGo2.transform.position);
            hitGo2.transform.positionTo(Constants.AnimationDuration, hitGo.transform.position);
            yield return new WaitForSeconds(Constants.AnimationDuration);

            UpdateMovedTime(new [] {hitGo, hitGo2});

            var matchSuccessful = hitGo.Match(hitGo2) || hitGo2.Match(hitGo);
            if (matchSuccessful && OnTurnStarted != null) {
                OnTurnStarted();

                hitGo.SendMessage("OnMovementFinished", SendMessageOptions.DontRequireReceiver);
                hitGo2.SendMessage("OnMovementFinished", SendMessageOptions.DontRequireReceiver);
            } else if (!matchSuccessful) {
                hitGo.transform.positionTo(Constants.AnimationDuration, hitGo2.transform.position);
                hitGo2.transform.positionTo(Constants.AnimationDuration, hitGo.transform.position);
                yield return new WaitForSeconds(Constants.AnimationDuration);

                UpdateMovedTime(new[] { hitGo, hitGo2 });

                Tokens.UndoSwap();
                GameController.state = GameState.Idle;
            }

            StartCheckForPotentialMatches();
        }

        public GameObject CreateBonus (GameObject bonusPrefab, PositionInfo position)
        {
            var bonus = Instantiate (bonusPrefab, FieldOrigin + 
                                                  new Vector2(position.Column * Match3Player.Settings.tokenSize.x,
                                                      position.Row * Match3Player.Settings.tokenSize.y),
                Quaternion.identity) as GameObject;
            bonus.SetActive(true);
            var bonusMovement = bonus.GetComponent<Movement> ();

            Tokens [position] = bonus;
            bonusMovement.Assign (position.Row, position.Column);

            return bonus;
        }

        public void DealDamage(IEnumerable<GameObject> objects)
        {
            foreach (var o in objects) {
                o.DealDamage(this);
            }
        }

        private readonly Dictionary<PositionInfo, GameObject> bonusesToSpawn =
            new Dictionary<PositionInfo, GameObject>(new PositionComparer());

        private void OnCollapsingStarted ()
        {
            SpawnBonuses();
        }

        private void OnShapedMatch (ShapedBundle bundle)
        {
            if (bundle.CentralToken == null) {
                return;
            }

            var info = bundle.CentralToken.GetComponent<Movement>().Position;
            var bonusPrefab = bonusManager.GetBonus(bundle);
            if (bonusPrefab != null && !bonusesToSpawn.ContainsKey(info)) {
                bonusesToSpawn.Add(info, bonusPrefab);
            }
        }

        private void SpawnBonuses()
        {
            foreach (var pair in bonusesToSpawn.Where(pair => pair.Value != null)) {
                CreateBonus(pair.Value, pair.Key);
            }

            bonusesToSpawn.Clear();
        }

        private AlteredTokensInfo collapsedTokensInfo = new AlteredTokensInfo();
        private AlteredTokensInfo newTokensInfo       = new AlteredTokensInfo();
        private AlteredTokensInfo avoidanceTokensInfo = new AlteredTokensInfo();
    
        public IEnumerator CollapseTokens ()
        {
            yield return new WaitForEndOfFrame();

            if (GameController.state == GameState.Idle) {
                GameController.state = GameState.Animating;
            }

            gameObject.SendMessage("OnCollapsingStarted");

            StopCheckForPotentialMatches();
		
            while(!Tokens.Stable()) {
                collapsedTokensInfo = Tokens.Cascade();
                newTokensInfo = CreateNewTokens();

                var time = Mathf.Max(MoveAndAnimate(collapsedTokensInfo),
                    MoveAndAnimate(newTokensInfo));
            
                yield return new WaitForSeconds(time);
                var combinedInfo = collapsedTokensInfo + newTokensInfo;
                UpdateMovedTime(combinedInfo);

                if (!DestroyMatchedTokens(combinedInfo)) {
                    Match3ScoreManager.NewCascadeLoop();
                    yield break;
                }

                avoidanceTokensInfo += Tokens.CascadeAvoidance();

                yield return new WaitForSeconds(MoveAndAnimate(avoidanceTokensInfo));

                UpdateMovedTime(avoidanceTokensInfo);
            }

            var stabilized = DestroyMatchedTokens(avoidanceTokensInfo);

            if (!stabilized) {
                Match3ScoreManager.NewCascadeLoop();
                yield break;
            }

            collapsedTokensInfo.Clear();
            newTokensInfo.Clear();
            avoidanceTokensInfo.Clear();

            Match3ScoreManager.ResetCascadeLoop();
            gameObject.SendMessage("OnCollapsingFinished");

            if (GameController.state != GameState.WinBonus && GameController.state != GameState.Pause) {
                GameController.state = GameState.Idle;
                Constraint.CheckGameOver();
            }

            if (OnCollapsingFinish != null && GameController.state != GameState.Pause) {
                StartCheckForPotentialMatches();
                OnCollapsingFinish();
            }
        }

        private bool DestroyMatchedTokens (IEnumerable<GameObject> collapsedTokens)
        {
            var bundle = new TokensBundle(Tokens.GetMatches(collapsedTokens));
            var stabilized = bundle.Count == 0;
            bundle.DetermineShapes();
            DealDamage(bundle);

            return stabilized;
        }

        private void UpdateMovedTime(IEnumerable<GameObject> tokens)
        {
            foreach (var item in tokens.Where(token => token != null)) {
                item.GetComponent<Movement> ().LastMoved = Time.realtimeSinceStartup;
            }
        }

        public void Shuffle ()
        {
            SoundManager.Shuffle();
            StartCoroutine(ShuffleCoroutine());
        }

        public IEnumerator ShuffleCoroutine ()
        {
            Debug.Log("Started shuffling");
            StopCheckForPotentialMatches();

            float maxDistance = PreShuffleAnimation();

            yield return new WaitForSeconds(Constants.MoveAnimationMinDuration * maxDistance);

            var shuffleInfo = Tokens.Shuffle();

            MoveAndAnimate(shuffleInfo, 0.75f / Constants.MoveAnimationMinDuration);

            yield return new WaitForSeconds(0.75f / Constants.MoveAnimationMinDuration);

            UpdateMovedTime(shuffleInfo);

            DealDamage(new TokensBundle(Tokens.GetMatches(shuffleInfo)));

            StartCheckForPotentialMatches();
        }

        private float PreShuffleAnimation()
        {
            var offsetX = LevelConfig.rows / 2 * Match3Player.Settings.tokenSize.y;
            var offsetY = LevelConfig.columns / 2 * Match3Player.Settings.tokenSize.x;
            var center = FieldOrigin + new Vector2(offsetX, offsetY);

            var maxDistance = 0f;
            foreach (var item in Tokens.Items.Where(token => !token.IsBlocked() && !token.HasComponent<Ingridient>() && !token.HasComponent<Bonus>())) {
                var tokenTransform = item.transform;
                var distance = Vector2.Distance(tokenTransform.position, center);
                if (distance > maxDistance) {
                    maxDistance = distance;
                }

                tokenTransform.positionTo(0.75f, center);
            }

            return maxDistance;
        }

        private AlteredTokensInfo CreateNewTokens()
        {
            var tokensInfo = new AlteredTokensInfo();
            foreach (var spawnPosition in Tokens.SpawnPositions) {
                var emptyItems = Tokens.GetEmptyItems(spawnPosition).ToList();
                var count = emptyItems.Count;
                var offset = count;
                foreach (var positionInfo in emptyItems) {
                    var positionBelowSpawn = new PositionInfo(spawnPosition);
                    var direction = Tokens.GetGravity(spawnPosition).Revert.Value.normalized;
                    var position = positionBelowSpawn.Coordinates(FieldOrigin) +
                                   direction * Match3Player.Settings.tokenSize.x * offset--;

                    var newToken = FetchIngridient(spawnPosition, position) ?? GetRandomToken(position);

                    newToken.SetActive(true);
                    newToken.GetComponent<Movement>().Assign(positionInfo.Row, positionInfo.Column);

                    Tokens[positionInfo] = newToken;

                    tokensInfo.AddToken(newToken);
                }
            }

            return tokensInfo;
        }

        private GameObject GetRandomToken (Vector2 position)
        {
            var go = tokensPrefabs[Random.Range(0, tokensPrefabs.Length)];
            var newToken = Instantiate(go, position, Quaternion.identity)
                as GameObject;
            return newToken;
        }

        private GameObject FetchIngridient (PositionInfo position, Vector2 coordinates)
        {
            if (Ingridients == null || Ingridients.Count == 0 || Ingridients.Keys[0] > ScoreManager.Score
                || !Tokens.IngridientSpawners.Contains(position, new PositionComparer())) {
                return null;
            }

            var ingridient = Ingridients.Values[0];
            Ingridients.RemoveAt(0);

            return Instantiate(ingridient, coordinates, Quaternion.identity) as GameObject;
        }


        private void MoveAndAnimate (IEnumerable<GameObject> movedGameObjects, float distance)
        {
            foreach (var item in movedGameObjects.Where(item => item != null)) {
                var position = item.GetComponent<Movement>().Position;
                item.transform.positionTo(Constants.MoveAnimationMinDuration * distance,
                    position.Coordinates(FieldOrigin));

                item.GetComponent<Movement> ().LastMoved = Time.realtimeSinceStartup;
            }
        }

        private float MoveAndAnimate (IEnumerable<GameObject> movedGameObjects)
        {
            return movedGameObjects.Where(item => item != null)
                .Select(item => item.GetComponent<Movement>())
                .Where(movement => movement != null)
                .Select(movement => movement.Animate())
                .Concat(new[] {0f})
                .Max();
        }

        private float MoveAndAnimate (GameObject obj, Vector2 destination)
        {
            var movement = obj?.GetComponent<Movement>();
            if (movement == null) {
                return 0f;
            }

            return movement.Animate(destination);
        }

        public IEnumerator WinBonusCoroutine()
        {
            StopCheckForPotentialMatches();
            GameController.state = GameState.WinBonus;

            var winWordVisible = Constraint.popUpManager.ShowWinWord();
            yield return new WaitForSeconds(winWordVisible);
            yield return StartCoroutine(winBonus.RunningCoroutine());

            //GameController.state = GameState.Pause;
            Constraint.WinBonusEnded = true;
            Constraint.CheckGameOver ();
        }

        /// <summary>
        /// Starts the coroutines, keeping a reference to stop later
        /// </summary>
        public void StartCheckForPotentialMatches ()
        {
            StopCheckForPotentialMatches();
            //get a reference to stop it later
            checkPotentialMatchesCoroutine = CheckPotentialMatches();
            StartCoroutine(checkPotentialMatchesCoroutine);
            Debug.Log("Started checking corourine");
        }

        /// <summary>
        /// Stops the coroutines
        /// </summary>
        public void StopCheckForPotentialMatches ()
        {
            if (animatePotentialMatchesCoroutine != null) {
                StopCoroutine(animatePotentialMatchesCoroutine);
            }
            if (checkPotentialMatchesCoroutine != null) {
                Debug.Log("Stopping checking coroutine");
                StopCoroutine(checkPotentialMatchesCoroutine);
            }
            ResetOpacityOnPotentialMatches();
        }

        /// <summary>
        /// Resets the opacity on potential matches (probably user dragged something?)
        /// </summary>
        private void ResetOpacityOnPotentialMatches ()
        {
            if (potentialMatches != null)
                foreach (var item in potentialMatches) {
                    if (item == null || !item.HasComponent<SpriteRenderer>()) {
                        break;
                    }

                    var c = item.GetComponent<SpriteRenderer>().color;
                    c.a = 1.0f;
                    item.GetComponent<SpriteRenderer>().color = c;
                }
        }

        /// <summary>
        /// Finds potential matches
        /// </summary>
        /// <returns></returns>
        private IEnumerator CheckPotentialMatches ()
        {
            yield return new WaitForSeconds(Constants.WaitBeforePotentialMatchesCheck);
            potentialMatches = matchHelper.Fetch();
            Debug.Log($"Fetching finished. Found: {potentialMatches}");
            if (potentialMatches != null) {
                Debug.Log($"Found potential match for {potentialMatches.Count()} tokens");
                while (true) {
                    animatePotentialMatchesCoroutine = matchHelper.Animate(potentialMatches);
                    StartCoroutine(animatePotentialMatchesCoroutine);
                    yield return new WaitForSeconds(Constants.WaitBeforePotentialMatchesCheck);
                    Debug.Log("New animating loop");
                }
            } else {
                Debug.Log("Didn't found matches. Starting shuffling");
            }

            StartCoroutine(ShuffleCoroutine());
        }
    }
}