﻿using System;
using UnityEngine;
using System.Collections;
using MMK.Core;
using MMK.Match3;
using MMK.Match3.TokenComponents;

[AddComponentMenu("Match3/Match/BonusMatch")]
[ElementEditorMenu("Match/Bonus match")]
public class BonusMatch : TokensMatch
{
    private Bonus bonus;
    public bool matchableWithTokens;

    protected override void Start()
    {
        base.Start();

        var tokenMatch = GetComponent<TokensMatch>();
        if (!(tokenMatch is BonusMatch)) {
            Destroy(tokenMatch);
        }

        bonus = GetComponent<Bonus>();
    }

    public override bool DoMatch(GameObject target)
    {
        //TODO Test
        if (target == null) {
            return base.DoMatch(null);
        }

        if (!target.IsMatch()) {
            return false;
        }

        if (!target.HasComponent<BonusMatch>() && !matchableWithTokens) {
            return base.DoMatch(target);
        }

        gameObject.SendMessage("OnMatch", SendMessageOptions.DontRequireReceiver);

        return RunBonus(target);
    }

    /// <summary>
    /// Method were written for the purpose of substitution in unit tests
    /// </summary>
    /// <returns>is match completed</returns>
    protected virtual bool RunBonus (GameObject target)
    {
        var matchShape = !target.HasComponent<Bonus>() && matchableWithTokens
                    ? MatchShape.None
                    : target.GetComponent<Bonus>().matchShape;

        try {
            bonus.RunAppropriateAction(matchShape, target);
        } catch (InvalidOperationException) {
            bonus.IsRunning = false;
            return false;
        } catch (Exception e) {
            Debug.Log("Error during bonus running");
            Debug.Log(e);
            bonus.IsRunning = false;
            return false;
        }

        return true;
    }
}
