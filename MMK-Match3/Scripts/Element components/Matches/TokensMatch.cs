﻿using System;
using UnityEngine;

using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Match3;
using MMK.Match3.TokenComponents;

[AddComponentMenu("Match3/Match/TokenMatch")]
[ElementEditorMenu("Match/Tokens match")]
public class TokensMatch : Match
{
    private List<GameObject> matchedTokens = new List<GameObject>();

    private IEnumerable<GameObject> MatchedTokens => matchedTokens.Distinct();

    public override bool DoMatch(GameObject target)
    {
        var bonusMatch = target?.GetComponent<BonusMatch>();
        if (bonusMatch != null && bonusMatch.matchableWithTokens) {
            return bonusMatch.DoMatch(gameObject);
        }

        gameObject.SendMessage("OnMatch", SendMessageOptions.DontRequireReceiver);

        matchedTokens = Tokens.GetMatches(target).ToList();
        matchedTokens.AddRange(Tokens.GetMatches(gameObject));

        return MatchedTokens.Any() && CompleteMatch();
    }

    /// <summary>
    /// Method were written for the purpose of substitution in unit tests
    /// </summary>
    /// <returns>is match completed</returns>
    protected virtual bool CompleteMatch ()
    {
        new TokensBundle(matchedTokens).DetermineShapes();
        DealDamage(matchedTokens);

        return true;
    }

    private bool isManualMatch;

    private TokenColor color;
    private GameObject centralToken;
    private List<GameObject> forvardTokens;
    private List<GameObject> leftTokens;
    private List<GameObject> rightTokens;

    private PositionInfo position;
    private GravityVector direction;
    private GravityVector rightDirection;
    private GravityVector leftDirection;

    private bool DetermineMatch(GameObject target)
    {
        color = gameObject.GetColor();

        var position = gameObject.GetComponent<Movement>().Position;
        if (target != null) {
            var targetPosition = target.GetComponent<Movement>().Position;
            direction = new GravityVector(position.column - targetPosition.column, position.row - targetPosition.row);
            isManualMatch = true;
        } else {
            direction = Tokens.GetGravity(position);
        }
       
        rightDirection = direction.Right.Right;
        leftDirection = rightDirection.Revert;

        forvardTokens = CollectTokens(position, direction);
        forvardTokens.Add(gameObject);

        leftTokens = CollectTokens(position, leftDirection);
        rightTokens = CollectTokens(position, rightDirection);

        var shape = isManualMatch ? CalculateManualShape() : CalculateCascadeShape();

        if (shape == MatchShape.None) {
            return false;
        }

        CorrectMatch(shape);
        PerfomMatch(shape);

        return true;
    }

    private MatchShape CalculateManualShape()
    {
        var perpLine = leftTokens.Count + rightTokens.Count + 1;
        var forvardLine = forvardTokens.Count;
        if (perpLine == 5) {
            return MatchShape.Five;
        }

        if (leftTokens.Count >= 1 && rightTokens.Count >= 1 && forvardLine == 3) {
            return MatchShape.T;
        }

        if ((leftTokens.Count == 2 || rightTokens.Count == 2) && forvardLine == 3) {
            return MatchShape.Corner;
        }

        if (perpLine == 4) {
            return direction == GravityVector.DirectionUp || direction == GravityVector.DirectionDown
                ? MatchShape.HorizontalFour
                : MatchShape.VerticalFour;
        }

        var found = false;
        if (forvardLine == 2) {
            var diagPosition = position + direction.Left;
            if (leftTokens.Count >= 1) {
                found = Tokens[diagPosition].GetColor() == color && Tokens[diagPosition].IsMatch();
            }

            if (found) {
                leftTokens.Add(Tokens[diagPosition]);
                return MatchShape.Square;
            }

            if (rightTokens.Count >= 1) {
                diagPosition = position + direction.Right;
                found = Tokens[diagPosition].GetColor() == color && Tokens[diagPosition].IsMatch();
            }

            if (found) {
                rightTokens.Add(Tokens[diagPosition]);
                return MatchShape.Square;
            }
        }

        if (forvardLine == 3 || perpLine == 3) {
            return MatchShape.Three;
        }

        return MatchShape.None;
    }

    private MatchShape CalculateCascadeShape()
    {
        
        return (MatchShape) 0;
    }

    private void CorrectMatch (MatchShape shape)
    {
        switch (shape) {
            case MatchShape.Three:
                if (forvardTokens.Count == 3) {
                    leftTokens.Clear();
                    rightTokens.Clear();
                } else if (leftTokens.Count + rightTokens.Count + 1 == 3) {
                    forvardTokens.RemoveAll(obj => obj != gameObject);
                } 
                break;
            case MatchShape.HorizontalFour:
                if (isManualMatch) {
                    CorrectCentralToken(MatchShape.HorizontalFour);
                }
                goto case MatchShape.VerticalFour;
            case MatchShape.VerticalFour:
                forvardTokens.RemoveAll(obj => obj != gameObject);
                break;
            case MatchShape.Five:
                forvardTokens.RemoveAll(obj => obj != gameObject);
                break;
        }
    }

    private void CorrectCentralToken(MatchShape matchShape)
    {
        switch (matchShape) {
            case MatchShape.Five:
                break;
            case MatchShape.HorizontalFour:
                break;
            case MatchShape.VerticalFour:
                break;
                
        }
    }

    private void PerfomMatch(MatchShape shape)
    {
        new ShapedBundle(color, shape, gameObject);

        forvardTokens.AddRange(leftTokens);
        forvardTokens.AddRange(rightTokens);

        DealDamage(forvardTokens);
    }

    private List<GameObject> CollectTokens(PositionInfo start, GravityVector direction)
    {
        var result = new List<GameObject>();
        var newPosition = start + direction;
        var add = Tokens[newPosition] != null && Tokens[newPosition].GetColor() == color && Tokens[newPosition].IsMatch();
        while (add) {
            result.Add(Tokens[newPosition]);

            newPosition = newPosition + direction;
            add = Tokens[newPosition] != null && Tokens[newPosition].GetColor() == color && Tokens[newPosition].IsMatch();
        }

        return result;
    }
}
