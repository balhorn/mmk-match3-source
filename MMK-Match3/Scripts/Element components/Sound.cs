﻿using UnityEngine;

using System;
using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Match3;

[Serializable]
public class Clips
{
    public TokenEvent tokenEvent;
    public AudioClip clip;
}

[Serializable]
public class SuperClips
{
    public string name;
    public AudioClip clip;
}

namespace MMK.Match3.TokenComponents
{
    public class Sound : ElementComponent
    {
        public List<Clips> clips;
        //[Tooltip("Sound clips for super bonuses triggers")]
        //public List<SuperClips> superClips; 
         
        private AudioSource source;
        private Dictionary<TokenEvent, AudioClip> eventsToClips = new Dictionary<TokenEvent, AudioClip>();
        private Dictionary<string, AudioClip> namesToClips = new Dictionary<string, AudioClip>(); 

        protected virtual void Awake ()
        {
            InitSource();
            InitDictionary();
        }

        private void InitDictionary()
        {
            //TODO Temporary stub
            if (clips == null) {
                return;
            }

            foreach (var clip in clips) {
                eventsToClips[clip.tokenEvent] = clip.clip;
            }

            //foreach (var superClip in superClips) {
            //    namesToClips[superClip.name] = superClip.clip;
            //}
        }

        private void InitSource ()
        {
            var audioSource = GetComponent<AudioSource>();
            // ReSharper disable once ConvertConditionalTernaryToNullCoalescing
            source = audioSource != null ? audioSource : gameObject.AddComponent<AudioSource>();
            source.playOnAwake = false;
        }

        

        protected virtual void Play ()
        {
            try {
                var clipToPlay = eventsToClips[CurrentEvent];
                if (clipToPlay == null) {
                    return;
                }

                Play(clipToPlay);
            } catch (KeyNotFoundException) {
            }
        }

        protected virtual void Play (AudioClip clip)
        {
            if (!SoundManager.SoundOn) {
                return;
            }

            source.clip = clip;
            source.Play();
        }

        
        #region EventHandles

        private TokenEvent currentEvent;
        protected TokenEvent CurrentEvent
        {
            get { return currentEvent; }
            set
            {
                currentEvent = value;
                Play();
            }
        }

        protected virtual void OnMovementFinished ()
        {
            Play(Match3Player.Settings.tokenLandingSound);
        }

        protected virtual void OnTokenDestroyed ()
        {
            CurrentEvent = TokenEvent.OnTokenDestroyed;
        }

        protected virtual void OnDurabilityLost ()
        {
            CurrentEvent = TokenEvent.OnDurabilityLost;
        }

        protected virtual void OnDurabilityBroken ()
        {
            CurrentEvent = TokenEvent.OnDurabilityBroken;
        }

        protected virtual void OnActivated ()
        {
            CurrentEvent = TokenEvent.OnActivate;
        }

        protected virtual void OnSpawn ()
        {
            CurrentEvent = TokenEvent.OnSpawn;
        }

        protected virtual void SuperBonus (object name)
        {
            try {
                var clip = namesToClips[(string) name];
                if (clip != null) {
                    Play(clip);
                }
            } catch (KeyNotFoundException) {
            }
        }
        #endregion

        #region Obsolete
        private IEnumerable<TokenEvent> SeparateValues (TokenEvent triggerEvent)
        {
            return Enum.GetValues(typeof(TokenEvent))
                .Cast<TokenEvent>()
                .Where(value => (triggerEvent & value) == value);
        }

        public virtual void Add (TokenEvent triggerEvent, AudioClip clip)
        {
            foreach (var separateValue in SeparateValues(triggerEvent)) {
                eventsToClips.Add(separateValue, clip);
            }
        }
        #endregion
    }
}
