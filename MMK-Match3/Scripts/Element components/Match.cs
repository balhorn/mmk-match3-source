﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MMK.Match3;

namespace MMK.Match3.TokenComponents
{
    public abstract class Match : ElementComponent, IDamageDealer
    {
        public int DamageAmount { get; set; }

        public int ScoreMultiplier { get; set; }

        public void DealDamage(IEnumerable<GameObject> objects)
        {
            foreach (GameObject o in objects) {
                o.DealDamage(this);
            }
        }

        protected virtual void SetupDamage ()
        {
            DamageAmount = 1;
            //ScoreMultiplier = 1;
        }

        protected virtual void Start ()
        {
            SetupDamage();
        }

        public abstract bool DoMatch (GameObject target);
    }
}

