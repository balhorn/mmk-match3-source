﻿using UnityEngine;
using System.Collections;
using MMK.Match3;

namespace MMK.Match3.TokenComponents
{
    public abstract class Active : ElementComponent, IGraphicsHandler
    {
        [SerializeField] [HideInInspector] private Graphics graphics;
        [SerializeField] [HideInInspector] private new Animation animation;

        public bool IsRunning { get; set; }

        public Graphics Graphics
        {
            get { return graphics ?? (graphics = GetComponent<Graphics>()); }
            set { graphics = value; }
        }

        public Animation Animation
        {
            get { return animation; }
            set { animation = value; }
        }

        protected abstract void Start();

        public virtual void Activate ()
        {
            gameObject.SendMessage("OnActivated", SendMessageOptions.DontRequireReceiver);
        }

        public abstract void SetupGraphics(Graphics graphics);
        public abstract void SetupAnimation(Animation animation);
    }
}
