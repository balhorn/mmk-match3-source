﻿using System;
using UnityEngine;
using System.IO;
using MMK.Core;
using MMK.Match3;

namespace MMK.Match3.TokenComponents
{
    [ExecuteInEditMode]
    [ElementEditorMenu("", true)]
    public class Graphics : ElementComponent, IEquatable<Graphics>
    {
        public Sprite            sprite;
        public Sprite            editorSprite;
        public string            sortingLayerName = "Tokens";
        [HideInInspector]
        public SpriteLoadingMode packing = SpriteLoadingMode.OnThisObject;

        protected SpriteRenderer spriteRenderer;

        [HideInInspector] public GameObject landingObject;
        [HideInInspector] public GameObject baseObject;
        [HideInInspector] public string path;

        public Sprite EditorSprite
        {
            get
            {
                if (editorSprite == null) {
                    return Sprite;
                }

                return editorSprite;
            }
        }

        public Sprite Sprite
        {
            get { return sprite; }
            set
            {
                sprite = value;
                SetupSprite();
            }
        }

        protected virtual void Start()
        {
            
        }

        protected virtual void SetupBaseObject ()
        {
            if (baseObject == null) {
                baseObject = gameObject;
            }
        }

        protected virtual void SetupLandingObject ()
        {
            if (landingObject == null || landingObject == gameObject) {
                landingObject = gameObject;
            } else {
                landingObject.transform.parent = baseObject.transform;
                landingObject.transform.localPosition = Vector3.zero;
            }
        }

        private void OnRenderObject ()
        {
            if (!Application.isPlaying) {
                Awake();
            }
        }

        protected void Awake ()
        {
            if (!Application.isPlaying) {
                ResolveExternalPacking();
            }

            SetupBaseObject();
            SetupLandingObject();
            SetupRenderer();
        }

        private void ResolveExternalPacking()
        {
            switch (packing) {
                case SpriteLoadingMode.OnChildObject:
                    if (landingObject != null && landingObject != gameObject) {
                        return;
                    }

                    landingObject = ElementFactory.LoadEmptyPrefab();
                    DestroyImmediate(landingObject.GetComponent<Collider2D>());
                    break;
                case SpriteLoadingMode.OnThisObject:
                    if (landingObject != null && landingObject != gameObject) {
                        DestroyImmediate(landingObject);
                        landingObject = gameObject;
                    }
                    break;
            }
        }

        private void SetupRenderer ()
        {
            if (Application.isPlaying && spriteRenderer != null) {
                spriteRenderer.sprite = null;
            }

            spriteRenderer = landingObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sortingOrder = packing == SpriteLoadingMode.OnChildObject
                ? baseObject.GetComponent<SpriteRenderer>().sortingOrder + 1
                : 1;
            spriteRenderer.sortingLayerName = sortingLayerName;
        }

        protected void SetupSprite ()
        {
            if (packing == SpriteLoadingMode.OnChildObject) {
                return;
            }

            if (!Application.isPlaying && spriteRenderer.sprite != null) {
                return;
            }

            if (sprite != null) {
                spriteRenderer.sprite = sprite;
            }
        }

        public static Sprite CreateSprite (Texture2D texture)
        {
            return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), Match3Player.Settings.pixelsPerUnit);
        }

        public void RenderLandingObject (bool render)
        {
            var renderer = landingObject.GetComponent<SpriteRenderer>();
            renderer.enabled = render;
        }

        public void RenderBaseObject (bool render)
        {
            baseObject.GetComponent<SpriteRenderer>().enabled = render;
        }

        private void OnDestroy ()
        {
            if (Application.isPlaying) {
                return;
            }

            spriteRenderer.sprite = null;

            if (landingObject != baseObject) {
                DestroyImmediate(landingObject);
            }
        }

        public bool Equals(Graphics other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return Equals(Sprite, other.Sprite) 
                && string.Equals(sortingLayerName, other.sortingLayerName) 
                && packing == other.packing;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != this.GetType()) {
                return false;
            }
            return Equals((Graphics) obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ (Sprite != null ? Sprite.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (sortingLayerName != null ? sortingLayerName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int) packing;
                return hashCode;
            }
        }
    }
}