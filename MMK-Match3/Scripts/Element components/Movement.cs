﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MMK.Core;

namespace MMK.Match3.TokenComponents
{
    [AddComponentMenu("Match3/Movement")]
    public class Movement : ElementComponent
    {
        private enum Direction
        {
            Left,
            Right,
        }

        private static Direction currentDirection = Direction.Right;
        
        public static void SwitchDirection ()
        {
            switch (currentDirection) {
                case Direction.Left:
                    currentDirection = Direction.Right;
                    break;
                case Direction.Right:
                    currentDirection = Direction.Left;
                    break;
            }
        }

        public static Vector2 FieldOrigin { get; set; }

        public int Row { get; set; }
        public int Column { get; set; }

        public float LastMoved { get; set; }

        public bool canAvoidObstacles = true;

        public PositionInfo Position => new PositionInfo(this);

        private Transform cachedTransform;
        protected new Transform transform => cachedTransform ?? (cachedTransform = base.transform);

        public float DistancePassed { get; private set; }

        protected static ITokensCollection TokensCollection
        {
            get { return Tokens; }
            set { Tokens = value; }
        }

        public static void SwapPosition (Movement one, Movement another)
        {
            var tempColumn = one.Column;
            one.Column = another.Column;
            another.Column = tempColumn;

            var tempRow = one.Row;
            one.Row = another.Row;
            another.Row = tempRow;
        }

        private HashSet<ElementComponent> blockers = new HashSet<ElementComponent>(); 

        /// <summary>
        /// Required to be able to manualy disable this component in the inspector
        /// </summary>
        private void Start ()
        {
            
        }

        private Direction OppositeDirection
        {
            get
            {
                switch (currentDirection) {
                    case Direction.Left:
                        return Direction.Right;
                    case Direction.Right:
                        return Direction.Left;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private GravityVector GetGravity (Direction direction)
        {
            var gravityVector = TokensCollection.GetGravity(Position);
            switch (direction) {
                case Direction.Left:
                    return gravityVector.Left;
                case Direction.Right:
                    return gravityVector.Right;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        public void Block (ElementComponent blocker)
        {
            enabled = false;
            blockers.Add(blocker);
        }

        public void Release (ElementComponent blocker)
        {
            blockers.Remove(blocker);
            enabled = blockers.Count == 0;
        }

        public void Assign(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public void Assign(PositionInfo position)
        {
            Assign(position.row, position.column);
        }

        public virtual bool CanMoveAsight()
        {
            if (!canAvoidObstacles || !enabled) {
                return false;
            }

            var gravityVector = TokensCollection.GetGravity(Position);
            switch (currentDirection) {
                case Direction.Left:
                    return CanMoveInDirection(gravityVector.Left);
                case Direction.Right:
                    return CanMoveInDirection(gravityVector.Right);
                default:
                    return false;
            }
        }

        public virtual bool CanMoveLeftOrRight ()
        {
            if (!canAvoidObstacles || !enabled) {
                return false;
            }

            var gravity = TokensCollection.GetGravity(Position);
            return CanMoveInDirection(gravity.Left) || CanMoveInDirection(gravity.Right);
        }

        public virtual bool CanMoveForward()
        {
            if (!enabled) {
                return false;
            }

            var gravityVector = TokensCollection.GetGravity(Position);
            return CanMoveInDirection(gravityVector);
        }

        protected PositionInfo postionToMove = null;

        protected bool CanMoveInDirection(GravityVector direction)
        {
            var position = Position + direction;
            if (position.Overextend()) {
                return false;
            }

            while (TokensCollection[position] != null && TokensCollection[position].HasComponent<Volatile>()) {
                position = position + TokensCollection.GetGravity(position);
            }

            if (TokensCollection[position] != null || position.Overextend()) {
                return false;
            }

            postionToMove = position;
            return true;
        }

        public virtual bool MoveForvard()
        {
            if (TokensCollection.BottomPositions.Any(pos => pos.Equals(Position))) {
                return false;
            }

            if (!CanMoveForward()) {
                return false;
            }

            var forvardPosition = Position + TokensCollection.GetGravity(Position);
            var token = TokensCollection[forvardPosition];

            if (forvardPosition.Overextend() || (token != null && !token.HasComponent<Volatile>())) {
                return false;
            }

            PositionInfo nullPosition = null;
            while (!forvardPosition.Overextend() && token == null || token.HasComponent<Volatile>()) {
                if (!forvardPosition.Overextend() && token == null) {
                    nullPosition = new PositionInfo(forvardPosition);
                }

                forvardPosition += TokensCollection.GetGravity(forvardPosition);
                token = TokensCollection[forvardPosition];
            }

            MoveTo(nullPosition);
            return true;
        }

        public virtual void MoveAsight()
        {
            MoveTo(postionToMove);
            postionToMove = null;
        }

        protected void MoveTo(PositionInfo position)
        {
            if (position == null) {
                return;
            }

            TokensCollection[position] = gameObject;
            TokensCollection[Position] = null;

            DistancePassed = PositionInfo.Distance(position, Position);

            Row = position.Row;
            Column = position.Column;
        }

        private float timeForAnimation;
        private float lastTimeRecord;
        private float currentTime;
        private const float LerpTime = 1f;

        private float Speed => Match3Player.Settings.tokensMovementSpeed;

        private float LerpValue
        {
            get
            {
                var t = currentTime / LerpTime;
                return 5f * Mathf.Cos(t * Mathf.PI / 3) * Time.fixedDeltaTime;
            }
        }

        public virtual float Animate ()
        {
            StartCoroutine(AnimationCoroutine(Position.Coordinates(FieldOrigin)));
            return timeForAnimation;
        }

        public virtual float Animate (Vector2 destination)
        {
            StartCoroutine(AnimationCoroutine(destination));
            return timeForAnimation;
        }

        private IEnumerator AnimationCoroutine (Vector2 destination)
        {
            var fullDistance = Vector2.Distance(transform.position, destination);
            timeForAnimation = fullDistance * Time.fixedDeltaTime / (Speed * 0.002f);

            if (Math.Abs(timeForAnimation) < 0.000001f) {
                yield break;
            }

            var tween = transform.positionTo(timeForAnimation, destination);

            yield return tween.waitForCompletion();

            gameObject.SendMessage("OnMovementFinished", SendMessageOptions.DontRequireReceiver);
        }
    }
}