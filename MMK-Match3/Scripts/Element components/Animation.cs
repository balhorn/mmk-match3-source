﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;
using MMK.Core;
using MMK.Match3;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace MMK.Match3.TokenComponents
{
    [ExecuteInEditMode]
    [ElementEditorMenu("", true)]
    public class Animation : ElementComponent, IEquatable<Animation>
    {
        protected Animator animator;
        protected AnimationsManager animationManager;
        protected ParticleSystem particleSystem;

        [HideInInspector] public Graphics graphics;
        [HideInInspector] public string path;

        public RuntimeAnimatorController controller;
        public float length;
        public string trigger;
        public GameObject particlePrefab;

        protected float particlesDuration;
        public GameObject Particles
        {
            get { return particlePrefab; }
            set
            {
                particlePrefab = value;
                InitParticles();
            }
        }

        public virtual IEnumerator Play ()
        {
            BeforeStartAnimation();

            int hash = StartAnimation();

            Animate();

            yield return new WaitForSeconds(length);

            AfterWaiting();

            yield return StartCoroutine(StopAnimationCoroutine(hash, particlesDuration));

            AfterAnimation();
        }

        public void Trigger (string trigger = "")
        {
            if (string.IsNullOrEmpty(trigger)) {
                animator.SetTrigger(this.trigger);
                return;
            }
            animator.SetTrigger(trigger);
        }

        public void SetInteger (string name, int value)
        {
            animator.SetInteger(name, value);
        }

        public void SetFloat (string name, float value)
        {
            animator.SetFloat(name, value);
        }

        public int StartAnimation ()
        {
            animator.enabled = true;
            return animationManager.StartAnimation(Hash);
        }

        public void EndAnimation (int hash)
        {
            StartCoroutine(StopAnimationCoroutine(hash, length));
        }

        public void EndAnimation (int hash, UnityAction callback)
        {
            StartCoroutine(StopAnimationCoroutine(hash, length, callback));
        }

        public void EndAnimation (int hash, float delay)
        {
            StartCoroutine(StopAnimationCoroutine(hash, delay));
        }

        public void EndAnimation (int hash, float delay, UnityAction callback)
        {
            StartCoroutine(StopAnimationCoroutine(hash, delay, callback));
        }

        public void EndAnimationImmidiate (int hash)
        {
            animationManager.StopAnimation(hash);
            animator.enabled = false;
        }

        private IEnumerator StopAnimationCoroutine (int hash, float delay)
        {
            yield return new WaitForSeconds(delay);
            animationManager.StopAnimation(hash);
            animator.enabled = false;
        }

        private IEnumerator StopAnimationCoroutine (int hash, float delay, UnityAction callback)
        {
            yield return new WaitForSeconds(delay);
            animationManager.StopAnimation(hash);
            animator.enabled = false;
            callback();
        }

        protected virtual int Hash => Animator.StringToHash("Token " + Time.realtimeSinceStartup + Random.value);

        private void OnRenderObject ()
        {
            if (!Application.isPlaying) {
                Start();
            }
        }

        private void OnColorChanged ()
        {
            particlePrefab = Match3Player.Settings.Sprites.GetParticles(gameObject.GetColor());
            InitParticles();
        }

        protected virtual void Awake ()
        {

        }

        protected virtual void Start ()
        {
            InitComponents();
            LoadController();
        }

        protected virtual void LoadController ()
        {
            if (controller != null) {
                animator.runtimeAnimatorController = controller;
            }
        }

        protected virtual void InitComponents()
        {
            if (graphics == null) {
                animator = GetComponent<Animator>();
                graphics = GetComponent<Graphics>();
            } else {
                animator = graphics.landingObject.GetComponent<Animator>();
            }

            if (particleSystem == null) {
                InitParticles();
            }

            animator.enabled = false;

            if (Application.isPlaying) {
                animationManager = AnimationsManager.Instance;
            }
        }

        private void InitParticles ()
        {
            if (particlePrefab == null) {
                return;
            }

            var holder = Instantiate(particlePrefab, transform.position, Quaternion.identity) as GameObject;
            holder.transform.SetParent(transform);
            particleSystem = holder.GetComponent<ParticleSystem>();
            particlesDuration =
                holder.GetComponentsInChildren<ParticleSystem>().Max(system => system.duration);
            
        }

        protected virtual void AfterAnimation()
        {
            Destroy(gameObject);
        }

        protected virtual void AfterWaiting()
        {
            particleSystem?.Play(true);
        }

        protected virtual void Animate()
        {
            animator.SetTrigger(trigger);
            animator.SetInteger("Color", (int) gameObject.GetColor());
        }

        protected virtual void BeforeStartAnimation()
        {

        }

        private void OnDestroy ()
        {
            if (Application.isPlaying) {
                return;
            }

            animator.runtimeAnimatorController = null;
        }

        public bool Equals(Animation other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            var a = Equals(controller, other.controller);
            var b = length.Equals(other.length);
            var c = string.Equals(trigger, other.trigger);

            return a && b && c;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != this.GetType()) {
                return false;
            }
            return Equals((Animation) obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ (controller != null ? controller.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ length.GetHashCode();
                hashCode = (hashCode * 397) ^ (trigger != null ? trigger.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}

