﻿using MMK.Core;
using MMK.Match3;
using UnityEngine;

namespace MMK.Match3.TokenComponents
{
    [AddComponentMenu("Match3/Color")]
    [ExecuteInEditMode]
    public class Color : ElementComponent, IGraphicsHandler
    {
        public TokenColor color;

        public TokenColor Value
        {
            get { return color; }
            set
            {
                color = value;
                ApplyColor();
                gameObject.SendMessage("OnColorChanged", SendMessageOptions.DontRequireReceiver);
            }
        }

        private SpriteSettings spriteSettings;

        private SpriteSettings Sprites
        {
            get
            {
                if (Application.isPlaying) {
                    return Match3Player.Settings.Sprites;
                }

                if (spriteSettings != null) {
                    return spriteSettings;
                }

                return (spriteSettings = Resources.Load<SpriteSettings>("Sprite settings"));
            }
        }

        private Graphics graphics;

        public Animation Animation { get; set; }
        public Graphics Graphics
        {
            get { return graphics ?? (graphics = GetComponent<Graphics>()); }
            set { graphics = value; }
        }

        private void Start ()
        {
            ApplyColor();
            Utilites.FindGraphicsAndAnimation(gameObject, this);
        }

        private void ApplyColor ()
        {
            if (Graphics == null) {
                return;
            }

            var sprite = Sprites.GetSprite(color);
            Graphics.Sprite = sprite;
        }

        public void SetupGraphics(Graphics graphics)
        {
            this.Graphics = graphics;
        }

        public void SetupAnimation(Animation animation)
        {
            this.Animation = animation;
            this.Animation.graphics = Graphics;
        }
    }
}


