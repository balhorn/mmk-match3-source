﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using MMK.Core;
using MMK.Match3;

namespace MMK.Match3.TokenComponents
{
    [AddComponentMenu("Match3/Durability")]
    [ElementEditorMenu("Durability/Durability")]
    public class Durability : ElementComponent, IComparable<Durability>, IGraphicsHandler,
        IEquatable<Durability>, IMatch3ScoreEntity
    {
        public int durability;
        public int order;

        public bool IsFirst { get; set; }

        [HideInInspector] [SerializeField] private Graphics graphics;
        [HideInInspector] [SerializeField] private Animation animation;

        public Graphics Graphics { get { return graphics; } set { graphics = value; } }
        public Animation Animation { get { return animation; } set { animation = value; } }
        
        public int Score { get; set; }
        public bool DependsOnCascade { get; set; }
        public Vector2 Coordinates { get; set; }
        public UnityEngine.Color TextColor { get; set; }

        protected ScoreManager scoreManager;
        protected IDamageDealer damageDealer;

        public static event Delegates.TokenDestroyed OnTokenDestroyed;

        protected const string DurabilityParameter = "Durability";

        public virtual void TakeDamage (IDamageDealer damageDealer)
        {
            if (durability <= 0) {
                return;
            }
            this.damageDealer = damageDealer;

            var overDamage = damageDealer.DamageAmount - durability;
            durability -= damageDealer.DamageAmount;

            switch (Math.Sign(overDamage)) {
                case 1:
                    Disable();
                    var nextTarget = FindNext();
                    if (nextTarget != null) {
                        nextTarget.TakeDamage(damageDealer);
                    }
                    break;
                case 0:
                    Disable();
                    break;
                case -1:
                    gameObject.SendMessage("OnDurabilityLost", SendMessageOptions.DontRequireReceiver);
                    UpdateAnimation();
                    break;
            }
        }

        protected virtual void UpdateAnimation()
        {
            if (Animation == null) {
                Debug.Log($"Missing animation on {gameObject.name}. Unable to update");
                return;
            }

            int hash = Animation.StartAnimation();
            Animation.SetInteger(DurabilityParameter, durability);
            Animation.Trigger();
            Animation.EndAnimation(hash);
        }

        protected virtual IEnumerator SetupAnimation ()
        {
            yield return new WaitForEndOfFrame();
            Animation?.SetInteger(DurabilityParameter, durability);
        }

        protected Durability FindNext ()
        {
            return GetComponents<Durability>()
                .Where(component => component.enabled)
                .Max();
        }

        protected bool TheLast
        {
            get
            {
                var durabilities = GetComponents<Durability>();
                try {
                    return durabilities.Count(component => component.order > order && component.enabled) == 0;
                } catch (InvalidOperationException) {
                    return true;
                }
            }
        }

        protected virtual void Start ()
        {
            InitComponents();
            SetupScore();
            ArrangePriority();
            Utilites.FindGraphicsAndAnimation(gameObject, this);
            StartCoroutine(SetupAnimation());
        }

        protected virtual void SetupScore ()
        {
            Score = Match3Player.Settings.tokenScore;
            Coordinates = transform.position;
            TextColor = Utilites.GetTokenColor(gameObject.GetColor());

            DependsOnCascade = false;
        }

        protected virtual void InitComponents()
        {
            scoreManager = ShapesManager.Instance.GetComponent<ScoreManager>();
        }

        public virtual void SetupGraphics (Graphics graphics)
        {
            Graphics = graphics;
        }

        public virtual void SetupAnimation (Animation animation)
        {
            Animation = animation;
            Animation.graphics = Graphics;
        }

        private void ArrangePriority ()
        {
            try {
                var first = GetComponents<Durability>()
                    .Where(component => component.enabled)
                    .First(component => component.IsFirst);

                if (first.order < order) {
                    IsFirst = true;
                    first.IsFirst = false;
                }
            } catch (InvalidOperationException) {
                IsFirst = true;
            }
        }

        public int CompareTo(Durability other)
        {
            return other == null ? 1 : order.CompareTo(other.order);
        }

        protected virtual void Disable ()
        {
            var active = GetComponent<Active>();
            if (active != null) {
                active.Activate();
            } else {
                StartCoroutine(Animation.Play());
            }

            FireEvent();
            gameObject.SendMessage(TheLast == this ? "OnTokenDestroyed" : "OnDurabilityBroken",
                SendMessageOptions.DontRequireReceiver);

            enabled = false;
        }

        protected virtual void FireEvent ()
        {
            if (OnTokenDestroyed != null) {
                OnTokenDestroyed(gameObject.GetColor(), new PositionInfo(GetComponent<Movement>()), damageDealer);
            }
        }

        protected virtual void OnDurabilityDisabled ()
        {
            if (!enabled) {
                return;
            }

            try {
                var maxOrder = GetComponents<Durability>()
                    .Where(component => component.enabled)
                    .Select(component => component.order)
                    .Max();

                if (order == maxOrder) {
                    IsFirst = true;
                }
            } catch (InvalidOperationException) {
                IsFirst = true;
            }
        }

        public bool Equals(Durability other)
        {
            return durability == other.durability && order == other.order;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != this.GetType()) {
                return false;
            }
            return Equals((Durability) obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ order;
                return hashCode;
            }
        }
    }
}


