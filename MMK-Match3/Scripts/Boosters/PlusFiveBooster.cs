﻿using MMK.Core;
using MMK.Core.Boosters;
using UnityEngine;

namespace MMK.Match3
{
    public class PlusFiveBooster : AbstractBooster
    {
        public override void UpdateUI()
        {
        }

        public override void Trigger(GameObject targetToken)
        {

        }

        public override void Activate()
        {
            if (Amount <= 0) {
                return;
            }

            //TODO Remove hardcoded value
            Constraint.Current.Increment(5);
            DecreaseAmount();
        }

        protected override void Update()
        {
        }
    }
}
