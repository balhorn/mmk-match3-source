﻿using System.Collections;
using MMK.Core;
using MMK.Core.Boosters;
using MMK.Match3.TokenComponents;
using UnityEngine;

namespace MMK.Match3
{
    public class HandBooster : AbstractBooster
    {
        protected GameObject firstHit;
        private IEnumerator animationCoroutine;

        public override void UpdateUI()
        {
        }

        public override void Trigger (GameObject targetToken)
        {
            StartCoroutine(Swap(targetToken));
        }

        private IEnumerator Swap(GameObject target)
        {
            if (!CanSwap(target)) {
                yield break;
            }

            ElementComponent.Tokens.Swap(firstHit, target);

            firstHit.transform.positionTo(Constants.AnimationDuration, target.transform.position);
            target.transform.positionTo(Constants.AnimationDuration, firstHit.transform.position);
            yield return new WaitForSeconds(Constants.AnimationDuration);

            firstHit.SendMessage("OnMovementFinished");
            target.SendMessage("OnMovementFinished");

            CheckForMatch(target);

            Deactivate();
        }

        protected bool CheckForMatch (GameObject target)
        {
            var match = firstHit.GetComponent<BonusMatch>();
            var targetMatch = target.GetComponent<BonusMatch>();

            if (match != null && targetMatch != null) {
                return false;
            }

            if (match != null && match.matchableWithTokens) {
                return target.Match(target);
            }

            if (targetMatch != null && targetMatch.matchableWithTokens) {
                return firstHit.Match(firstHit);
            }

            return firstHit.Match(target);
        }

        private bool CanSwap (GameObject target)
        {
            var m1 = firstHit.GetComponent<Movement>();
            var m2 = target.GetComponent<Movement>();

            return m1 != null && m2 != null
                   && m1.enabled && m2.enabled
                   && Utilites.AreVerticalOrHorizontalNeighbors(m1, m2);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            StopAnimation();
            firstHit = null;
        }

        protected override void Update()
        {
            if (GameController.state != GameState.BoosterTargetSelection
                || !Input.GetMouseButtonDown(0)
                || !running) {
                return;
            }

            var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider == null) {
                return;
            }

            if (firstHit == null) {
                firstHit = hit.collider.gameObject;
                AnimateSelectedToken();
            } else {
                DecreaseAmount();
                Trigger(hit.collider.gameObject);
            }
        }

        private Vector3 scaleBackup;

        private void AnimateSelectedToken ()
        {
            animationCoroutine = Animating();
            StartCoroutine(animationCoroutine);
        }

        private IEnumerator Animating ()
        {
            const float addition = 0.3f;
            scaleBackup = firstHit.transform.localScale;

            while (true) {
                for (int i = 0; i < 3; i++) {
                    firstHit.transform.scaleTo(Constants.OpacityAnimationFrameDelay, addition * i);
                    yield return new WaitForSeconds(Constants.OpacityAnimationFrameDelay);
                }

                for (int i = 0; i < 3; i++) {
                    firstHit.transform.scaleFrom(Constants.OpacityAnimationFrameDelay,
                        new Vector3(addition * i, addition * i, 0f));
                    yield return new WaitForSeconds(Constants.OpacityAnimationFrameDelay);
                }
            }
        }

        private void StopAnimation ()
        {
            if (animationCoroutine == null) {
                return;
            }

            if (firstHit != null) {
                firstHit.transform.localScale = scaleBackup;
            }
            StopCoroutine(animationCoroutine);
        }
    }
}
