﻿using System.Collections.Generic;
using MMK.Core.Boosters;
using UnityEngine;

namespace MMK.Match3
{
    public class HammerBooster : AbstractBooster, IDamageDealer
    {
        protected new void Start()
        {
            base.Start();
            DamageAmount = 1;
            //ScoreMultiplier = 1;
        }

        public int DamageAmount { get; set; }

        public int ScoreMultiplier { get; set; }

        public void DealDamage(IEnumerable<GameObject> objects)
        {
            foreach (var o in objects) {
                o.DealDamage(this);
            }
        }

        public override void UpdateUI()
        {

        }

        public override void Trigger (GameObject targetToken)
        {
            if (targetToken.DealDamage(this)) {
                ScoreManager.RecordScore(targetToken.Durability());
            }
        }

        protected override void Update()
        {

        }
    }
}
