﻿using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Core.UI;
using MMK.Match3.Preboosters;
using UnityEngine;

namespace MMK.Match3
{
    public class PreboostersPanel : MonoBehaviour
    {
        public GetBoosterWindow getBooster;
        public GameObject emptyPrebooster;
        public Transform parentTransform;

        private Dictionary<string, GameObject> preboosters;

        private void Awake ()
        {
            preboosters = Resources.LoadAll<GameObject>(Constants.PreboostersFolder)
                .Where(prefab => prefab.HasComponent<Prebooster>())
                .ToDictionary(prebooster => prebooster.GetComponent<Prebooster>().GetType().Name,
                    prebooster => prebooster);
        }

        public void InstantiatePreboosters (Level level)
        {
            for (int i = 0; i < parentTransform.childCount; i++) {
                Destroy(parentTransform.GetChild(i).gameObject);
            }

            var spawned = 0;
            var m3Level = level.PremadeLevel as Match3PremadeLevel;
            foreach (var prefabTransform in m3Level.preboosters
                .Where (preboosterInfo => preboosters.ContainsKey(preboosterInfo.name))
                .Select(preboosterInfo => Instantiate(preboosters[preboosterInfo.name]).transform)) {
                prefabTransform.SetParent(parentTransform, false);
                prefabTransform.GetComponent<Prebooster>().GetBoosterWindow = getBooster;

                if (++spawned >= Constants.MaximumPreboostersPerLevel) {
                    break;
                }
            }

            for (int i = spawned; i < Constants.MaximumPreboostersPerLevel; i++) {
                var emptyItem = Instantiate(emptyPrebooster).transform;
                emptyItem.SetParent(parentTransform, false);
            }
        }

        public void SetupPreboosters ()
        {
            var preboosters = GetComponentsInChildren<Prebooster>();
            foreach (var prebooster in preboosters) {
                prebooster.Save();
            }
        }
    }
}
