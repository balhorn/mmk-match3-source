﻿using MMK.Core;
using MMK.Core.UI;
using UnityEngine;

namespace MMK.Match3
{
    public class Match3PopupManager : PopUpManager
    {
        public Goal goal;
        public ComplimentaryWords winWords;

        protected override string MapSceneName => Constants.MapSceneName;

        /// <summary>
        /// Shows notification that the player has successfully completed level
        /// </summary>
        /// <returns>Number of seconds while popup will be visible</returns>
        public float ShowWinWord ()
        {
            if (winWords == null) {
                return 0f;
            }

            var animationLength = winWords.animationLength;
            winWords.Show();
            return animationLength;
        }

        public override void StartGame()
        {
            base.StartGame();
            var manager = FindObjectOfType<ShapesManager>();
            StartCoroutine(manager.CollapseTokens());
        }

        public override void Continue()
        {
            if (Player.Instance.HardCurrency >= Match3Player.Settings.additionalTurnCost) {
                GameController.state = GameState.Idle;
                outOfMoves.Hide();
                Constraint.Current.Increment(Match3Player.Settings.additionalTurnsAmount);
                Player.Instance.SpendCurrency(Match3Player.Settings.additionalTurnCost);
            } else {
                shopWindow.Show();
            }
        }

        protected override void Start()
        {
            goal?.Show();
        }
    }
}