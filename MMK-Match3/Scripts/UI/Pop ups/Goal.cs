﻿using System.Collections;
using MMK.Core;
using MMK.Core.UI;
using UnityEngine;

namespace MMK.Match3
{
    public class Goal : PopUp
    {
        public float timeBeforeClose = 3f;

        private Coroutine closingCoroutine;

        void Start ()
        {
            Invoke(nameof(Close), timeBeforeClose);
            GetComponent<LevelAimText>()?.SetupText(Level.Current);
            SoundManager.LevelStart();
        }

        private IEnumerator ClosingCoroutine ()
        {
            yield return new WaitForSeconds(timeBeforeClose);
            Close();
        }

        public void Close ()
        {
            CancelInvoke();
            var popUp = FindObjectOfType<PopUpManager>();
            if (popUp != null) {
                popUp.StartTutorial();
            }

            Hide();
        }
    }
}
