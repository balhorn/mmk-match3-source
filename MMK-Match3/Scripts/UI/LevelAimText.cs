﻿using MMK.Core;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Match3
{
    public class LevelAimText : MonoBehaviour
    {
        public Text levelAimText;
        public Image levelAimIcon;

        public void SetupText (Level level)
        {
            var premadeLevel = level.premadeLevel as Match3PremadeLevel;
            var textItem = Match3Player.Settings.Texts.GetLevelTypeText(premadeLevel.type);
            var text = textItem.text;

            if (string.IsNullOrEmpty(text)) {
                Debug.LogWarning($"Level aim text for {premadeLevel.type} is not specified. You can go to M3K settings menu to assign it");
            }

            var sprite = textItem.sprite;
            if (sprite == null) {
                Debug.LogWarning($"Level aim icon for {premadeLevel.type} is not specified. You can go to M3K settings menu to assign it");
            }

            if (levelAimText != null) {
                levelAimText.text = text;
            }

            if (levelAimIcon != null) {
                levelAimIcon.sprite = sprite;
                levelAimIcon.SetNativeSize();
            }
        }
    }
}