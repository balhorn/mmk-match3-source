﻿    using UnityEngine;

namespace MMK.Match3
{
    public class OctopusAppear : MonoBehaviour
    {
        public Animator anim;

        public void Appear ()
        {
            anim.SetTrigger(Animator.StringToHash("Appear"));
        }
    }
}
