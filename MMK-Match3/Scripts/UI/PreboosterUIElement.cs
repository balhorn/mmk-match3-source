﻿using MMK.Core;
using MMK.Match3.Preboosters;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Match3
{
    public class PreboosterUIElement : MonoBehaviour
    {
        public GameObject amountButton;
        public PreboosterSprite[]   sprites;

        private Image   mainImage;
        private Outline outline;
        private Text    amountText;

        private Prebooster prebooster;

        [SerializeField]
        [HideInInspector]
        private bool handleEvent = true;
        private int Amount => Match3Player.Instance.GetBoosterAmount(prebooster);

        public Image MainImage
        {
            get { return mainImage ?? (mainImage = GetComponent<Image>()); }
            set { mainImage = value; }
        }

        private void Start ()
        {
            prebooster = GetComponent<Prebooster>();
            MainImage = GetComponent<Image>();
            outline = GetComponent<Outline>();
            outline.enabled = false;

            if (amountButton == null) {
                return;
            }

            amountText = amountButton.GetComponentInChildren<Text>();

            UpdateCounter();
            Player.BoosterUpdated += UpdateCounter;
        }

        public void SwitchOutline ()
        {
            outline.enabled = !outline.enabled;
        }

        public void SetNonInteractive ()
        {
            Destroy(GetComponent<Button>());
            amountButton.SetActive(false);

            handleEvent = false;
        }

        public void SetSprite (int index)
        {
            if (sprites == null || sprites.Length == 0) {
                Debug.Log("Please, assign required prebooster sprites to the Prebooster UI Element component");
                return;
            }

            if (index < 0 || index >= sprites.Length) {
                return;
            }

            SetSprite(sprites[index]);
        }

        private void SetSprite (PreboosterSprite sprite)
        {
            if (sprite.amount >= 2) {
                amountButton.SetActive(true);
                amountButton.GetComponentInChildren<Text>().text = sprite.amount.ToString();
            } else {
                amountButton.SetActive(false);
            }

            MainImage.sprite = sprite.sprite;
        }

        private void UpdateCounter ()
        {
            if (!handleEvent) {
                return;
            }

            int amount = Amount;
            amountText.text = amount > 0 ? amount.ToString() : "+";
        }

    

        private void OnDestroy ()
        {
            Player.BoosterUpdated -= UpdateCounter;
        }
    }
}
