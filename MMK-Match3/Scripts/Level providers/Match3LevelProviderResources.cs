﻿using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    public class Match3LevelProviderResources : LevelProviderResources<Match3PremadeLevel>
    {
        public Match3LevelProviderResources(MonoBehaviour coroutineHandler, string levelsFolder) : base(coroutineHandler, levelsFolder)
        {
        }
    }
}