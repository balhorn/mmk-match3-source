﻿using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    // ReSharper disable once InconsistentNaming
    public abstract class Match3LevelProviderJSON : LevelProviderJSON<Match3PremadeLevel>
    {
        protected Match3LevelProviderJSON(MonoBehaviour coroutineHandler) : base(coroutineHandler)
        {
        }
    }
}