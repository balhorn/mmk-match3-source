﻿using MMK.Core;
using UnityEngine;

namespace MMK.Match3
{
    public class Match3LevelProviderHttp : LevelProviderHttp<Match3PremadeLevel>
    {
        public Match3LevelProviderHttp(MonoBehaviour coroutineHandler) : base(coroutineHandler)
        {
        }
    }
}