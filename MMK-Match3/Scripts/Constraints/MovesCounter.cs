﻿using MMK.Core;
using MMK.Match3;

namespace MMK.Match3
{
    public class MovesCounter : Match3Constraint
    {
        private int movesLeft;

        public int MovesLeft => movesLeft;

        public override int Amount => MovesLeft;
        public float progressFillDuration = 0.5f;

        protected override void Start()
        {
            base.Start();

            label.text = "MOVES";

            movesLeft = LevelConfig.turns;
            IsLevelCompleted = false;

            ShapesManager.OnTurnStarted += Decrement;

            UIOutput();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            ShapesManager.OnTurnStarted -= Decrement;
        }

        public override void Decrement()
        {
            if (movesLeft-- >= 0) {
                UIOutput();
            }
        }

        public override void Increment(int amount)
        {
            movesLeft += amount;
            isGameOver = false;
            UIOutput();
        }

        private bool isGameOver;

        public override void CheckGameOver()
        {
            if (GameController.state == GameState.Pause) {
                return;
            }

            RegularCheck();
        }

        private void RegularCheck()
        {
            if (PlayToTheEnd && movesLeft > 0) {
                return;
            }

            if (isLevelCompleted) {
                isLevelCompleted = false;
                StartCoroutine(WinBonus());
            } else if (WinBonusEnded) {
                GameController.state = GameState.Pause;
                isLevelCompleted = true;
                StartCoroutine(Win());
            } else if (GameController.state != GameState.WinBonus && movesLeft <= 0 && !isGameOver) {
                GameController.state = GameState.Pause;
                isGameOver = true;
                UpdateAndSave();
                StartCoroutine(GameOver());
            }
        }

        private void ToTheEndCheck ()
        {
            if (movesLeft <= 0 && isLevelCompleted) {
                GameController.state = GameState.Pause;
                StartCoroutine(Win());
            } else if(movesLeft <= 0 && !isLevelCompleted && !isGameOver) {
                GameController.state = GameState.Pause;
                isGameOver = true;
                UpdateAndSave();
                StartCoroutine(GameOver());
            }
        }

        protected override void UIOutput ()
        {
            movesLeft = movesLeft < 0 ? 0 : movesLeft;
            outputText.text = movesLeft.ToString ();

            if (sliderImage == null) {
                return;
            }

            Go.to(sliderImage, progressFillDuration, new GoTweenConfig()
                .floatProp("fillAmount", (float) movesLeft / LevelConfig.turns));
        }
    }
}