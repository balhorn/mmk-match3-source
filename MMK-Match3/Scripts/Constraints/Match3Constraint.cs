﻿using System.Collections;
using MMK.Core;

namespace MMK.Match3
{
    public abstract class Match3Constraint : Constraint
    {
        public ShapesManager shapesManager;
        public new Match3PopupManager popUpManager;

        public bool WinBonusEnded
        {
            get; set;
        }

        protected IEnumerator WinBonus ()
        {
            yield return StartCoroutine(shapesManager.WinBonusCoroutine());
        }
    }
}