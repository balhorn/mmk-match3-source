﻿using UnityEngine;
using UnityEngine.UI;
using MMK.Core;

namespace MMK.Match3
{
    public class Timer : Match3Constraint
    {
        public int timeForLevel;
        public float currentTime;

        public Image UICircle;
        public Animator UIAnimator;

        private bool stop = false;

        public override int Amount => (int) currentTime;

        protected override void Start ()
        {
            base.Start();

            label.text = "TIME";

            timeForLevel = LevelConfig.turns;
            currentTime = timeForLevel;
            UIOutput();

            LevelConfig.winBonusEnabled = false;
            enabled = false;
        }

        // Update is called once per frame
        private void Update ()
        {
            if (stop || GameController.state == GameState.Pause) {
                return;
            }

            if (currentTime <= 0) {
                stop = true;
                CheckGameOver();
                return;
            }

            currentTime -= Time.deltaTime;
            UpdateText ();
        }

        public override void Decrement()
        {
            if (currentTime-- >= 0) {
                UIOutput();
            }
        }

        public override void CheckGameOver ()
        {
            if (isLevelCompleted) {
                stop = true;
                isLevelCompleted = true; // = false;
                StartCoroutine(Win());
            } else if (currentTime <= 0) {
                UpdateAndSave();
                StartCoroutine(GameOver());
            }
        }

        public override void Increment(int amount)
        {
            stop = false;
            currentTime = Mathf.Clamp(currentTime + amount, amount, timeForLevel);
            UIOutput();
        }

        protected override void UIOutput()
        {
            UpdateText();
            UpdateAnimation();
        }

        private void UpdateText()
        {
            outputText.text = Mathf.RoundToInt(currentTime).ToString();
        }

        private void UpdateAnimation()
        {
            if (sliderImage == null) {
                return;
            }

            var amount = (timeForLevel - currentTime) / timeForLevel;
            sliderImage.fillAmount = amount;
        }
    }
}