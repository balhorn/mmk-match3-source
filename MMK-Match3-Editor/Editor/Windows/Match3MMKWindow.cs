﻿using MMK.Core.Editor;
using UnityEditor;

namespace MMK.Match3.Editor
{
    public class Match3MMKWindow : MMKWindow
    {
        [MenuItem("Window/MMK/Level editor", false, 1)]
        public static MMKWindow OpenLevelEditor ()
        {
            if (instance == null) {
                OpenWindow();
            }

            ((Match3MMKWindow)instance).OpenTab<Match3LevelEditorTab>();
            return instance;
        }

        [MenuItem("Window/MMK/Elements editor", false, 2)]
        public static MMKWindow OpenElementsEditor ()
        {
            if (instance == null) {
                OpenWindow();
            }

            ((Match3MMKWindow) instance).OpenTab<ElementEditorTab>();
            return instance;
        }

        static Match3MMKWindow()
        {
            windowType = typeof(Match3MMKWindow);
        }

        protected override void InitTabs()
        {
            base.InitTabs();
            AddTab<Match3LevelEditorTab>(0);
            AddTab<ElementEditorTab>(1);
            OverrideTab<SettingsTab, Match3SettingsTab>();
        }
    }
}