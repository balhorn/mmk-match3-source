﻿using UnityEditor;
using UnityEngine;

namespace MMK.Match3.Editor
{
    public class AddGraphicsWindow : EditorWindow
    {
        private enum GraphicsHandleMode
        {
            Internal,
            External
        }

        public IGraphicsHandler Handler { get; set; }

        private GUIContent spriteContent;
        private GUIContent editorSpriteContent;
        private GUIContent graphicsHandleContent;

        private GraphicsHandleMode graphicsHandleMode = GraphicsHandleMode.Internal;
        private Sprite sprite;
        private Sprite editorSprite;

        private void OnEnable ()
        {
            spriteContent = new GUIContent("Sprite") {tooltip = "Sprite to be displayed in game"};
            editorSpriteContent = new GUIContent("Editor sprite") {
            };
            graphicsHandleContent = new GUIContent("Graphics handle mode") {
            };
        }

        private void OnGUI ()
        {
            sprite = (Sprite) EditorGUILayout.ObjectField(spriteContent , this.sprite, typeof (Sprite), false);

            //GUI.enabled = MMK.edition != Edition.Community;
            editorSprite = (Sprite) EditorGUILayout.ObjectField(editorSpriteContent, editorSprite, typeof (Sprite),
                false);
            //GUI.enabled = true;

            graphicsHandleMode = (GraphicsHandleMode) EditorGUILayout.EnumPopup(graphicsHandleContent, graphicsHandleMode);

            if (GUILayout.Button("Done")) {
                Apply();
            }
        }

        private void Apply ()
        {
            var token = ((Component) Handler).gameObject;
            var graphics = token.AddComponent<MMK.Match3.TokenComponents.Graphics>();

            if (graphicsHandleMode == GraphicsHandleMode.External) {
                graphics.packing = SpriteLoadingMode.OnChildObject;
                graphics.landingObject = ElementFactory.LoadEmptyPrefab();

                DestroyImmediate(graphics.landingObject.GetComponent<Collider2D>());
            }

            graphics.Sprite = sprite;
            graphics.editorSprite = editorSprite;
        
            Handler.SetupGraphics(graphics);
            Close();
        }
    }
}
