﻿using System;
using System.Linq;
using System.Reflection;
using MMK.Core;
using UnityEditor;
using UnityEngine;

namespace MMK.Match3.Editor
{
    public class AddAnimationWindow : EditorWindow
    {
        public IGraphicsHandler Handler { get; set; }

        private RuntimeAnimatorController controller;
        private float length;
        private string trigger;
        private GameObject particlesPrefab;

        private readonly Type[]   availableTypes;
        private readonly string[] typeNames;
        private int index;

        private bool isValid = true;

        public AddAnimationWindow ()
        {
            var animationType = typeof (TokenComponents.Animation);
            availableTypes = Assembly.GetAssembly(animationType).GetTypes()
                .Where(type => type.IsSubclassOf(animationType)
                               && !type.IsAbstract
                               || type == animationType)
                .ToArray();
            typeNames = availableTypes.Select(type => type.Name).ToArray();
        }

        private void OnGUI ()
        {
            index = EditorGUILayout.Popup(index, typeNames);

            DoubleSpace();

            controller =
                (RuntimeAnimatorController)
                EditorGUILayout.ObjectField("Controller", controller, typeof (RuntimeAnimatorController), false);
            length = EditorGUILayout.FloatField("Length", length);
            trigger = EditorGUILayout.TextField("Trigger", trigger);
            EditorGUI.BeginChangeCheck();
            particlesPrefab =
                EditorGUILayout.ObjectField("Particles prefab", particlesPrefab, typeof (GameObject), false) as GameObject;
            if (EditorGUI.EndChangeCheck()) {
                Validate();
            }

            if (GUILayout.Button("Done")) {
                Apply();
            }
        }

        private void Validate()
        {
            isValid = particlesPrefab == null || particlesPrefab.HasComponent<ParticleSystem>();

            if (!isValid) {
                EditorGUILayout.HelpBox("Provided object doesn't contain particle system", MessageType.Error);
            }
        }

        private void Apply()
        {
            if (!isValid) {
                return;
            }

            var token = ((Component) Handler).gameObject;
            var animation = token.AddComponent(availableTypes[index]) as MMK.Match3.TokenComponents.Animation;
            animation.controller = controller;
            animation.length = length;
            animation.trigger = trigger;
            animation.particlePrefab = particlesPrefab;

            Handler.SetupAnimation(animation);
            Close();
        }

        private static void DoubleSpace ()
        {
            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }
    }
}
