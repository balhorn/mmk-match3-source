﻿using UnityEditor;
using UnityEngine;

namespace MMK.Match3.Editor
{
    [CustomPropertyDrawer(typeof(PreboosterSprite))]
    public class PreboosterSpritesDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var sprite = property.FindPropertyRelative("sprite");
            var amount = property.FindPropertyRelative("amount");

            EditorGUI.PropertyField(new Rect(position.x, position.y, 5 * position.width / 6, position.height), sprite);
            EditorGUI.PropertyField(
                new Rect(position.x + 5 * position.width / 6, position.y, position.width / 6, position.height), amount, new GUIContent());
        }
    }
}
