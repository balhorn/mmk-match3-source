﻿using System.Linq;
using MMK.Core.Editor;

namespace MMK.Match3.Editor
{
    public class Match3SettingsTab : SettingsTab
    {
        public Match3SettingsTab()
        {
            subsections[0] = new SettingsSection<Match3Settings>("GameSettings");
            AddSubsection(new SpriteSettingsSubsection("Sprite settings"), 1);
            AddSubsection(new TextSettingsSubsection("Text settings"), 1);

            subsectionsContents = subsections.Select(sec => sec.TabContent).ToArray();
        }
    }
}