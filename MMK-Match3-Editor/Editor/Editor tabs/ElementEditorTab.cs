﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using MMK.Core;
using MMK.Core.Editor;
using MMK.Match3.TokenComponents;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace MMK.Match3.Editor
{
    public class ElementEditorTab : EditorTab
    {
        private const string BackgroundFolder = Constants.MMKMatch3Root + "Resources/Prefabs/Background";
        private const string HolesFolder = Constants.MMKMatch3Root + "Resources/Prefabs/Holes";
        private const string CellsFolder = Constants.MMKMatch3Root + "Resources/Prefabs/Base";
        private const string TokensResourcesFolder = Constants.MMKMatch3Root + "Resources/Prefabs/Tokens";
        private const string AttachmentsFolder = Constants.MMKMatch3Root + "Resources/Prefabs/Attachments";

        private const string BackgroundName = "Empty background";
        private const string HoleName = "Hole";
        private const string CellName = "Empty cell";
        private const string EmptyTokenName = "Empty prefab";
        private const string EmptyAttachmentName = "Empty Attachment";

        private const string TokensExtension = ".prefab";

        public enum ElementType { Background, Hole, Cell, Token, Attachment }
        public ElementType type;
        public GameObject currentToken;

        private Texture2D backgroundIcon;
        private Texture2D holeIcon;
        private Texture2D baseIcon;
        private Texture2D tokenIcon;
        private Texture2D attachmentIcon;

        private GUIContent newBackgroundButtonContent;
        private GUIContent newHoleButtonContent;
        private GUIContent newBaseButtonContent;
        private GUIContent newTokenButtonContent;
        private GUIContent newAttachmentButtonContent;

        private Vector2 scrollPosition;
        private GUIContent[] elementEditorOptions;

        private string Folder
        {
            get
            {
                switch (type) {
                    case ElementType.Background:
                        return BackgroundFolder;
                    case ElementType.Hole:
                        return HolesFolder;
                    case ElementType.Cell:
                        return CellsFolder;
                    case ElementType.Token:
                        return TokensResourcesFolder;
                    case ElementType.Attachment:
                        return AttachmentsFolder;
                    default:
                        return string.Empty;
                }
            }
        }

        private string Name
        {
            get
            {
                switch (type) {
                    case ElementType.Background:
                        return BackgroundName;
                    case ElementType.Hole:
                        return HoleName;
                    case ElementType.Cell:
                        return CellName;
                    case ElementType.Token:
                        return EmptyTokenName;
                    case ElementType.Attachment:
                        return EmptyAttachmentName;
                    default:
                        return string.Empty;
                }
            }
        }

        private GUIStyle eeButton => MMKStyles.GetStyleByName("eeButton");



        #region Drawing
        
        protected override void DrawBody()
        {
            DrawElementEditor();
        }

        protected override void DrawFooter()
        {
            DrawElementEditorButtons();
        }

        private void DrawElementEditor()
        {
            if (currentToken == null) {
                DrawCreateMenu();
            } else {
                DrawEditMenu();
            }
        }

        private void DrawElementEditorButtons()
        {
            EditorGUILayout.BeginVertical(MMKStyles.footer, EditorUtilites.Height(48f));

            GUILayout.FlexibleSpace();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Load", MMKStyles.footerButton, EditorUtilites.Width(128f), EditorUtilites.Height(48f))) {
                Open();
            }

            GUILayout.FlexibleSpace();

            var defaultColor = GUI.color;
            GUI.color = EditorUtilites.HexToColor("#ffbdbd");
            GUI.enabled = currentToken != null;
            if (GUILayout.Button("Reset", MMKStyles.footerButton, EditorUtilites.Width(128f), EditorUtilites.Height(48f))) {
                Reset();
            }
            GUI.enabled = true;

            GUI.color = EditorUtilites.HexToColor("#e7fabf");
            if (GUILayout.Button("Save", MMKStyles.footerButton, EditorUtilites.Width(128f), EditorUtilites.Height(48f))) {
                Save();
            }
            GUI.color = defaultColor;

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        private void DrawEditMenu()
        {
            EditorGUILayout.BeginHorizontal(MMKStyles.header);

            EditorGUI.BeginChangeCheck();
            var index = GUILayout.Toolbar((int)type, elementEditorOptions, EditorUtilites.Height(54f));
            if (EditorGUI.EndChangeCheck()) {
                ChangeType((ElementType) index);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Element type: ", EditorStyles.boldLabel);
            EditorGUILayout.LabelField(type.ToString());
            EditorGUILayout.EndHorizontal();

            currentToken.name = EditorGUILayout.TextField(currentToken.name);
            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
            foreach (var component in currentToken.GetComponents<ElementComponent>()) {
                EditorGUILayout.InspectorTitlebar(true, component);
                UnityEditor.Editor.CreateEditor(component)?.OnInspectorGUI();
            }

            EditorGUILayout.Space();
            EditorGUILayout.Separator();
            EditorGUILayout.Space();

            if (GUILayout.Button("Add component")) {
                DrawAddComponentMenu();
            }

            EditorGUILayout.EndScrollView();

        }

        private void DrawCreateMenu()
        {
            var type = ElementType.Background;
            var pressed = false;

            GUILayout.Space(EditorUtilites.CalculateSize(44f));

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(EditorUtilites.CalculateSize(44f));
            EditorGUILayout.LabelField("Create new element:", EditorStyles.boldLabel);
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(EditorUtilites.CalculateSize(14f));

            if (GUILayout.Button(newBackgroundButtonContent, eeButton, EditorUtilites.Width(180f), EditorUtilites.Height(58f))) {
                type = ElementType.Background;
                pressed = true;
            }

            if (GUILayout.Button(newHoleButtonContent, eeButton, EditorUtilites.Width(180f), EditorUtilites.Height(58f))) {
                type = ElementType.Hole;
                pressed = true;
            }

            if (GUILayout.Button(newBaseButtonContent, eeButton, EditorUtilites.Width(180f), EditorUtilites.Height(58f))) {
                type = ElementType.Cell;
                pressed = true;
            }

            if (GUILayout.Button(newTokenButtonContent, eeButton, EditorUtilites.Width(180f), EditorUtilites.Height(58f))) {
                type = ElementType.Token;
                pressed = true;
            }

            if (GUILayout.Button(newAttachmentButtonContent, eeButton, EditorUtilites.Width(180f), EditorUtilites.Height(58f))) {
                type = ElementType.Attachment;
                pressed = true;
            }

            if (pressed) {
                Create(type);
            }
        }

        private void DrawAddComponentMenu ()
        {
            var elementComponents = EditorUtilites.GetDerivedTypes(typeof (ElementComponent));
            var menu = new GenericMenu();
            foreach (var component in elementComponents) {
                var attribute =
                    (ElementEditorMenuAttribute)
                    Attribute.GetCustomAttribute(component, typeof (ElementEditorMenuAttribute));
                if (attribute != null && attribute.Ignore) {
                    continue;
                }
                menu.AddItem(new GUIContent(attribute != null ? attribute.Path : component.Name), false,
                    AddComponentCallback, component);
            }
            menu.ShowAsContext();
        }
        
        #endregion

        #region Callbacks

        public override void OnTabOpened()
        {
            base.OnTabOpened();

            if (SceneManager.GetActiveScene().name != Constants.ElementsEditorScene) {
                EditorSceneManager.OpenScene($"{Constants.MMKMatch3Root}Scenes/{Constants.ElementsEditorScene}.unity");
            }
        }

        public override void OnTabClosed()
        {
            base.OnTabClosed();
            Unload();
        }

        public void Create (ElementType type)
        {
            this.type = type;
            Unload();

            var path = Path.Combine("Prefabs", Name);
            var template = Object.Instantiate(Resources.Load<GameObject>(path));
            template.name = Name;

            SceneView.lastActiveSceneView?.AlignViewToObject(template.transform);

            Selection.activeGameObject = template;
            currentToken = template;
        }

        public bool Save()
        {
            if (currentToken.name.Equals(Name)) {
                Debug.LogError("Please, specify the name of the element");
                return false;
            }

            if (PrefabUtility.GetPrefabParent(currentToken)) {
                PrefabUtility.ReplacePrefab(currentToken, PrefabUtility.GetPrefabParent(currentToken),
                    ReplacePrefabOptions.ConnectToPrefab);
            } else {
                var assetPath = Folder + '/' + currentToken.name + TokensExtension;
                PrefabUtility.CreatePrefab(assetPath, currentToken);
            }

            return true;
        }

        private readonly string PrefabsFolder = Constants.MMKMatch3Root + "Resources/" + Constants.PrefabsFolder;

        public void Open()
        {
            Unload();

            var path = EditorUtility.OpenFilePanel("Open token", PrefabsFolder, "");
            path = ValidatePath(path);

            if (string.IsNullOrEmpty(path)) {
                return;
            }

            if (path.Contains(BackgroundFolder)) {
                type = ElementType.Background;
            } else if (path.Contains(HolesFolder)) {
                type = ElementType.Hole;
            } else if (path.Contains(CellsFolder)) {
                type = ElementType.Cell;
            } else if (path.Contains(TokensResourcesFolder)) {
                type = ElementType.Token;
            } else if (path.Contains(AttachmentsFolder)) {
                type = ElementType.Attachment;
            } else {
                throw new UnityException("You've selected wrong file");
            }

            var asset = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            currentToken = Object.Instantiate(asset);
            currentToken.name = asset.name;

            Selection.activeGameObject = currentToken;
            SceneView.lastActiveSceneView?.AlignViewToObject(currentToken.transform);
        }

        private void AddComponentCallback (object arg)
        {
            var type = (Type) arg;
            if (type == null) {
                return;
            }

            Undo.AddComponent(currentToken, type);
        }

        #endregion

        #region Initialization

        public ElementEditorTab()
        {
            LoadAssets();
            InitContents();
        }

        private void LoadAssets()
        {
            backgroundIcon = AssetDatabase.LoadAssetAtPath<Texture2D>($"{Constants.MMKMatch3Root}Sprites/Editor_sprites/Icons/ee_icon4_background.png");
            holeIcon       = AssetDatabase.LoadAssetAtPath<Texture2D>($"{Constants.MMKMatch3Root}Sprites/Editor_sprites/Icons/ee_icon5_hole.png");
            baseIcon       = AssetDatabase.LoadAssetAtPath<Texture2D>($"{Constants.MMKMatch3Root}Sprites/Editor_sprites/Icons/ee_icon3_base.png");
            tokenIcon      = AssetDatabase.LoadAssetAtPath<Texture2D>($"{Constants.MMKMatch3Root}Sprites/Editor_sprites/Icons/ee_icon2_token.png");
            attachmentIcon = AssetDatabase.LoadAssetAtPath<Texture2D>($"{Constants.MMKMatch3Root}Sprites/Editor_sprites/Icons/ee_icon1_attachment.png");
        }

        private void InitContents()
        {
            elementEditorOptions = new[] {
                new GUIContent {image = backgroundIcon},
                new GUIContent {image = holeIcon},
                new GUIContent {image = baseIcon},
                new GUIContent {image = tokenIcon},
                new GUIContent {image = attachmentIcon},
            };

            newBackgroundButtonContent = new GUIContent {text = "Background", image = backgroundIcon };
            newHoleButtonContent = new GUIContent {text = "Hole", image = holeIcon };
            newBaseButtonContent = new GUIContent {text = "Base", image = baseIcon };
            newTokenButtonContent = new GUIContent {text = "Token", image = tokenIcon };
            newAttachmentButtonContent = new GUIContent {text = "Attachment", image = attachmentIcon };
        }

        #endregion

        #region Utilites

        private string ValidatePath(string path)
        {
            var pattern = $@"({Constants.MMKMatch3Root}Resources/Prefabs/[a-zA-Z]+/[a-zA-Z\s]+\.prefab)$";
            var matches = Regex.Matches(path, pattern);
            if (matches.Count == 0) {
                return string.Empty;
            }

            var matchGroups = matches[0].Groups;
            return matchGroups.Count == 0 ? string.Empty : matchGroups[1].Value;
        }

        private void Unload()
        {
            Object.DestroyImmediate(currentToken, false);
        }

        public void ChangeType(ElementType switchTo)
        {
            var option = EditorUtility.DisplayDialogComplex("Do you want to save your current token?",
                "If you don't save, current token won't exist anymore", "Save", "Cancel", "Don't save");

            switch (option) {
                case 0:
                    if (Save()) {
                        Create(switchTo);
                    }
                    break;
                case 1:
                    break;
                case 2:
                    Create(switchTo);
                    break;
            }
        }

        public void Reset()
        {
            foreach (var component in currentToken.GetComponents<ElementComponent>()) {
                Object.DestroyImmediate(component);
            }
        }

        #endregion
    }
}