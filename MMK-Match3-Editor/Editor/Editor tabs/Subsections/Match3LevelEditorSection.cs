﻿using System.Collections.Generic;
using MMK.Core.Editor;
using UnityEditor;
using UnityEngine;

namespace MMK.Match3.Editor
{
    public abstract class Match3LevelEditorSection : EditorSubsection
    {
        protected Match3LevelEditorTab LE => ParentTab as Match3LevelEditorTab;
        protected Match3PremadeLevel currentLevel => LE.CurrentLevel<Match3PremadeLevel>();
        protected List<Match3PremadeLevel> premadeLevels
        {
            get { return LE.Levels<Match3PremadeLevel>(); }
            set
            {
                if (value != null) {
                    LE.SetLevels(value);
                }
            }
        }

        protected LevelGrid Grid => LE.Grid;

        public override void Draw()
        {
            if (LE == null) {
                DrawWarning();
            } else {
                DrawBody();
            }
        }

        private void DrawWarning()
        {
            EditorGUILayout.HelpBox(
                "This section is only available withing Level Editor tab!",
                MessageType.Warning,
                true);
        }

        protected Match3LevelEditorSection (EditorTab le)
        {
            ParentTab = le;
        }

        protected abstract void DrawBody();
    }
}