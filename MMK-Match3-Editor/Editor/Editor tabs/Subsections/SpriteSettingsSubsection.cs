﻿using MMK.Core.Editor;

namespace MMK.Match3.Editor
{
    public class SpriteSettingsSubsection : SettingsSection<SpriteSettings>
    {
        public SpriteSettingsSubsection(string settingsAssetName) : base(settingsAssetName)
        {
        }
    }
}