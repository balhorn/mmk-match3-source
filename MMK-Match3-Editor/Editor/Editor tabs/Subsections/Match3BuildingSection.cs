﻿
using System;
using System.Collections.Generic;
using System.Linq;
using MMK.Core.Editor;
using UnityEditor;
using UnityEngine;

namespace MMK.Match3.Editor
{
    public class Match3BuildingSection : Match3LevelEditorSection
    {
        private GameObject[]   backround;
        private GameObject[]   holes;
        private GameObject[]   tokens;
        private GameObject[]   cells;
        private GameObject[]   attachments;

        #region Drawing

        protected override void DrawBody()
        {
            DrawToolbar();
        }

        private const float Size = 64f;

        private Vector2 scrollPosition;

        private void DrawToolbar()
        {
            GUILayout.Space(EditorUtilites.CalculateSize(25f));

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            DrawToolset(backround, "Background", GridLayer.Background);
            DrawToolset(holes, "Holes", GridLayer.Background);
            DrawToolset(cells, "Base", GridLayer.Base);
            DrawToolset(tokens, "Tokens", GridLayer.Tokens);
            DrawToolset(attachments, "Attachments", GridLayer.Attachments);

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Erase", GUILayout.Height(50f))) {
                buttonPressed = ButtonPressedType.Erase;
            }

            if (GUILayout.Button("Set spawn positions", GUILayout.Height(50f))) {
                buttonPressed = ButtonPressedType.SpawnPositionButton;
            }
            EditorGUILayout.EndHorizontal();

            if (currentLevel.type == LevelType.CollectIngridients) {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Ingridient spawners", GUILayout.Height(50f))) {
                    buttonPressed = ButtonPressedType.IngridientSpawner;
                }

                if (GUILayout.Button("Ingridient despawners", GUILayout.Height(50f))) {
                    buttonPressed = ButtonPressedType.DespawnPositionButton;
                }
                EditorGUILayout.EndHorizontal();
            }

            if (buttonPressed != ButtonPressedType.None) {
                LE.HelpMessage = new GUIContent {
                    text = "<b>Build Mode</b>\nRMB to return to default Unity controls",
                    image = EditorUtilites.GetTexture(selectedSprite)
                };
            } else {
                LE.HelpMessage = new GUIContent {
                    text = "",
                    image = null
                };
            }

            EditorGUILayout.EndScrollView();
        }

        private const int MaxButtonsInARow = 5;

        private void DrawToolset(GameObject[] set, string label, GridLayer type)
        {
            EditorGUILayout.BeginVertical(MMKStyles.buildModeOffset);
            var enabled = EditorGUILayout.ToggleLeft(label, Grid.GetLayerState(type), EditorStyles.boldLabel);
            GUILayout.Space(EditorUtilites.CalculateSize(18f));
            Grid.SwitchLayer(type, enabled);

            EditorGUILayout.BeginHorizontal();
            for (int index = 0; index < set.Length; index++) {
                var graphics = set[index].GetComponent<TokenComponents.Graphics>();
                var sprite = graphics?.EditorSprite;

                var texture = EditorUtilites.GetTexture(sprite);

                if (index != 0 && index % MaxButtonsInARow == 0) {
                    EditorGUILayout.EndHorizontal();
                    GUILayout.Space(EditorUtilites.CalculateSize(12f));
                    EditorGUILayout.BeginHorizontal();
                }

                var active = selectedObject == set[index] && buttonPressed != ButtonPressedType.None;
                var switched = GUILayout.Toggle(active, new GUIContent(texture, set[index].name),
                    GUI.skin.button, EditorUtilites.Width(Size), EditorUtilites.Height(Size));

                GUILayout.Space(EditorUtilites.CalculateSize(12f));

                if (!active && switched) {
                    buttonPressed = set.Equals(holes) ? ButtonPressedType.Hole : ButtonPressedType.ToolButton;
                    selectedObject = set[index];
                    selectedSprite = graphics?.EditorSprite;
                    color = set[index].GetColor();
                    layer = type;
                } else if (active && !switched) {
                    buttonPressed = ButtonPressedType.None;
                }
            }

            GUILayout.Space(EditorUtilites.CalculateSize(30f));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        #endregion

        #region Objects editing

        private Sprite            selectedSprite;
        private GameObject        selectedObject;
        private GridLayer         layer;
        private TokenColor        color;
        private ButtonPressedType buttonPressed;

        private void OnSceneGUI (SceneView sceneView)
        {
            if (buttonPressed == ButtonPressedType.None) {
                return;
            }

            var e = Event.current;
            int controlId = GUIUtility.GetControlID(FocusType.Passive);

            // Start treating your events
            switch (e.type) {
                case EventType.MouseDown:
                    var origin = HandleUtility.GUIPointToWorldRay(e.mousePosition);
                    GUIUtility.hotControl = controlId;
                    ResolveMouseDrag(origin);
                    e.Use();
                    break;
                case EventType.MouseUp:
                    GUIUtility.hotControl = 0;
                    if (e.button == 1) {
                        buttonPressed = ButtonPressedType.None;
                        MMKWindow.Instance.Repaint();
                    }
                    e.Use();
                    break;
                case EventType.MouseDrag:
                    origin = HandleUtility.GUIPointToWorldRay(e.mousePosition);
                    GUIUtility.hotControl = controlId;
                    ResolveMouseDrag(origin);
                    e.Use();

                    break;

            }
        }

        private void ResolveMouseDrag(Ray ray)
        {
            RaycastHit2D[] hits = Physics2D.RaycastAll(ray.origin, ray.direction, 0f);
            var gridElements = hits
                .Where(hit => hit.collider != null)
                .Select(hit => hit.collider.GetComponent<LevelGridElement>());

            switch (buttonPressed) {
                case ButtonPressedType.ToolButton:
                    SetSprite(gridElements);
                    break;
                case ButtonPressedType.Erase:
                    Grid.Erase(gridElements.ToList());
                    break;
                case ButtonPressedType.SpawnPositionButton:
                    SetSpawnPositions(gridElements);
                    break;
                case ButtonPressedType.Hole:
                    Grid.EraseAll(ray.origin);
                    SetSprite(gridElements, false);
                    break;
                case ButtonPressedType.DespawnPositionButton:
                    SetDespawnPositions(gridElements);
                    break;
                case ButtonPressedType.IngridientSpawner:
                    SetIngridientSpawners(gridElements);
                    break;
            }
        }

        private void SetIngridientSpawners(IEnumerable<LevelGridElement> gridElements)
        {
            var elementsArray = gridElements.ToArray();
            if (elementsArray.Length == 0) {
                return;
            }

            var position = elementsArray[0].transform.position;
            Grid.SetIngridientSpawner(position);
            SceneView.RepaintAll();
        }

        private void SetSpawnPositions(IEnumerable<LevelGridElement> gridElements)
        {
            var elementsArray = gridElements.ToArray();
            if (elementsArray.Length == 0) {
                return;
            }

            var position = elementsArray[0].transform.position;
            Grid.SetSpawnPosition(position);
            SceneView.RepaintAll();
        }

        private void SetDespawnPositions (IEnumerable<LevelGridElement> gridElements)
        {
            var elementsArray = gridElements.ToArray();
            if (elementsArray.Length == 0) {
                return;
            }

            var position = elementsArray[0].transform.position;
            Grid.SetDespawnPosition(position);
            SceneView.RepaintAll();
        }

        private void SetSprite(IEnumerable<LevelGridElement> gridElements, bool check = true)
        {
            try {
                var layerElement = gridElements.First(element => element.layer == layer);
                if (check && !Grid.CanDrawLayer(layerElement.transform.position, layer)) {
                    Debug.LogWarningFormat("You cannot set {0} without backround {1}", layer,
                        layer == GridLayer.Attachments ? "or token" : "");
                    return;
                }

                layerElement.Sprite = selectedSprite;
                layerElement.Prefab = selectedObject;
                layerElement.Color = color;
            } catch (InvalidOperationException) {
            }
        }

        private enum ButtonPressedType { None, ToolButton, Erase, SpawnPositionButton, Hole, DespawnPositionButton, IngridientSpawner }

        #endregion

        #region Initialization

        public Match3BuildingSection(EditorTab le) : base(le)
        {
            SceneView.onSceneGUIDelegate += OnSceneGUI;

            LoadAssets();
        }

        ~Match3BuildingSection()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
        }

        private void LoadAssets()
        {
            premadeLevels = Resources.LoadAll<Match3PremadeLevel>(Constants.LevelsFolder)
                .OrderBy(lvl => lvl.number)
                .ToList();

            backround   = Resources.LoadAll<GameObject>(Constants.BackgroundFolder);
            holes       = Resources.LoadAll<GameObject>(Constants.HolesForlder);
            cells       = Resources.LoadAll<GameObject>(Constants.CellsFolder);
            tokens      = Resources.LoadAll<GameObject>(Constants.ElementsFolder);
            attachments = Resources.LoadAll<GameObject>(Constants.AttachmentsFolder);
        }

        #endregion
    }
}