﻿using MMK.Core.Editor;

namespace MMK.Match3.Editor
{
    public class TextSettingsSubsection : SettingsSection<TextSettings>
    {
        public TextSettingsSubsection(string settingsAssetName) : base(settingsAssetName)
        {
        }
    }
}