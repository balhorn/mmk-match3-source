﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Core.Aims;
using MMK.Core.Boosters;
using MMK.Core.Editor;
using MMK.Match3.Aims;
using MMK.Match3.Preboosters;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace MMK.Match3.Editor
{
    public class Match3LevelEditorProperties : Match3LevelEditorSection
    {
        private int selectedConstraint;

        private string[] constraintNames;
        private string[] boosterNames;
        private string[] aimNames;
        private string[] preboosterNames;

        private Type[] aimTypes;

        private List<int> selectedBoosters;
        private List<int> selectedAims;
        private List<int> selectedPreboosters;

        private ReorderableList boostersList;
        private ReorderableList aimsList;
        private ReorderableList preboostersList;
        private ReorderableList ingridientList;

        private Match3Settings settings;

        #region Drawing

        protected override void DrawBody()
        {
            EditorGUI.BeginChangeCheck();

            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
            LE.previousLevelNumber = EditorGUILayout.Popup(new GUIContent("Set after"), LE.previousLevelNumber, LE.LevelNames, MMKStyles.popup);

            GUILayout.Space(EditorUtilites.CalculateSize(22f));

            EditorGUI.BeginChangeCheck();
            currentLevel.chessBackground = EditorGUILayout.Toggle("Chess background", currentLevel.chessBackground);
            if (EditorGUI.EndChangeCheck()) {
                Grid.ChessBackground = currentLevel.chessBackground;
            }
            //GUI.enabled = true;
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
            EditorUtilites.LabelFitContent("Size", MMKStyles.boldLabel);

            GUILayout.Space(EditorUtilites.CalculateSize(17f));

            EditorGUI.BeginChangeCheck();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Rows: ", MMKStyles.label);
            currentLevel.size.x = EditorGUILayout.IntSlider((int) currentLevel.size.x, 0, Constants.MaximumRows);
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(EditorUtilites.CalculateSize(22f));

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Columns: ", MMKStyles.label);
            currentLevel.size.y = EditorGUILayout.IntSlider((int) currentLevel.size.y, 0, Constants.MaximumColumns);
            EditorGUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck()) {
                Grid.Update();
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
            EditorUtilites.LabelFitContent("Constraint", MMKStyles.boldLabel);

            GUILayout.Space(EditorUtilites.CalculateSize(17f));

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Type", MMKStyles.label);
            selectedConstraint = EditorGUILayout.Popup(selectedConstraint,
                constraintNames.Select(name => Core.Utilites.SplitCamelCase(name)).ToArray(), MMKStyles.popup, EditorUtilites.Height(28f));
            currentLevel.constraint = constraintNames[selectedConstraint];
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(EditorUtilites.CalculateSize(22f));

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Amount", MMKStyles.label);
            currentLevel.constraintAmount = EditorGUILayout.IntField(currentLevel.constraintAmount, MMKStyles.centeredIntField,
                EditorUtilites.Height(28f), EditorUtilites.Width(42f));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
            EditorUtilites.LabelFitContent("Score goals", MMKStyles.boldLabel);

            GUILayout.Space(EditorUtilites.CalculateSize(20f));

            DrawMilestones();
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
            EditorUtilites.LabelFitContent("Available boosters", MMKStyles.boldLabel);
            GUILayout.Space(EditorUtilites.CalculateSize(17f));
            boostersList.DoLayoutList();
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
            EditorUtilites.LabelFitContent("Preboosters", MMKStyles.boldLabel);
            GUILayout.Space(EditorUtilites.CalculateSize(17f));
            preboostersList.DoLayoutList();
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
            EditorUtilites.LabelFitContent("Level aims", MMKStyles.boldLabel);
            GUILayout.Space(EditorUtilites.CalculateSize(17f));
            aimsList.DoLayoutList();
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
            EditorUtilites.LabelFitContent("Tutorial", MMKStyles.boldLabel);
            GUILayout.Space(EditorUtilites.CalculateSize(17f));
            currentLevel.tutorialAnimator =
                (RuntimeAnimatorController)EditorGUILayout.ObjectField(currentLevel.tutorialAnimator,
                    typeof(RuntimeAnimatorController), false);
            EditorGUILayout.EndVertical();

            if (currentLevel.type == LevelType.CollectIngridients) {
                DrawIngridientsSettings();
            }

            if (EditorGUI.EndChangeCheck()) {
                LE.LevelChanged = true;
            }
        }

        private void DrawIngridientsSettings()
        {
            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
            EditorUtilites.LabelFitContent("Ingridient settings", MMKStyles.boldLabel);

            GUILayout.Space(EditorUtilites.CalculateSize(17f));

            ingridientList?.DoLayoutList();
            EditorGUILayout.EndVertical();
        }

        private void DrawMilestones()
        {
            for (int i = 0; i < settings.maximumScoreMilestones; i++) {
                string suffix;
                switch (i + 1) {
                    case 1:
                        suffix = "st";
                        break;
                    case 2:
                        suffix = "nd";
                        break;
                    case 3:
                        suffix = "rd";
                        break;
                    default:
                        suffix = "th";
                        break;
                }
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel($"{i + 1} {suffix}", MMKStyles.label);
                currentLevel.progressValues[i] = EditorGUILayout.IntField(currentLevel.progressValues[i], MMKStyles.centeredIntField,
                    EditorUtilites.Height(28f), EditorUtilites.Width(62f));
                GUILayout.Space(EditorUtilites.CalculateSize(30f));
                EditorGUILayout.EndHorizontal();
            }
        }

        #endregion

        #region Initialization

        public Match3LevelEditorProperties(EditorTab le) : base(le)
        {
            TabContent.text = "Properties";

            InitFields();
            InitLists();
            InitIndicies();
        }

        private void InitLists()
        {
            if (currentLevel == null) {
                return;
            }

            boostersList = new ReorderableList(currentLevel.boosters, typeof(string), false, false, true, true);
            boostersList.elementHeight = 28f;
            boostersList.onAddCallback += OnAddBooster;
            boostersList.onRemoveCallback += OnRemoveBooster;
            boostersList.drawElementCallback += OnDrawBoosterElement;

            aimsList = new ReorderableList(currentLevel.aims, typeof(LevelAimInfo), false, true, true, true);
            aimsList.elementHeight = 28f;
            aimsList.headerHeight = 18f;
            aimsList.onAddCallback += OnAddAim;
            aimsList.onRemoveCallback += OnRemoveAim;
            aimsList.drawElementCallback += OnDrawAimElement;
            aimsList.drawHeaderCallback += OnDrawAimsHeader;

            preboostersList = new ReorderableList(currentLevel.preboosters, typeof(BoosterInfo), false, false, true, true);
            preboostersList.elementHeight = 28f;
            preboostersList.onAddCallback += OnAddPrebooster;
            preboostersList.onRemoveCallback += OnRemovePrebooster;
            preboostersList.drawElementCallback += OnDrawPreboosterElement;

            ingridientList = new ReorderableList(currentLevel.ingridients, typeof(IngridientAimInfo.IngridientItem), true,
                true, true, true);
            ingridientList.elementHeight = 28f;
            ingridientList.headerHeight = 18f;
            ingridientList.onAddCallback += OnAddIngridient;
            ingridientList.onRemoveCallback += OnRemoveIngridient;
            ingridientList.drawElementCallback += OnDrawIngridient;
            ingridientList.drawHeaderCallback += DrawIngridientHeader;

            LE.RefreshLevelNames();
            LE.previousLevelNumber = currentLevel.number - 1;
        }

        private void InitFields()
        {
            settings = Resources.Load<Match3Settings>("GameSettings");

            boosterNames    = EditorUtilites.GetTypeNames(typeof(AbstractBooster));
            preboosterNames = EditorUtilites.GetTypeNames(typeof(Prebooster));
            constraintNames = EditorUtilites.GetTypeNames(typeof (Constraint));
            aimNames        = EditorUtilites.GetTypeNames(typeof (LevelAim));

            aimTypes = EditorUtilites.GetDerivedTypes(typeof(LevelAim)).ToArray();

            if (boostersList == null) {
                boostersList = new ReorderableList(new List<string>(), typeof(string));
            }

            if (aimsList == null) {
                aimsList = new ReorderableList(new List<LevelAimInfo>(), typeof(LevelAimInfo));
            }

            if (preboostersList == null) {
                preboostersList = new ReorderableList(new List<BoosterInfo>(), typeof(BoosterInfo));
            }

            if (ingridientList == null) {
                ingridientList = new ReorderableList(new List<IngridientAimInfo.IngridientItem>(),
                    typeof (IngridientAimInfo.IngridientItem));
            }
        }

        private void InitIndicies()
        {
            selectedBoosters = new List<int>();
            selectedAims = new List<int>();
            selectedPreboosters = new List<int>();

            selectedConstraint = constraintNames.ToList().IndexOf(currentLevel.constraint);
            selectedConstraint = selectedConstraint < 0 ? 0 : selectedConstraint;

            var boosterNamesList = boosterNames.ToList();
            foreach (var booster in currentLevel.boosters) {
                selectedBoosters.Add(boosterNamesList.IndexOf(booster));
            }

            var aimNamesList = aimNames.ToList();
            foreach (var aim in currentLevel.aims) {
                selectedAims.Add(aimNamesList.IndexOf(aim.name));
            }

            var preboosters = preboosterNames.ToList();
            foreach (var prebooster in currentLevel.preboosters) {
                selectedPreboosters.Add(preboosters.IndexOf(prebooster.name));
            }
        }

        #endregion

        #region Callbacks

        private void DrawIngridientHeader (Rect rect)
        {
            var prefabRect = new Rect(rect.x + rect.width / 4, rect.y, rect.width / 2f - 20f, rect.height);
            EditorGUI.LabelField(prefabRect, new GUIContent("Prefab"), MMKStyles.label);
            EditorGUI.LabelField(new Rect(prefabRect.x + prefabRect.width + 20f, rect.y, rect.width - prefabRect.width - 60f, rect.height), new GUIContent("Score"),
                MMKStyles.                label);
        }

        private void OnDrawIngridient (Rect rect, int index, bool isActive, bool isFocused)
        {
            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 4, rect.height), (index + 1).ToString(), MMKStyles.listLabel);
            currentLevel.ingridients[index].prefab = EditorGUI.ObjectField(
                new Rect(rect.x + rect.width / 4, rect.y, rect.width / 2, rect.height), currentLevel.ingridients[index].prefab,
                typeof (GameObject), false) as GameObject;
            currentLevel.ingridients[index].score = EditorGUI.IntField(new Rect(rect.x + 3 * rect.width / 4, rect.y, rect.width / 4, 26f),
                currentLevel.ingridients[index].score, MMKStyles.centeredIntField);
        }

        private void OnRemoveIngridient (ReorderableList list)
        {
            currentLevel.ingridients.RemoveAt(list.index);
        }

        private void OnAddIngridient (ReorderableList list)
        {
            currentLevel.ingridients.Add(new IngridientAimInfo.IngridientItem(null, 0));
        }

        private void OnRemoveBooster(ReorderableList list)
        {
            selectedBoosters.RemoveAt(list.index);
            currentLevel.boosters.RemoveAt(list.index);
        }

        private void OnAddAim(ReorderableList list)
        {
            selectedAims.Add(0);
            currentLevel.aims.Add(new LevelAimInfo(aimTypes[0], 0));
        }

        private void OnRemoveAim(ReorderableList list)
        {
            selectedAims.RemoveAt(list.index);

            if (currentLevel.aims[list.index].name == typeof (IngridientAim).Name) {
                currentLevel.type = LevelType.Score;
                currentLevel.ingridients.Clear();
            }

            currentLevel.aims.RemoveAt(list.index);
        }

        private void OnAddBooster(ReorderableList list)
        {
            if (list.count < settings.maximumBoostersAmount) {
                selectedBoosters.Add(0);
                currentLevel.boosters.Add(boosterNames[selectedBoosters.Last()]);
            }
        }

        private void OnDrawAimsHeader(Rect rect)
        {
            var typeRect = new Rect(rect.x + rect.width / 4, rect.y, rect.width / 2f - 20f, rect.height);
            EditorGUI.LabelField(typeRect, "Type", MMKStyles.label);
            EditorGUI.LabelField(new Rect(typeRect.x + typeRect.width + 20f, rect.y, rect.width - typeRect.width - 60f, rect.height), "Amount", MMKStyles.label);
        }

        private void OnDrawBoosterElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            try {
                EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 6, rect.height), $"{index + 1}.", MMKStyles.listLabel);
                var popUpRect = new Rect(rect.x + rect.width / 6, rect.y, 5 * rect.width / 6 , rect.height);
                selectedBoosters[index] = EditorGUI.Popup(popUpRect, selectedBoosters[index],
                    boosterNames.Select(name => Core.Utilites.SplitCamelCase(name)).ToArray(), MMKStyles.popup);
                currentLevel.boosters[index] = boosterNames[selectedBoosters[index]];
            } catch (IndexOutOfRangeException) {
                boostersList.list.RemoveAt(index);
            }
        }

        private void OnDrawAimElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            try {
                EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 4, rect.height), $"{index + 1}.", MMKStyles.listLabel);
                var popUpRect = new Rect(rect.x + rect.width / 4, rect.y, rect.width / 2, rect.height);
                selectedAims[index] = EditorGUI.Popup(popUpRect, selectedAims[index], aimNames.Select(name => Core.Utilites.SplitCamelCase(name)).ToArray(), MMKStyles.popup);

                if (aimNames[selectedAims[index]] == typeof (IngridientAim).Name) {
                    currentLevel.type = LevelType.CollectIngridients;
                } else {
                    currentLevel.aims[index].amount = EditorGUI.IntField(new Rect(rect.x + 3 * rect.width / 4, rect.y, rect.width / 4, EditorUtilites.CalculateSize(26f)), currentLevel.aims[index].amount, MMKStyles.centeredIntField);
                    currentLevel.type = LevelType.Score;
                    currentLevel.ingridients.Clear();
                }

                currentLevel.aims[index].Type = aimTypes[selectedAims[index]];
            } catch (IndexOutOfRangeException) {
                aimsList.list.RemoveAt(index);
            }
        }

        private void OnAddPrebooster (ReorderableList list)
        {
            if (list.count < Constants.MaximumPreboostersPerLevel) {
                selectedPreboosters.Add(0);
                currentLevel.preboosters.Add(new BoosterInfo());
            }
        }

        private void OnRemovePrebooster (ReorderableList list)
        {
            selectedPreboosters.RemoveAt(list.index);
            currentLevel.preboosters.RemoveAt(list.index);
        }

        private void OnDrawPreboostersHeader (Rect rect)
        {
            OnDrawAimsHeader(rect);
        }

        private void OnDrawPreboosterElement (Rect rect, int index, bool isActive, bool isFocused)
        {
            try {
                EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 6, rect.height), $"{index + 1}.", MMKStyles.listLabel);
                var popUpRect = new Rect(rect.x + rect.width / 6, rect.y, 5 * rect.width / 6, rect.height);
                selectedPreboosters[index] = EditorGUI.Popup(popUpRect, selectedPreboosters[index],
                    preboosterNames.Select(name => Core.Utilites.SplitCamelCase(name)).ToArray(), MMKStyles.popup);
                currentLevel.preboosters[index].name = preboosterNames[selectedPreboosters[index]];
            } catch (IndexOutOfRangeException) {
                preboostersList.list.RemoveAt(index);
            }
        }

        #endregion
    }
}