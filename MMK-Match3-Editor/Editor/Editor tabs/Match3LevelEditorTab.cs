﻿using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Core.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MMK.Match3.Editor
{
    public class Match3LevelEditorTab : LevelEditorTab
    {
        public static int lastEditedLevel;

        public new int previousLevelNumber
        {
            get { return base.previousLevelNumber; }
            set { base.previousLevelNumber = value; }
        }
        public GUIContent[] LevelNames => levelNames;

        private Match3PremadeLevel currentLevel => CurrentLevel<Match3PremadeLevel>();
        private List<Match3PremadeLevel> premadeLevels => Levels<Match3PremadeLevel>();
        private LevelGrid grid;

        protected override string LevelsFolder => Constants.LevelsFolderPath;

        public LevelGrid Grid
        {
            get
            {
                if (grid != null) {
                    return grid;
                }

                if (EditorApplication.isPlayingOrWillChangePlaymode) {
                    return null;
                }

                var gridObject = GameObject.Find("Grid") ?? new GameObject("Grid", typeof(LevelGrid));
                grid = gridObject.GetComponent<LevelGrid>();

                return grid;
            }
        }

        public override void OnTabOpened()
        {
            base.OnTabOpened();

            if (SceneManager.GetActiveScene().name != Core.Constants.MainGameScene) {
                var levelNumber = currentLevel.number;
                EditorSceneManager.OpenScene($"{Constants.MMKMatch3Root}Scenes/{Core.Constants.MainGameScene}.unity");
                LoadPremadeLevel(levelNumber);
            }

            Grid.CurrentLevel = currentLevel;
        }

        public Match3LevelEditorTab()
        {
            AddSubsection(new Match3LevelEditorProperties(this));
            AddSubsection(new Match3BuildingSection(this));
            AddSubsection<Match3AssetBundlesSection>();

            subsectionsContents = contentsList.ToArray();

            EditorApplication.playmodeStateChanged -= EditorUpdate;
            EditorApplication.playmodeStateChanged += EditorUpdate;
        }

        private static void EditorUpdate ()
        {
            var instance = MMKWindow.FindTab<Match3LevelEditorTab>();

            if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying) {
                lastEditedLevel = instance.currentLevel.number;
            } else if (!EditorApplication.isPlaying) {
                Object.DestroyImmediate(Level.Current.gameObject);
                MMKWindow.OpenWindow();
                instance.Grid.CurrentLevel = instance.currentLevel;
            }
        }

        protected override void SaveLevel()
        {
            Grid.SaveLevel();
            base.SaveLevel();
        }

        protected override PremadeLevel CreateNewLevel()
        {
            return ScriptableObject.CreateInstance<Match3PremadeLevel>();
        }
    }
}