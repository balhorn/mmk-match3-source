﻿using MMK.Core.Editor;
using UnityEditor;

namespace MMK.Match3.Editor
{
    public static class Match3EditorUtilites
    {
        [MenuItem("Open scene/Game")]
        public static void OpenGameScene ()
        {
            EditorsMenu.OpenScene(Core.Constants.MainGameScene, Constants.MMKMatch3Root);
        }
    }
}
