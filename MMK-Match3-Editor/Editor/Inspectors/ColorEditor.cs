﻿using UnityEditor;
using Color = MMK.Match3.TokenComponents.Color;

namespace MMK.Match3.Editor
{
    [CustomEditor(typeof (Color))]
    public class ColorEditor : GraphicsHandlerEditor
    {

    }
}
