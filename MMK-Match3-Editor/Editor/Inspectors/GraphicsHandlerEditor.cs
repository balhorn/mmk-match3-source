﻿using MMK.Match3.Editor;
using UnityEditor;
using UnityEngine;

namespace MMK.Match3.Editor
{
    public class  GraphicsHandlerEditor : UnityEditor.Editor
    {
        private IGraphicsHandler handler;

        public override void OnInspectorGUI ()
        {
            handler = (IGraphicsHandler) target;

            DrawDefaultInspector();

            DrawGraphicsButtons();

            DrawAnimationButtons();

        }

        protected virtual void DrawAnimationButtons ()
        {
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Add animation")) {
                    var window = EditorWindow.GetWindow<AddAnimationWindow>();
                    window.Handler = handler;
                }

                if (GUILayout.Button("Remove animation")) {
                    DestroyImmediate(handler.Animation);
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        protected virtual void DrawGraphicsButtons ()
        {
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Add graphics")) {
                    var window = EditorWindow.GetWindow<AddGraphicsWindow>();
                    window.Handler = handler;
                }

                if (GUILayout.Button("Remove graphics")) {
                    DestroyImmediate(handler.Graphics);
                }
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}
