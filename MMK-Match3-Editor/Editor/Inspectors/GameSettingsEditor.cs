﻿using System;
using System.Linq;
using MMK.Core.Boosters;
using MMK.Core.Editor;
using UnityEditor;

namespace MMK.Match3.Editor
{
    [CustomEditor(typeof(Match3Settings))]
    public class GameSettingsEditor : UnityEditor.Editor
    {
        private Match3Settings instance;
        private Type[] boosterTypes;
        private Type[] preboosterTypes;

        private void OnEnable ()
        {
            instance = (Match3Settings) target;
            boosterTypes = EditorUtilites.GetDerivedTypes(typeof (AbstractBooster));
            preboosterTypes = EditorUtilites.GetDerivedTypes(typeof (MMK.Match3.Preboosters.Prebooster));
        }

        public override void OnInspectorGUI()
        {
            #region Game mode constraints

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Game mode contsraints");
            instance.maximumBoostersAmount = EditorUtilites.IntField("Maximum booster amount",
                instance.maximumBoostersAmount);
            instance.maximumScoreMilestones = EditorUtilites.IntField("Maximum score milestones",
                instance.maximumScoreMilestones);
            instance.maximumLifes = EditorUtilites.IntField("Maximum lifes",
                instance.maximumLifes);

            EditorUtilites.Space(15f);

            instance.maximumIceDurability = EditorUtilites.IntField("Maximum ice durability",
                instance.maximumIceDurability);
            instance.maximumStoneDurability = EditorUtilites.IntField("Maximum stone durability",
                instance.maximumStoneDurability);
            instance.maximumCellDurability = EditorUtilites.IntField("Maximum cell durability",
                instance.maximumCellDurability);

            EditorUtilites.Space(15f);

            instance.regainLifesHours = EditorUtilites.IntField("Regain life hours",
                instance.regainLifesHours);
            instance.regainLifesMinutes = EditorUtilites.IntField("Regain life minutes",
                instance.regainLifesMinutes);
            instance.regainLifesSeconds = EditorUtilites.IntField("Regain life seconds",
                instance.regainLifesSeconds);

            EditorUtilites.Space(15f);

            instance.tokenSize = EditorGUILayout.Vector2Field("Token size", instance.tokenSize);

            instance.tokensMovementSpeed = EditorUtilites.FloatField("Tokens movement speed",
                instance.tokensMovementSpeed);
            EditorUtilites.EndOffsetBlock();

            #endregion

            #region Naming

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Naming");
            instance.regularTokenName = EditorUtilites.TextField("Regular token name", instance.regularTokenName);
            instance.regularCellName = EditorUtilites.TextField("Regular cell name", instance.regularCellName);
            EditorUtilites.EndOffsetBlock();

            #endregion

            #region Prefabs

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Prefabs");
            instance.defaultTokenPrefab = EditorUtilites.ObjectField("Default token prefab", instance.defaultTokenPrefab);
            instance.defaultBackroundPrefab = EditorUtilites.ObjectField("Default background prefab", instance.defaultBackroundPrefab);
            instance.defaultHolePrefab = EditorUtilites.ObjectField("Default hole prefab", instance.defaultHolePrefab);
            EditorUtilites.EndOffsetBlock();

            #endregion

            #region MapSettings

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Map settings");
            instance.levelsPerLocation = EditorUtilites.IntField("Levels per location", instance.levelsPerLocation);
            instance.fogOffset = EditorUtilites.IntField("Fog offset", instance.fogOffset);
            EditorUtilites.EndOffsetBlock();
            #endregion

            #region Score settings

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Score settings");
            instance.tokenScore = EditorUtilites.IntField("Token score", instance.tokenScore);

            EditorUtilites.Space(15f);

            instance.matchThreeScore = EditorUtilites.IntField("Match three score", instance.matchThreeScore);
            instance.matchFourScore = EditorUtilites.IntField("Match four score", instance.matchFourScore);
            instance.matchFiveScore = EditorUtilites.IntField("Match five score", instance.matchFiveScore);
            instance.matchCornerScore = EditorUtilites.IntField("Match corner score", instance.matchCornerScore);
            instance.matchTScore = EditorUtilites.IntField("Match T score", instance.matchTScore);

            EditorUtilites.Space(15f);

            instance.bombExplosionScore = EditorUtilites.IntField("Bomb explosion score", instance.bombExplosionScore);

            EditorUtilites.Space(15f);

            instance.iceScore = EditorUtilites.IntField("Ice score", instance.iceScore);
            instance.cookieScore = EditorUtilites.IntField("Cookie score", instance.cookieScore);
            instance.stoneScore = EditorUtilites.IntField("Stone score", instance.stoneScore);
            instance.chocolateScore = EditorUtilites.IntField("Chocolate score", instance.chocolateScore);
            instance.ingridientScore = EditorUtilites.IntField("Ingridient score", instance.ingridientScore);

            EditorUtilites.EndOffsetBlock();

            #endregion

            #region Audio settings

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Audio settings");
            instance.backgroundMusic = EditorUtilites.ObjectField("Background music", instance.backgroundMusic);
            instance.levelStartSound = EditorUtilites.ObjectField("Level start sound", instance.levelStartSound);
            instance.winSound = EditorUtilites.ObjectField("Win sound", instance.winSound);
            instance.matchSound = EditorUtilites.ObjectField("Match sound", instance.matchSound);
            instance.furtherMatchSound = EditorUtilites.ObjectField("Further match sound", instance.furtherMatchSound);
            instance.shuffleSound = EditorUtilites.ObjectField("Shuffle sound", instance.shuffleSound);
            instance.tokenLandingSound = EditorUtilites.ObjectField("Token landing sound", instance.tokenLandingSound);
            instance.buttonTapSound = EditorUtilites.ObjectField("Button tap sound", instance.buttonTapSound);
            EditorUtilites.EndOffsetBlock();

            #endregion

            #region Shop settings

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Shop settings");
            instance.additionalTurnCost = EditorUtilites.IntField("Additional turn cost", instance.additionalTurnCost);
            instance.baseCostStep = EditorUtilites.IntField("Base cost step", instance.baseCostStep);
            instance.incrementStepBy = EditorUtilites.IntField("Increment step by", instance.incrementStepBy);
            instance.additionalTurnsAmount = EditorUtilites.IntField("Additional turns amount", instance.additionalTurnsAmount);
            EditorUtilites.EndOffsetBlock();

            #endregion
        }
    }
}