﻿using System.Collections;
using MMK.Match3;
using MMK.Match3.TokenComponents;
using UnityEditor;
using UnityEngine;

namespace MMK.Match3.Editor
{
    [CustomEditor(typeof (Durability), true)]
    public class DurabilityEditor : GraphicsHandlerEditor
    {
    
    }

    [CustomEditor(typeof (Active), true)]
    public class ActiveEditor : GraphicsHandlerEditor
    {

    }

    [CustomEditor(typeof (Ingridient), true)]
    public class IngridientEditor : GraphicsHandlerEditor
    {

    }
}