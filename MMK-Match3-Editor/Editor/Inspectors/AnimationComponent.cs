﻿using UnityEditor;
using Animation = MMK.Match3.TokenComponents.Animation;

namespace MMK.Match3.Editor
{
    [CustomEditor(typeof (Animation))]
    public class AnimationComponent : UnityEditor.Editor
    {
        private Animation animation;

        public override void OnInspectorGUI()
        {
            animation = (Animation)target;

            DrawDefaultInspector();
        }
    }
}
