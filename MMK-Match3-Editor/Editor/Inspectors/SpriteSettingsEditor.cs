﻿using System;
using MMK.Core.Aims;
using MMK.Core.Boosters;
using MMK.Core.Editor;
using MMK.Match3;
using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEditorInternal;

namespace MMK.Match3.Editor
{
    [CustomEditor(typeof (SpriteSettings))]
    public class SpriteSettingsEditor : UnityEditor.Editor
    {
        private string[] boosterTypes;
        private string[] aimTypes;

        private string[] BoosterTypes
        {
            get
            {
                if (boosterTypes != null && boosterTypes.Length != 0) {
                    return boosterTypes;
                }

                var boosterType = typeof(AbstractBooster);

                return boosterTypes = EditorUtilites.GetTypeNames(boosterType);
            }
        }

        private string[] AimTypes
        {
            get
            {
                if (aimTypes != null && aimTypes.Length != 0) {
                    return aimTypes;
                }

                return aimTypes = EditorUtilites.GetTypeNames(typeof(LevelAim));
            }
        }

        private SpriteSettings instance;
        private ReorderableList tokens;
        private ReorderableList bonuses;

        private AnimBool tokensVisible;
        private AnimBool bonusesVisible;
        private AnimBool boostersVisible;
        private AnimBool aimsVisible;

        private void OnEnable ()
        {
            instance = (SpriteSettings)target;

            tokens = new ReorderableList(instance.tokenSprites, typeof(SpriteSettings.TokenSpriteItem), false, true, true, true);
            tokens.onAddCallback += OnAddToken;
            tokens.onRemoveCallback += OnRemoveToken;
            tokens.drawElementCallback += DrawTokenElement;
            tokens.drawHeaderCallback += DrawElementHeader;
            tokens.elementHeight = 100f;

            bonuses = new ReorderableList(instance.bonusSprites, typeof(SpriteSettings.BonusSpriteItem), false, true, true, true);
            bonuses.onAddCallback += OnAddBonus;
            bonuses.onRemoveCallback += OnRemoveBonus;
            bonuses.drawElementCallback += DrawBonusElement;
            bonuses.drawHeaderCallback += DrawBonusHeader;
            bonuses.elementHeight = 100f;

            tokensVisible = new AnimBool(true);
            bonusesVisible = new AnimBool(true);
            boostersVisible = new AnimBool(true);
            aimsVisible = new AnimBool(true);

            tokensVisible.valueChanged.AddListener(Repaint);
            bonusesVisible.valueChanged.AddListener(Repaint);
            boostersVisible.valueChanged.AddListener(Repaint);
            aimsVisible.valueChanged.AddListener(Repaint);
        }

        public override void OnInspectorGUI ()
        {
            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Chess background");
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();

            instance.whiteBackground =
                EditorGUILayout.ObjectField(instance.whiteBackground, typeof(GameObject), false, GUILayout.Height(28f)) as GameObject;
            instance.blackBackground =
                EditorGUILayout.ObjectField(instance.blackBackground, typeof(GameObject), false, GUILayout.Height(28f)) as GameObject;

            EditorGUILayout.EndHorizontal();
            EditorUtilites.EndOffsetBlock();

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Despawners");

            instance.despawnerSprite =
                EditorGUILayout.ObjectField("Sprite", instance.despawnerSprite, typeof(Sprite), false) as Sprite;
            instance.despawnerOffset = EditorGUILayout.FloatField("Offset", instance.despawnerOffset, MMKStyles.centeredIntField);


            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(instance);
            }
            EditorUtilites.EndOffsetBlock();

            EditorUtilites.BeginOffsetBlock();
            tokensVisible.target = EditorGUILayout.ToggleLeft("Tokens", tokensVisible.target, EditorStyles.boldLabel);

            if (EditorGUILayout.BeginFadeGroup(tokensVisible.faded)) {
                tokens.DoLayoutList();
            }

            EditorGUILayout.EndFadeGroup();
            EditorUtilites.EndOffsetBlock();

            EditorUtilites.BeginOffsetBlock();
            bonusesVisible.target = EditorGUILayout.ToggleLeft("Bonuses", bonusesVisible.target, EditorStyles.boldLabel);

            if (EditorGUILayout.BeginFadeGroup(bonusesVisible.faded)) {
                bonuses.DoLayoutList();
            }

            EditorGUILayout.EndFadeGroup();
            EditorUtilites.EndOffsetBlock();

            EditorUtilites.BeginOffsetBlock();
            boostersVisible.target = EditorGUILayout.ToggleLeft("Boosters", boostersVisible.target, EditorStyles.boldLabel);
            EditorGUILayout.Space();

            if (EditorGUILayout.BeginFadeGroup(boostersVisible.faded)) {
                DrawTypeSprites(BoosterTypes);
                BoosterIcons();
            }

            EditorGUILayout.EndFadeGroup();
            EditorUtilites.EndOffsetBlock();

            EditorUtilites.BeginOffsetBlock();
            aimsVisible.target = EditorGUILayout.ToggleLeft("Aims", aimsVisible.target, EditorStyles.boldLabel);
            EditorGUILayout.Space();

            if (EditorGUILayout.BeginFadeGroup(aimsVisible.faded)) {
                DrawTypeSprites(AimTypes);
            }

            EditorGUILayout.EndFadeGroup();
            EditorUtilites.EndOffsetBlock();
        }

        private void BoosterIcons ()
        {
            instance.lockedSprite = EditorGUILayout.ObjectField("Locked sprite", instance.lockedSprite, typeof(Sprite), false) as Sprite;
            instance.numberIcon = EditorGUILayout.ObjectField("Number icon", instance.numberIcon, typeof(Sprite), false) as Sprite;
            instance.plusIcon = EditorGUILayout.ObjectField("Plus icon", instance.plusIcon, typeof(Sprite), false) as Sprite;
        }

        private void DrawTypeSprites (string[] types)
        {
            foreach (var typeName in types) {
                EditorGUI.BeginChangeCheck();
                var setSprite = EditorGUILayout.ObjectField(typeName, instance[typeName], typeof(Sprite), false) as Sprite;
                if (EditorGUI.EndChangeCheck()) {
                    instance.Set(typeName, setSprite);
                    EditorUtility.SetDirty(instance);
                }
            }
        }

        private void DrawBonusHeader (Rect rect)
        {
            EditorGUI.LabelField(new Rect(rect.x + 5f, rect.y, 140f, rect.height), "Color and shape", EditorStyles.boldLabel);
            EditorGUI.LabelField(new Rect(rect.x + 170f, rect.y, 85f, rect.height), "Sprite", EditorStyles.boldLabel);
            EditorGUI.LabelField(new Rect(rect.x + 280f, rect.y, 140f, rect.height), "Particle prefab", EditorStyles.boldLabel);
        }

        private void DrawElementHeader (Rect rect)
        {
            EditorGUI.LabelField(new Rect(rect.x + 5f, rect.y, 140f, rect.height), "Color", EditorStyles.boldLabel);
            EditorGUI.LabelField(new Rect(rect.x + 170f, rect.y, 85f, rect.height), "Sprite", EditorStyles.boldLabel);
            EditorGUI.LabelField(new Rect(rect.x + 280f, rect.y, 140f, rect.height), "Particle prefab", EditorStyles.boldLabel);
        }

        private void DrawBonusElement (Rect rect, int index, bool isActive, bool isFocused)
        {
            EditorGUI.BeginChangeCheck();
            EditorGUI.LabelField(new Rect(rect.x, rect.y, 5f, rect.height), index.ToString());
            instance.bonusSprites[index].color =
                (TokenColor)EditorGUI.EnumPopup(new Rect(rect.x + 5f, rect.y, 140f, 50f),
                    instance.bonusSprites[index].color);
            instance.bonusSprites[index].shape =
                (MatchShape)EditorGUI.EnumMaskPopup(new Rect(rect.x + 5f, rect.y + 40f, 140f, 50f), GUIContent.none,
                    instance.bonusSprites[index].shape);
            instance.bonusSprites[index].sprite =
                (Sprite)EditorGUI.ObjectField(new Rect(rect.x + 170f, rect.y, 85f, 85f),
                    instance.bonusSprites[index].sprite, typeof(Sprite), false);
            instance.bonusSprites[index].particlePrefab =
                (GameObject)
                EditorGUI.ObjectField(new Rect(rect.x + 280f, rect.y + 30f, 140f, 20f),
                    instance.bonusSprites[index].particlePrefab, typeof(GameObject), false);
            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(instance);
            }
        }

        private void OnRemoveBonus (ReorderableList list)
        {
            instance.bonusSprites.RemoveAt(list.index);
        }

        private void OnAddBonus (ReorderableList list)
        {
            instance.bonusSprites.Add(new SpriteSettings.BonusSpriteItem());
        }

        private void DrawTokenElement (Rect rect, int index, bool isActive, bool isFocused)
        {
            EditorGUI.BeginChangeCheck();
            EditorGUI.LabelField(new Rect(rect.x, rect.y, 5f, rect.height), index.ToString());
            instance.tokenSprites[index].color =
                (TokenColor)EditorGUI.EnumPopup(new Rect(rect.x + 5f, rect.y, 140f, 50f),
                    instance.tokenSprites[index].color);
            instance.tokenSprites[index].sprite =
                (Sprite)EditorGUI.ObjectField(new Rect(rect.x + 170f, rect.y, 85f, 85f),
                    instance.tokenSprites[index].sprite, typeof(Sprite), false);
            instance.tokenSprites[index].particlePrefab =
                (GameObject)
                EditorGUI.ObjectField(new Rect(rect.x + 280f, rect.y + 30f, 140f, 20f),
                    instance.tokenSprites[index].particlePrefab, typeof(GameObject), false);
            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(instance);
            }
        }

        private void OnRemoveToken (ReorderableList list)
        {
            instance.tokenSprites.RemoveAt(list.index);
        }

        private void OnAddToken (ReorderableList list)
        {
            instance.tokenSprites.Add(new SpriteSettings.TokenSpriteItem());
        }
    }

}
